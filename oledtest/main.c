#include <stdio.h>
#include <math.h>
#include <taihen.h>
#include <psp2kern/kernel/cpu.h>
#include <psp2kern/kernel/modulemgr.h>
#include <psp2kern/kernel/threadmgr.h>
#include <psp2kern/kernel/sysmem.h>
#include <psp2kern/io/fcntl.h>
#include <psp2kern/io/stat.h>
#include <psp2kern/io/stat.h>
#include <psp2kern/display.h>
#include <psp2kern/lowio/i2c.h>
#include <psp2kern/lowio/gpio.h>
#include <psp2kern/registrymgr.h>
#include <psp2kern/kernel/intrmgr.h>
#include "utils.h"
#include "log.h"
#include "draw.h"

#define dmb() asm volatile("dmb\n\t")
#define dsb() asm volatile("dsb\n\t")

static inline unsigned int rbit(unsigned int x)
{
	unsigned int xrev;
	asm volatile("rbit %0, %1\n\t" : "=r"(xrev) : "r"(x));
	return xrev;
}

#define LOG(s, ...) \
	do { \
		char buffer[256]; \
		snprintf(buffer, sizeof(buffer), s, ##__VA_ARGS__); \
		log_write(buffer, strlen(buffer)); \
		console_print(buffer); \
	} while (0)

void _start() __attribute__ ((weak, alias ("module_start")));

extern int kscePervasiveSpiResetEnable(int bus);
extern int kscePervasiveSpiResetDisable(int bus);
extern int kscePervasiveSpiClockDisable(int bus);
extern int kscePervasiveSpiClockEnable(int bus);

static int ioremap(uintptr_t paddr, unsigned int size, SceUID *uid, void **addr);

int module_start(SceSize argc, const void *args)
{
	SceUID spi2regs_uid;
	volatile unsigned int *spi2regs_addr;

	log_reset();
	map_framebuffer();
	LOG("\n\noledtest by xerpi\n");

	ioremap(0xE0A20000, 0x1000, &spi2regs_uid, (void **)&spi2regs_addr);

	LOG("SPI 2 regs (OLED) addr: %p\n", spi2regs_addr);

	int enabled = ksceGpioPortRead(0, 0);
	if (!enabled) {
		ksceGpioPortSet(0, 0);
		ksceKernelDelayThread(20000);
	}

        kscePervasiveSpiClockEnable(2);

	for (; spi2regs_addr[0xA]; spi2regs_addr[0])
		;
	spi2regs_addr[2] = 0x30001;
	spi2regs_addr[4] = 1;

	unsigned int tmpbuffer = 0;
	unsigned int tmpbuffer_size = 0;

	tmpbuffer |= ((rbit(0xA1) >> 24) << 1) << tmpbuffer_size;
	tmpbuffer_size += 9;

	while (tmpbuffer_size > 15) {
		while (spi2regs_addr[0xB] == 0x7F)
			;

		spi2regs_addr[1] = tmpbuffer & 0xFFFF;

		tmpbuffer_size -= 16;
		tmpbuffer >>= 16;
	}

	const unsigned int data = ((rbit(0x00) >> 24) << 1) | 1;

	for (int i = 0; i < 6; i++) {
		tmpbuffer |= data << tmpbuffer_size;
		tmpbuffer_size += 9;

		while (tmpbuffer_size > 15) {
			while (spi2regs_addr[0xB] == 0x7F)
				;

			spi2regs_addr[1] = tmpbuffer & 0xFFFF;

			tmpbuffer_size -= 16;
			tmpbuffer >>= 16;
		}
	}

	while (tmpbuffer_size > 15) {
		while (spi2regs_addr[0xB] == 0x7F)
			;

		spi2regs_addr[1] = tmpbuffer & 0xFFFF;

		tmpbuffer_size -= 16;
		tmpbuffer >>= 16;
	}

	if (tmpbuffer_size > 0) {
		while (spi2regs_addr[0xB] == 0x7F)
			;

		spi2regs_addr[1] = tmpbuffer & 0xFFFF;

		tmpbuffer_size = 0;
		tmpbuffer >>= 16;
	}

	while (spi2regs_addr[0xB])
		;

	spi2regs_addr[4] = 0;

	while (spi2regs_addr[4] & 1)
		;

	/* Read */
	unsigned char read_buffer[4];
	unsigned int read_size = 0;
	unsigned int read_shift = 0;

	unsigned int v26 = 2;
	unsigned int v27 = 0;
	do {
		while (!spi2regs_addr[0xA])
			;

		unsigned int data = spi2regs_addr[0] << read_shift;
		read_shift += 16;

		for (v27 |= data; read_shift > 8; v27 >>= 9) {
			if (v26) {
				--v26;
			} else {
				read_buffer[read_size++] = rbit((v27 >> 1) & 0xFF) >> 24;
			}

			read_shift -= 9;
		}
	} while (read_size <= 4);

	if (0) {
		while (spi2regs_addr[0xB] && spi2regs_addr[0xB])
			;

		while (spi2regs_addr[0xA])
			spi2regs_addr[0];

		spi2regs_addr[4] = 0;

		while (spi2regs_addr[4] & 1)
			;
		spi2regs_addr[2] = 0x30001;

		spi2regs_addr[2];
		dsb();
	}

	if (!enabled) {
		ksceGpioPortClear(0, 0);
	}

	kscePervasiveSpiClockDisable(2);
	//kscePervasiveSpiResetEnable(2);

	for (int i = 0; i < 4; i++)
		LOG("+0x%02X: 0x%02X\n", i, read_buffer[i]);

	ksceKernelFreeMemBlock(spi2regs_uid);

	return SCE_KERNEL_START_SUCCESS;
}

int module_stop(SceSize argc, const void *args)
{
	unmap_framebuffer();

	return SCE_KERNEL_STOP_SUCCESS;
}

static int ioremap(uintptr_t paddr, unsigned int size, SceUID *uid, void **addr)
{
	int ret;
	SceUID mem_uid;
	unsigned int mem_size;
	void *mem_addr;

	mem_size = ALIGN(size, 4096);

	SceKernelAllocMemBlockKernelOpt opt;
	memset(&opt, 0, sizeof(opt));
	opt.size = sizeof(opt);
	opt.attr = 2;
	opt.paddr = paddr;
	mem_uid = ksceKernelAllocMemBlock("ioremap", 0x20100206, mem_size, &opt);
	if (mem_uid < 0)
		return mem_uid;

	ret = ksceKernelGetMemBlockBase(mem_uid, &mem_addr);
	if (ret < 0) {
		ksceKernelFreeMemBlock(mem_uid);
		return ret;
	}

	if (uid)
		*uid = mem_uid;
	if (addr)
		*addr = mem_addr;

	return 0;
}
