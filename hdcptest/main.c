#include <stdio.h>
#include <math.h>
#include <taihen.h>
#include <psp2kern/kernel/modulemgr.h>
#include <psp2kern/kernel/threadmgr.h>
#include <psp2kern/kernel/sysmem.h>
#include <psp2kern/lowio/i2c.h>
#include "utils.h"
#include "log.h"
#include "draw.h"

typedef unsigned char u8;

#define BIT(x)		(1 << (x))

#define HDMI_I2C_BUS		1
#define HDMI_I2C_ADDR		0x7A
#define HDMI_I2C_CEC_ADDR	0x7C

/* ADV7533 Main registers */
#define ADV7533_REG_PACKET_ENABLE0		0x40
#define ADV7533_REG_POWER			0x41
#define ADV7533_REG_STATUS			0x42
#define ADV7533_REG_AVI_INFOFRAME(x)		(0x55 + (x))
#define ADV7533_REG_INT(x)			(0x96 + (x))
#define ADV7533_REG_HDCP_HDMI_CFG		0xAF
#define ADV7533_REG_CEC_I2C_ADDR		0xE1
#define ADV7533_REG_POWER2			0xD6

#define ADV7533_PACKET_ENABLE_GC		BIT(7)

#define ADV7533_POWER_POWER_DOWN		BIT(6)

#define ADV7533_INT1_BKSV			BIT(6)

#define ADV7533_STATUS_HPD			BIT(6)

#define ADV7533_REG_POWER2_HPD_SRC_MASK		0xC0
#define ADV7533_REG_POWER2_HPD_SRC_HPD		BIT(6)

/* ADV7533 CEC registers */
#define ADV7533_REG_CEC_DSI_INTERNAL_TIMING	0x27

#define LOG(s, ...) \
	do { \
		char __buffer[256]; \
		snprintf(__buffer, sizeof(__buffer), s, ##__VA_ARGS__); \
		uart_print(0, __buffer); \
		if (0) console_print(__buffer); \
	} while (0)

void _start() __attribute__ ((weak, alias ("module_start")));

extern int ksceUartReadAvailable(int device);
extern int ksceUartWrite(int device, unsigned char data);
extern int ksceUartRead(int device);
extern int ksceUartInit(int device);

extern int ScePervasiveForDriver_18DD8043(int device);
extern int ScePervasiveForDriver_788B6C61(int device);
extern int ScePervasiveForDriver_A7CE7DCC(int device);
extern int ScePervasiveForDriver_EFD084D8(int device);

extern int SceHdmiForDriver_0686FBD6(int unk);
extern int SceHdmiForDriver_F20529CC(int unk);
extern int SceHdmiForDriver_FAB3A2E9(int unk);


static void uart_print(int bus, const char *str)
{
	while (*str) {
		ksceUartWrite(bus, *str);
		if (*str == '\n')
			ksceUartWrite(bus, '\r');
		str++;
	}
}

static int run = 1;
static SceUID thid;


static inline void hdmi_i2c_cmd_write_1(unsigned char addr, unsigned char reg, unsigned char data)
{
	unsigned char buff[2] = {reg, data};
	ksceI2cTransferWrite(HDMI_I2C_BUS, addr, buff, sizeof(buff));
}

static inline void hdmi_i2c_cmd_write_read(unsigned char addr, unsigned char reg, unsigned char *data, int size)
{
	unsigned char write_buff = reg;
	ksceI2cTransferWriteRead(HDMI_I2C_BUS,
		addr, &write_buff, sizeof(write_buff),
		addr, data, size);
}

static inline void hdmi_i2c_cmd_write_read_1(unsigned char addr, unsigned char reg, unsigned char *data)
{
	hdmi_i2c_cmd_write_read(addr, reg, data, sizeof(*data));
}

void hdmi_i2c_update_bits(unsigned char addr, unsigned char reg,
			  unsigned char mask, unsigned char val)
{
	unsigned char data;

	hdmi_i2c_cmd_write_read_1(addr, reg, &data);
	data &= ~mask;
	data |= val & mask;
	hdmi_i2c_cmd_write_1(addr, reg, data);
}

#define hdmi_write(addr,reg, data) \
	hdmi_i2c_cmd_write_1(addr, reg, data)

static inline u8 hdmi_read(u8 addr, u8 reg)
{
	unsigned char data;
	hdmi_i2c_cmd_write_read_1(addr, reg, &data);
	return data;
}

void hdmi_set_bit(u8 addr, u8 reg, u8 bit)
{
	u8 val;
	val = hdmi_read(addr, reg);
	val |= bit;
	hdmi_write(addr, reg, val);
}

void hdmi_clr_bit(u8 addr, u8 reg, u8 bit)
{
	u8 val;
	val = hdmi_read(addr, reg);
	val &= ~bit;
	hdmi_write(addr, reg, val);
}

static tai_hook_ref_t SceI2cForDriver_ksceI2cTransferWrite_ref;
static SceUID SceI2cForDriver_ksceI2cTransferWrite_hook_uid = -1;
static tai_hook_ref_t SceI2cForDriver_ksceI2cTransferRead_ref;
static SceUID SceI2cForDriver_ksceI2cTransferRead_hook_uid = -1;
static tai_hook_ref_t SceI2cForDriver_ksceI2cTransferWriteRead_ref;
static SceUID SceI2cForDriver_ksceI2cTransferWriteRead_hook_uid = -1;

static tai_hook_ref_t SceHdmi_SceHdmiIntr_subintr_handler_ref;
static SceUID SceHdmi_SceHdmiIntr_subintr_handler_hook_uid = -1;


static int SceI2cForDriver_ksceI2cTransferWrite_hook_func(int bus, unsigned int addr, const unsigned char *buffer, int size)
{
	/*unsigned char tmp[2];

	if (bus == 1 && addr == 0x7A && size == 2) {
		if (buffer[0] == 0xAF) {
			tmp[0] = 0xAF;
			tmp[1] = buffer[1] & ~((1 << 7) | (1 << 4));
			LOG("Got: 0x%02X to 0x%02X\n", buffer[1], tmp[1]);
			buffer = tmp;
		}
	}*/

	return TAI_CONTINUE(int, SceI2cForDriver_ksceI2cTransferWrite_ref, bus, addr, buffer, size);
}

static int SceI2cForDriver_ksceI2cTransferRead_hook_func(int bus, unsigned int addr, unsigned char *buffer, int size)
{
	if (bus == 1 && addr == 0x7A) {
		LOG("read\n");
	}

	return TAI_CONTINUE(int, SceI2cForDriver_ksceI2cTransferRead_ref, bus, addr, buffer, size);
}

static int SceI2cForDriver_ksceI2cTransferWriteRead_hook_func(int bus,
	unsigned int write_addr, unsigned char *write_buffer, int write_size,
	unsigned int read_addr, unsigned char *read_buffer, int read_size)
{
	int ret;

	ret = TAI_CONTINUE(int, SceI2cForDriver_ksceI2cTransferWriteRead_ref, bus,
		write_addr, write_buffer, write_size, read_addr, read_buffer, read_size);

	if (bus == 1 && write_addr == 0x7A) {
		LOG("Reg: 0x%02X, val: 0x%02X\n", write_buffer[0], read_buffer[0]);

		if (write_buffer[0] == 0xC8)
			read_buffer[0] = (read_buffer[0] & ~0b1111) | 0b0100;
	}

	return ret;
}

static int SceHdmi_SceHdmiIntr_subintr_handler_hook_func(int a1, int a2)
{
	LOG("HDMI interrupt!\n");

	return TAI_CONTINUE(int, SceHdmi_SceHdmiIntr_subintr_handler_ref, a1, a2);
}

static int thread(SceSize args, void *argp)
{
#define LOG_VAL(base, offset) \
	LOG(#base " +0x%04X: 0x%08X\n", offset, GET_VAL(base, offset))

	ksceKernelDelayThread(500 * 1000);

	SceHdmiForDriver_0686FBD6(1);
	SceHdmiForDriver_F20529CC(1);
	SceHdmiForDriver_FAB3A2E9(1);

	/*LOG("0xC8: 0x%02X\n", hdmi_read(HDMI_I2C_ADDR, 0xC8));
	LOG("0xC8: 0x%02X\n", hdmi_read(HDMI_I2C_ADDR, 0xC8));

	static unsigned char buffer[] = {
		0xAF,
		1 << 2,
	};

	ksceI2cTransferWrite(1, 0x7A, buffer, sizeof(buffer));*/

	//int y = console_get_y();
	uart_print(0, "\e[1;1H\e[2J");
	while (run) {
		//uart_print(0, "\033[0;0H");

		LOG("0xC8: 0x%02X\n", hdmi_read(HDMI_I2C_ADDR, 0xC8));
		LOG("0xD5: 0x%02X\n", hdmi_read(HDMI_I2C_ADDR, 0xD5));

		//LOG("\n");
		ksceKernelDelayThread(10 * 1000);
	}

	return 0;
}

SceUID inject_uid = -1;

int module_start(SceSize argc, const void *args)
{
	ScePervasiveForDriver_EFD084D8(0); // Turn on clock
	ScePervasiveForDriver_A7CE7DCC(0); // Out of reset
	ksceUartInit(0);
	map_framebuffer();
	LOG("\n\nhdcptest by xerpi\n");

	tai_module_info_t SceHdmi_modinfo;
	SceHdmi_modinfo.size = sizeof(tai_module_info_t);
	taiGetModuleInfoForKernel(KERNEL_PID, "SceHdmi", &SceHdmi_modinfo);

#if 0
	/*uint8_t patch = 1;
	inject_uid = taiInjectDataForKernel(KERNEL_PID, SceHdmi_modinfo.modid, 1,
		0x00A7C00A - 0x00A7C000, &patch, sizeof(patch));*/

	extern int module_get_offset(SceUID pid, SceUID modid, int segidx, size_t offset, uintptr_t *addr);

	void *addr = NULL;
	module_get_offset(KERNEL_PID, SceHdmi_modinfo.modid, 0,
		0x00BED30C - 0x00BE8000, &addr);

	LOG("addr: %p\n", addr);

	LOG("calling\n");
	((int (*)(int))addr + 1)(0);
	LOG("called\n");
#endif

	SceI2cForDriver_ksceI2cTransferWrite_hook_uid = taiHookFunctionExportForKernel(
		KERNEL_PID,
		&SceI2cForDriver_ksceI2cTransferWrite_ref,
		"SceLowio",
		0xE14BEF6E, /* SceI2cForDriver */
		0xCA94A759, /* ksceI2cTransferWrite */
		SceI2cForDriver_ksceI2cTransferWrite_hook_func);

	SceI2cForDriver_ksceI2cTransferRead_hook_uid = taiHookFunctionExportForKernel(
		KERNEL_PID,
		&SceI2cForDriver_ksceI2cTransferRead_ref,
		"SceLowio",
		0xE14BEF6E, /* SceI2cForDriver */
		0xD1D0A9A4, /* ksceI2cTransferRead */
		SceI2cForDriver_ksceI2cTransferRead_hook_func);

	SceI2cForDriver_ksceI2cTransferWriteRead_hook_uid = taiHookFunctionExportForKernel(
		KERNEL_PID,
		&SceI2cForDriver_ksceI2cTransferWriteRead_ref,
		"SceLowio",
		0xE14BEF6E, /* SceI2cForDriver */
		0x0A40B7BF, /* ksceI2cTransferWriteRead */
		SceI2cForDriver_ksceI2cTransferWriteRead_hook_func);

	SceHdmi_SceHdmiIntr_subintr_handler_hook_uid = taiHookFunctionOffsetForKernel(KERNEL_PID,
		&SceHdmi_SceHdmiIntr_subintr_handler_ref, SceHdmi_modinfo.modid, 0,
		0x00BEC9CC - 0x00BE8000, 1, SceHdmi_SceHdmiIntr_subintr_handler_hook_func);

	thid = ksceKernelCreateThread("disptest", thread, 0x00, 0x1000, 0, 1 << 0, 0);

	ksceKernelStartThread(thid, 0, NULL);

	return SCE_KERNEL_START_SUCCESS;
}

int module_stop(SceSize argc, const void *args)
{
	run = 0;
	ksceKernelWaitThreadEnd(thid, NULL, NULL);

	taiInjectReleaseForKernel(inject_uid);

	if (SceI2cForDriver_ksceI2cTransferWrite_hook_uid > 0) {
		taiHookReleaseForKernel(SceI2cForDriver_ksceI2cTransferWrite_hook_uid,
			SceI2cForDriver_ksceI2cTransferWrite_ref);
	}

	if (SceI2cForDriver_ksceI2cTransferRead_hook_uid > 0) {
		taiHookReleaseForKernel(SceI2cForDriver_ksceI2cTransferRead_hook_uid,
			SceI2cForDriver_ksceI2cTransferRead_ref);
	}

	if (SceI2cForDriver_ksceI2cTransferWriteRead_hook_uid > 0) {
		taiHookReleaseForKernel(SceI2cForDriver_ksceI2cTransferWriteRead_hook_uid,
			SceI2cForDriver_ksceI2cTransferWriteRead_ref);
	}

	if (SceHdmi_SceHdmiIntr_subintr_handler_hook_uid > 0) {
		taiHookReleaseForKernel(SceHdmi_SceHdmiIntr_subintr_handler_hook_uid,
			SceHdmi_SceHdmiIntr_subintr_handler_ref);
	}

	unmap_framebuffer();

	return SCE_KERNEL_STOP_SUCCESS;
}
