#include <stdio.h>
#include <psp2kern/kernel/cpu.h>
#include <psp2kern/kernel/modulemgr.h>
#include <psp2kern/kernel/threadmgr.h>
#include <psp2kern/kernel/sysmem.h>
#include <psp2kern/io/fcntl.h>
#include <psp2kern/io/stat.h>
#include <psp2kern/display.h>
#include <psp2kern/lowio/i2c.h>
#include <psp2kern/lowio/gpio.h>
#include "utils.h"
#include "log.h"
#include "draw.h"

#define LOG(s, ...) \
	do { \
		char buffer[256]; \
		snprintf(buffer, sizeof(buffer), s, ##__VA_ARGS__); \
		uart_print(0, buffer); \
		console_print(buffer); \
	} while (0)

#define DUMP_PATH "ux0:/dump/regdump"

extern int ksceUartReadAvailable(int device);
extern int ksceUartWrite(int device, unsigned char data);
extern int ksceUartRead(int device);
extern int ksceUartInit(int device);

extern int ScePervasiveForDriver_18DD8043(int device);
extern int ScePervasiveForDriver_788B6C61(int device);
extern int ScePervasiveForDriver_A7CE7DCC(int device);
extern int ScePervasiveForDriver_EFD084D8(int device);

extern int SceSysconForDriver_62155962(int);
extern int SceSysconForDriver_FF86F4C5(void);

static void uart_print(int bus, const char *str)
{
	while (*str) {
		ksceUartWrite(bus, *str);
		if (*str == '\n')
			ksceUartWrite(bus, '\r');
		str++;
	}
}

static void dump_i2c_device(unsigned char i2c_addr)
{
	char filename[512];
	SceUID fd;

	snprintf(filename, sizeof(filename), DUMP_PATH "/i2c_addr_0x%02X.txt", i2c_addr);

	ksceIoMkdir(DUMP_PATH, 6);

	if (!(fd = ksceIoOpen(filename, SCE_O_WRONLY | SCE_O_CREAT | SCE_O_TRUNC, 6))) {
		LOG("Error creating %s\n", filename);
		return;
	}

	for (unsigned int i = 0; i < 256; i++) {
		char buff[64];
		unsigned char data = hdmi_i2c_cmd_write_read_1(i2c_addr, i);
		snprintf(buff, sizeof(buff), "0x%02X: 0x%02X\n", i, data);
		ksceIoWrite(fd, buff, strlen(buff));
	}

	ksceIoClose(fd);
}

static void dump_reg(const char *filename, unsigned int paddr, void *addr, unsigned int size)
{
	char filename_bin[512];
	char filename_txt[512];
	SceUID fd;

	snprintf(filename_bin, sizeof(filename_bin), DUMP_PATH "/%s_0x%08X.bin", filename, paddr);
	snprintf(filename_txt, sizeof(filename_txt), DUMP_PATH "/%s_0x%08X.txt", filename, paddr);

	if (!(fd = ksceIoOpen(filename_bin, SCE_O_WRONLY | SCE_O_CREAT | SCE_O_TRUNC, 6))) {
		LOG("Error creating %s\n", filename_bin);
		return;
	}
	ksceIoWrite(fd, addr, size);
	ksceIoClose(fd);

	if (!(fd = ksceIoOpen(filename_txt, SCE_O_WRONLY | SCE_O_CREAT | SCE_O_TRUNC, 6))) {
		LOG("Error creating %s\n", filename_txt);
		return;
	}

	for (unsigned int i = 0; i < size; i += 4) {
		char buff[64];
		unsigned int data = *(unsigned int *)((char *)addr + i);
		snprintf(buff, sizeof(buff), "+0x%04X: 0x%08X\n", i, data);
		ksceIoWrite(fd, buff, strlen(buff));
	}

	ksceIoClose(fd);
}

static const struct reg_region {
	const char *name;
	unsigned int paddr;
	unsigned int size;
} regs[] = {
	{"SceGpio1Reg", 0xE0100000, 0x1000},
	{"SceGpio0Reg", 0xE20A0000, 0x1000},
	/*{"SceIftu0RegA", 0xE5020000, 0x1000},
	{"SceIftu0RegB", 0xE5021000, 0x1000},
	{"SceIftuc0Reg", 0xE5022000, 0x1000},*/
	{"SceIftu1RegA", 0xE5030000, 0x1000},
	{"SceIftu1RegB", 0xE5031000, 0x1000},
	{"SceIftuc1Reg", 0xE5032000, 0x1000},
	//{"SceIftu2Reg", 0xE5040000, 0x1000},
	//{"SceDsi0Reg", 0xE5050000, 0x1000},
	{"SceDsi1Reg", 0xE5060000, 0x1000},
	/*{"ScePervasiveMisc", 0xE3100000, 0x1000},
	{"ScePervasiveResetReg", 0xE3101000, 0x1000},
	{"ScePervasiveGate", 0xE3102000, 0x1000},
	{"ScePervasiveBaseClk", 0xE3103000, 0x1000},
	{"ScePervasiveVid", 0xE3104000, 0x1000},
	{"SceUartClkgenReg", 0xE3105000, 0x1000},
	{"ScePervasiveMailboxReg", 0xE3106000, 0x1000},
	{"ScePervasiveTas0", 0xE3108000, 0x1000},
	{"ScePervasiveTas1", 0xE3109000, 0x1000},
	{"ScePervasiveTas2", 0xE310A000, 0x1000},
	{"ScePervasiveTas3", 0xE310B000, 0x1000},
	{"ScePervasiveTas4", 0xE310C000, 0x1000},
	{"ScePervasiveTas5", 0xE310D000, 0x1000},
	{"ScePervasiveTas6", 0xE310E000, 0x1000},
	{"ScePervasiveTas7", 0xE310F000, 0x1000},*/
};

void _start() __attribute__ ((weak, alias ("module_start")));

int module_start(SceSize argc, const void *args)
{
	ScePervasiveForDriver_EFD084D8(0); // Turn on clock
	ScePervasiveForDriver_A7CE7DCC(0); // Out of reset

	ksceUartInit(0);

	//log_reset();
	map_framebuffer();
	LOG("Regsdumper by xerpi\n");

	ksceIoMkdir(DUMP_PATH, 6);

	for (int i = 0; i < sizeof(regs) / sizeof(regs[0]); i++) {
		SceUID mem_uid;
		unsigned int mem_size;
		void *mem_addr;

		mem_size = ALIGN(regs[i].size, 4096);

		SceKernelAllocMemBlockKernelOpt opt;
		memset(&opt, 0, sizeof(opt));
		opt.size = sizeof(opt);
		opt.attr = 2;
		opt.paddr = regs[i].paddr;
		mem_uid = ksceKernelAllocMemBlock(regs[i].name, 0x20100206, mem_size, &opt);
		if (mem_uid < 0) {
			LOG("Skipping %s (0x%08X)...\n", regs[i].name, regs[i].paddr);
			continue;
		}

		if (ksceKernelGetMemBlockBase(mem_uid, &mem_addr) < 0) {
			LOG("Skipping %s (0x%08X)...\n", regs[i].name, regs[i].paddr);
			ksceKernelFreeMemBlock(mem_uid);
			continue;
		}

		LOG("Dumping %s (0x%08X)...\n", regs[i].name, regs[i].paddr);

		dump_reg(regs[i].name, regs[i].paddr, mem_addr, regs[i].size);

		ksceKernelFreeMemBlock(mem_uid);
	}

	LOG("Dump done.\n");

	return SCE_KERNEL_START_SUCCESS;
}

int module_stop(SceSize argc, const void *args)
{
	unmap_framebuffer();

	return SCE_KERNEL_STOP_SUCCESS;
}

int ioremap(uintptr_t paddr, unsigned int size, SceUID *uid, void **addr)
{
	int ret;
	SceUID mem_uid;
	unsigned int mem_size;
	void *mem_addr;

	mem_size = ALIGN(size, 4096);

	SceKernelAllocMemBlockKernelOpt opt;
	memset(&opt, 0, sizeof(opt));
	opt.size = sizeof(opt);
	opt.attr = 2;
	opt.paddr = paddr;
	mem_uid = ksceKernelAllocMemBlock("ioremap", 0x20100206, mem_size, &opt);
	if (mem_uid < 0)
		return mem_uid;

	ret = ksceKernelGetMemBlockBase(mem_uid, &mem_addr);
	if (ret < 0) {
		ksceKernelFreeMemBlock(mem_uid);
		return ret;
	}

	if (uid)
		*uid = mem_uid;
	if (addr)
		*addr = mem_addr;

	return 0;
}

int alloc_phycont(unsigned int size, SceUID *uid, void **addr)
{
	int ret;
	SceUID mem_uid;
	unsigned int mem_size;
	void *mem_addr;

	mem_size = ALIGN(size, 4096);

	SceKernelAllocMemBlockKernelOpt opt;
	memset(&opt, 0, sizeof(opt));
	opt.size = sizeof(opt);
	opt.attr = 0x200004;
	opt.alignment = 0x1000;
	mem_uid = ksceKernelAllocMemBlock("phycont", 0x30808006, mem_size, &opt);
	if (mem_uid < 0)
		return mem_uid;

	ret = ksceKernelGetMemBlockBase(mem_uid, &mem_addr);
	if (ret < 0) {
		ksceKernelFreeMemBlock(mem_uid);
		return ret;
	}

	if (uid)
		*uid = mem_uid;
	if (addr)
		*addr = mem_addr;

	return 0;
}
