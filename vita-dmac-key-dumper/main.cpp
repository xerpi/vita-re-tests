#include <cstdio>
#include <cstdlib>
#include <cstdint>
#include <cstring>
#include <vector>
#include <list>
#include <omp.h>
#include <openssl/des.h>

#define BUFSIZE 8

static void usage(const char *name)
{
	printf("vita-dmac-key-dumper by xerpi\n");
	printf("Usage:\n\t%s partials.bin\n", name);
}

int main(int argc, char *argv[])
{
	long i;
	long partials_size;
	long num_partials;
	FILE *fp;

	if (argc < 2) {
		usage(argv[0]);
		return EXIT_FAILURE;
	}

	fp = fopen(argv[1], "rb");
	if (!fp) {
		printf("Error opening \'%s\'.\n", argv[1]);
		return EXIT_FAILURE;
	}

	fseek(fp, 0, SEEK_END);
	partials_size = ftell(fp);
	rewind(fp);

	printf("\'%s\' is %ld bytes long.\n", argv[1], partials_size);

	/* Input (cleartext) is always 0s */
	unsigned char in[BUFSIZE];
	memset(in, 0, sizeof(in));

	num_partials = partials_size / BUFSIZE;

	typedef std::vector<uint32_t> key_vec_t;

	std::vector<std::list<key_vec_t>> res(num_partials);

	for (i = 0; i < num_partials; i++) {
		unsigned char partials_out[BUFSIZE];
		fread(partials_out, sizeof(partials_out), 1, fp);

		printf("\n[+] Target ciphertext (%ld/%ld):", i + 1, partials_size / BUFSIZE);
		int j;
		for (j = 0; j < BUFSIZE; j++)
			printf(" %02X", partials_out[j]);
		printf("\n");

		#pragma omp parallel
		{
			uint64_t test_val;
			unsigned char out[BUFSIZE];
			unsigned char tmp_key[BUFSIZE];
			uint32_t *key_partial = (uint32_t *)((char *)tmp_key + i * 4);

			memset(tmp_key, 0, sizeof(tmp_key));

			#pragma omp for
			for (test_val = 0; test_val <= 0x01FFFFFFULL; test_val++) {
				DES_key_schedule keysched;

				*key_partial = test_val;

				DES_set_key((DES_cblock *)tmp_key, &keysched);

				DES_ecb_encrypt((DES_cblock *)in,
						(DES_cblock *)out,
						&keysched, DES_ENCRYPT);

				if (memcmp(partials_out, out, sizeof(out)) == 0) {
					printf("Found ciphertext target! Partial key: 0x%08X\n", *key_partial);

					/* Add the key part to the list */
					#pragma omp critical
					if (i == 0) {
						key_vec_t new_key_part;
						new_key_part.push_back(*key_partial);
						res[0].push_back(new_key_part);
					} else {
						for (auto &prev_key: res[i - 1]) {
							auto new_key = prev_key;
							new_key.push_back(*key_partial);
							res[i].push_back(new_key);
						}
					}
				}
			}
		}
	}

	printf("Key successfully bruteforced! Possible combinations:\n");

	for (auto &key: res.back()) {
		printf("Key:");

		for (auto &key_e: key) {
			for (i = 0; i < 32; i+=8)
				printf(" %02X", (key_e >> i) & 0xFF);
		}

		printf("\n");

		DES_key_schedule keysched;
		unsigned char out[BUFSIZE];

		DES_set_key((DES_cblock *)key.data(), &keysched);

		DES_ecb_encrypt((DES_cblock *)in,
				(DES_cblock *)out,
				&keysched, DES_ENCRYPT);

		printf("Out:");
		int j;
		for (j = 0; j < BUFSIZE; j++)
			printf(" %02X", out[j]);
		printf("\n");
	}

	fclose(fp);

	return EXIT_SUCCESS;
}
