#include <psp2kern/kernel/modulemgr.h>
#include <psp2kern/kernel/threadmgr.h>
#include <psp2kern/kernel/sysmem.h>
#include <psp2kern/kernel/cpu.h>
#include <psp2kern/udcd.h>
#include <psp2kern/display.h>
#include <taihen.h>
#include "draw.h"
#include "log.h"

typedef unsigned int u32;

#define ALIGN(x, a) (((x) + ((a) - 1)) & ~((a) - 1))

#define RGBA8(r, g, b, a)      ((((a)&0xFF)<<24) | (((b)&0xFF)<<16) | (((g)&0xFF)<<8) | (((r)&0xFF)<<0))

#define RED   RGBA8(255, 0,   0,   255)
#define GREEN RGBA8(0,   255, 0,   255)
#define BLUE  RGBA8(0,   0,   255, 255)
#define CYAN  RGBA8(0,   255, 255, 255)
#define LIME  RGBA8(50,  205, 50,  255)
#define PURP  RGBA8(147, 112, 219, 255)
#define WHITE RGBA8(255, 255, 255, 255)
#define BLACK RGBA8(0,   0,   0,   255)

#define LOG(s, ...) \
	do { \
		char __buffer[256]; \
		snprintf(__buffer, sizeof(__buffer), s, ##__VA_ARGS__); \
		console_print(__buffer); \
		log_write(__buffer, strlen(__buffer)); \
	} while (0)

typedef enum SceIftuErrorCode {
	SCE_IFTU_ERROR_INVALID_PLANE		= 0x803F0700,
	SCE_IFTU_ERROR_INVALID_PARAM		= 0x803F0701,
	SCE_IFTU_ERROR_INVALID_PIXELFORMAT	= 0x803F0703,
	SCE_IFTU_ERROR_PLANE_BUSY		= 0x803F0704,
} SceIftuErrorCode;

typedef struct SceIftuCscParams {
	unsigned int post_add_0; /* 10-bit integer */
	unsigned int post_add_1_2; /* 10-bit integer */
	unsigned int post_clamp_max_0; /* 10-bit integer */
	unsigned int post_clamp_min_0; /* 10-bit integer */
	unsigned int post_clamp_max_1_2; /* 10-bit integer */
	unsigned int post_clamp_min_1_2; /* 10-bit integer */
	unsigned int ctm[3][3]; /* S3.9 fixed point format */
} SceIftuCscParams; /* size = 0x3C */

typedef struct SceIftuConvParams {
	unsigned int size;
	unsigned int unk04;
	SceIftuCscParams *csc_params1; // +0x08
	SceIftuCscParams *csc_params2; // +0x0C
	unsigned int csc_control; // +0x10
	unsigned int unk14;
	unsigned int unk18;
	unsigned int unk1C;
	unsigned int alpha;
	unsigned int unk24;
} SceIftuConvParams; /* size = 0x28 */

typedef struct SceIftuFrameBuf {
	unsigned int pixelformat;
	unsigned int width; /* Aligned to 16 */
	unsigned int height; /* Aligned to 8 */
	unsigned int leftover_stride; /* (pitch - aligned_w) * bpp */
	unsigned int leftover_align; /* if YCbCr: (width >> 1) & 0xF [chroma align?] */
	unsigned int paddr0;
	unsigned int paddr1;
	unsigned int paddr2;
} SceIftuFrameBuf; /* size = 0x20 */

typedef struct SceIftuPlaneState {
	SceIftuFrameBuf fb;
	unsigned int unk20; // always 0?
	unsigned int src_x; // in (0x10000 / 960) multiples
	unsigned int src_y; // in (0x10000 / 544) multiples
	unsigned int src_w; // in (0x10000 / 960) multiples
	unsigned int src_h; // in (0x10000 / 544) multiples
	unsigned int dst_x;
	unsigned int dst_y;
	unsigned int dst_w;
	unsigned int dst_h;
	unsigned int vtop_padding;
	unsigned int vbot_padding; /* h - aligned_h */
	unsigned int hleft_padding;
	unsigned int hright_padding; /* w - aligned_w */
} SceIftuPlaneState; /* size = 0x54 */

extern int ksceIftuCsc(SceIftuFrameBuf *dst_fb, SceIftuPlaneState *src_plane_state, SceIftuConvParams *conv_params);

#define FB_NUM 4

static SceUID fb_uid[FB_NUM];
static unsigned char *fb_addr[FB_NUM];
static unsigned int fb_paddr[FB_NUM];

#define FB_WIDTH 128
#define FB_HEIGHT 128
#define FB_SIZE (FB_WIDTH * FB_HEIGHT * 4)

//ksceKernelCpuUnrestrictedMemcpy(saved_ifturegA[0], ifturegA_addr[0], 0x1000);

/*void fill_fb(void *addr, unsigned int color)
{
	int i, j;

	for (i = 0; i < FB_HEIGHT; i++) {
		for (j = 0; j < FB_WIDTH; j++) {
			if (j % 64 < 32 || i % 64 < 32)
				*(unsigned int *)((char *)addr + i * FB_WIDTH * 4 + j * 4) = color;
			else
				*(unsigned int *)((char *)addr + i * FB_WIDTH * 4 + j * 4) = ~color | 0xFF000000;
		}
	}

	ksceKernelCpuDcacheAndL2WritebackRange(addr, FB_SIZE);
}*/

void _start() __attribute__((weak, alias("module_start")));

int module_start(SceSize argc, const void *args)
{
	int ret;

	log_reset();
	map_framebuffer();

	LOG("iftu-csc-test by xerpi\n");

	const unsigned int fb_size_align = ALIGN(FB_SIZE, 256 * 1024);
	for (int i = 0; i < FB_NUM; i++) {
		fb_uid[i] = ksceKernelAllocMemBlock("fb", 0x40404006 , fb_size_align, NULL);
		ksceKernelGetMemBlockBase(fb_uid[i], (void **)&fb_addr[i]);

		ksceKernelGetPaddr(fb_addr[i], &fb_paddr[i]);
	}

	SceIftuCscParams RGB_to_YCbCr_JPEG_csc_params = {
		0, 0x202, 0x3FF,
		0, 0x3FF,     0,
		{
			{ 0x99, 0x12C,  0x3A},
			{0xFAA, 0xF57, 0x100},
			{0x100, 0xF2A, 0xFD7}
		}
	};

	SceIftuCscParams YCbCr_to_RGB_HDTV_C0180C = {
		0x40, 0x202, 0x3FF,
		   0,     0,     0,
		{
			{0x254,     0, 0x395},
			{0x254, 0xF93, 0xEF0},
			{0x254, 0x439,     0}
		}
	};

	SceIftuCscParams stru_C01970 = {
		0x40,  0x40, 0x3AC,
		0x40, 0x3AC,  0x40,
		{
			{0x1B7,     0,     0},
			{    0, 0x1B7,     0},
			{    0,     0, 0x1B7}
		}
	};

	SceIftuCscParams identity_mtx = {
		0, 0, 0x3FF,
		0, 0x3FF, 0,
		{
			{0x200, 0,     0},
			{0,     0x200, 0},
			{0,     0,     0x200}
		}
	};

	SceIftuConvParams conv_params;
	memset(&conv_params, 0, sizeof(conv_params));
	conv_params.size = sizeof(conv_params);
	conv_params.unk04 = 1;
	conv_params.csc_params1 = &RGB_to_YCbCr_JPEG_csc_params;
	conv_params.csc_params2 = NULL;
	conv_params.csc_control = 1;
	conv_params.unk14 = 0;
	conv_params.unk18 = 0;
	conv_params.unk1C = 0;
	conv_params.alpha = 0xFF;
	conv_params.unk24 = 0;

	typedef enum SceIftuPixelformat {
		SCE_IFTU_PIXELFORMAT_BGR565		= 0x01,
		SCE_IFTU_PIXELFORMAT_RGB565		= 0x02,
		SCE_IFTU_PIXELFORMAT_BGRA5551		= 0x04,
		SCE_IFTU_PIXELFORMAT_RGBA5551		= 0x08,
		SCE_IFTU_PIXELFORMAT_BGRX8888		= 0x10,
		SCE_IFTU_PIXELFORMAT_RGBX8888		= 0x20,
		SCE_IFTU_PIXELFORMAT_BGRA1010102	= 0x40,
		SCE_IFTU_PIXELFORMAT_RGBA1010102	= 0x80,
		SCE_IFTU_PIXELFORMAT_BGRP		= 0x100,	/* 3 planes - R, G, B */
		SCE_IFTU_PIXELFORMAT_RGBX8888_MULT	= 0x1000,	/* 1 plane */
		SCE_IFTU_PIXELFORMAT_BGRX8888_MULT	= 0x2000,	/* 1 plane */
		SCE_IFTU_PIXELFORMAT_RGBA1010102_MULT	= 0x4000,
		SCE_IFTU_PIXELFORMAT_BGRA1010102_MULT	= 0x8000,
		SCE_IFTU_PIXELFORMAT_NV12		= 0x10000,	/* 2 planes - Y, Cb + Cr interleaved */
		SCE_IFTU_PIXELFORMAT_YUV420		= 0x20000,	/* 3 planes - Y, Cb, Cr */
		SCE_IFTU_PIXELFORMAT_YUV422		= 0x200000,	/* 3 planes - Y, Cb, Cr */
	} SceIftuPixelformat;


	#define	FB_SRC_COLOR	RGBA8(0xFF, 0x00, 0xFF, 0xFF)
	#define SRC_PIXELFORMAT	SCE_IFTU_PIXELFORMAT_BGRX8888
	#define DST_PIXELFORMAT	SCE_IFTU_PIXELFORMAT_RGBX8888_MULT

	// In: RGBA8(0xFF, 0x00, 0xFF, 0xFF)
	// Out YUV422:
	// Y:  0x69696969
	// Cb: 0xD5D5D5D5
	// Cr: 0xECECECEC

	// RGB565: 0x1106 = 00010 001000 00110

	// pxlfmt 2: 0x3102 = 00110 001000 00010
	// pxlfmt 4: 0x8886 = 10001 000100 00110

	SceIftuPlaneState src_plane_state;
	memset(&src_plane_state, 0, sizeof(src_plane_state));
	src_plane_state.fb.pixelformat = SRC_PIXELFORMAT;
	src_plane_state.fb.width = FB_WIDTH;
	src_plane_state.fb.height = FB_HEIGHT;
	src_plane_state.fb.leftover_stride = 0;
	src_plane_state.fb.leftover_align = 0;
	src_plane_state.fb.paddr0 = fb_paddr[0];
	src_plane_state.unk20 = 0;
	src_plane_state.src_x = 0;
	src_plane_state.src_y = 0;
	src_plane_state.src_w = 0x10000;
	src_plane_state.src_h = 0x10000;
	src_plane_state.dst_x = 0;
	src_plane_state.dst_y = 0;
	src_plane_state.dst_w = 0;
	src_plane_state.dst_h = 0;
	src_plane_state.vtop_padding = 0;
	src_plane_state.vbot_padding = 0;
	src_plane_state.hleft_padding = 0;
	src_plane_state.hright_padding = 0;

	SceIftuFrameBuf dst_fb;
	memset(&dst_fb, 0, sizeof(dst_fb));
	dst_fb.pixelformat = DST_PIXELFORMAT;
	dst_fb.width = FB_WIDTH;
	dst_fb.height = FB_HEIGHT;
	dst_fb.leftover_stride = 0;
	dst_fb.leftover_align = 0;
	dst_fb.paddr0 = fb_paddr[1];
	dst_fb.paddr1 = fb_paddr[2];
	dst_fb.paddr2 = fb_paddr[3];

	fill_fb(FB_WIDTH, FB_HEIGHT, fb_addr[0], FB_SRC_COLOR);
	fill_fb(FB_WIDTH, FB_HEIGHT, fb_addr[1], BLUE);
	fill_fb(FB_WIDTH, FB_HEIGHT, fb_addr[2], BLUE);
	fill_fb(FB_WIDTH, FB_HEIGHT, fb_addr[3], BLUE);

	ret = ksceIftuCsc(&dst_fb, &src_plane_state, &conv_params);
	LOG("ksceIftuCsc: 0x%08X\n", ret);
	LOG("Format: 0x%X\n", DST_PIXELFORMAT);

	if (ret < 0)
		return SCE_KERNEL_START_SUCCESS;

	u32 *fb_src = (u32 *)fb_addr[0];
	u32 *fb_dst[3] = {
		(u32 *)fb_addr[1],
		(u32 *)fb_addr[2],
		(u32 *)fb_addr[3]
	};

	LOG("src:  0x%08X 0x%08X\n", fb_src[0], fb_src[1]);
	LOG("dst1: 0x%08X 0x%08X\n", fb_dst[0][0], fb_dst[0][0]);
	LOG("dst2: 0x%08X 0x%08X\n", fb_dst[1][0], fb_dst[1][0]);
	LOG("dst3: 0x%08X 0x%08X\n", fb_dst[2][0], fb_dst[2][0]);

	draw_fb(100, 400, FB_WIDTH, FB_HEIGHT, fb_addr[0]);
	draw_rectangle(100, 370, 20, 20, fb_src[0]);

	for (int i = 0; i < 3; i++) {
		draw_fb(200 + FB_WIDTH + i * (FB_WIDTH + 50), 400, FB_WIDTH, FB_HEIGHT, fb_dst[i]);
		draw_rectangle(200 + FB_WIDTH + i * (FB_WIDTH + 50), 370, 20, 20, fb_dst[i][0]);
	}

	return SCE_KERNEL_START_SUCCESS;
}

int module_stop(SceSize argc, const void *args)
{
	for (int i = 0; i < FB_NUM; i++) {
		ksceKernelFreeMemBlock(fb_uid[i]);
	}

	unmap_framebuffer();

	return SCE_KERNEL_STOP_SUCCESS;
}

static int ioremap(uintptr_t paddr, unsigned int size, SceUID *uid, void **addr)
{
	int ret;
	SceUID mem_uid;
	unsigned int mem_size;
	void *mem_addr;

	mem_size = ALIGN(size, 4096);

	SceKernelAllocMemBlockKernelOpt opt;
	memset(&opt, 0, sizeof(opt));
	opt.size = sizeof(opt);
	opt.attr = 2;
	opt.paddr = paddr;
	mem_uid = ksceKernelAllocMemBlock("ioremap", 0x20100206, mem_size, &opt);
	if (mem_uid < 0)
		return mem_uid;

	ret = ksceKernelGetMemBlockBase(mem_uid, &mem_addr);
	if (ret < 0) {
		ksceKernelFreeMemBlock(mem_uid);
		return ret;
	}

	if (uid)
		*uid = mem_uid;
	if (addr)
		*addr = mem_addr;

	return 0;
}
