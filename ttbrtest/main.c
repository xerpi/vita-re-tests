#include <stdio.h>
#include <string.h>
#include <psp2kern/kernel/cpu.h>
#include <psp2kern/kernel/modulemgr.h>
#include <psp2kern/kernel/threadmgr.h>
#include <psp2kern/kernel/sysmem.h>
#include <psp2kern/uart.h>
#include <taihen.h>

#define LOG(...) \
	do { \
		char _buffer[256]; \
		snprintf(_buffer, sizeof(_buffer), ##__VA_ARGS__); \
		uart0_print(_buffer); \
	} while (0)

static void uart0_print(const char *str)
{
	while (*str) {
		ksceUartWrite(0, *str);
		if (*str == '\n')
			ksceUartWrite(0, '\r');
		str++;
	}
}

unsigned long get_ttbr0(void)
{
	unsigned long ttbr0;

	asm volatile("mrc p15, 0, %0, c2, c0, 0\n" : "=r"(ttbr0));

	return ttbr0;
}

unsigned long get_ttbr1(void)
{
	unsigned long ttbr0;

	asm volatile("mrc p15, 0, %0, c2, c0, 1\n" : "=r"(ttbr0));

	return ttbr0;
}

unsigned long get_ttbcr(void)
{
	unsigned long ttbcr;

	asm volatile("mrc p15, 0, %0, c2, c0, 2\n" : "=r"(ttbcr));

	return ttbcr;
}

unsigned long get_paddr(unsigned long vaddr)
{
	unsigned long paddr;

	ksceKernelGetPaddr((void *)vaddr, (uintptr_t *)&paddr);

	return paddr;
}

int find_paddr(unsigned long paddr, unsigned long vaddr_start, unsigned int range, unsigned int step, unsigned long *found_vaddr)
{
	unsigned int vaddr = vaddr_start;
	unsigned long vaddr_end = vaddr_start + range;

	for (; vaddr < vaddr_end; vaddr += step) {
		unsigned long cur_paddr = get_paddr(vaddr);

		if ((cur_paddr & ~(step - 1)) == (paddr & ~(step - 1))) {
			if (found_vaddr)
				*found_vaddr = vaddr;
			return 1;
		}
	}

	return 0;
}

unsigned long page_table_entry(unsigned long paddr)
{
	unsigned long base_addr = paddr >> 12;
	unsigned long XN        = 0b0;   /* XN disabled */
	unsigned long C_B       = 0b10;  /* Outer and Inner Write-Through, no Write-Allocate */
	unsigned long AP_1_0    = 0b11;  /* Full access */
	unsigned long TEX_2_0   = 0b000; /* Outer and Inner Write-Through, no Write-Allocate */
	unsigned long AP_2      = 0b0;   /* Full access */
	unsigned long S         = 0b1;   /* Shareable */
	unsigned long nG        = 0b0;   /* Global translation */

	return  (base_addr << 12) |
		(nG        << 11) |
		(S         << 10) |
		(AP_2      <<  9) |
		(TEX_2_0   <<  6) |
		(AP_1_0    <<  4) |
		(C_B       <<  2) |
		(1         <<  1) |
		(XN        <<  0);
}

void map_identity(void)
{
	int i;
	unsigned long ttbcr_n;
	unsigned long ttbr0_paddr;
	unsigned long ttbr0_vaddr;
	unsigned long first_page_table_paddr;
	unsigned long first_page_table_vaddr;
	unsigned long pt_entries[4];

	/*
	 * Identity-map the start of the scratchpad (PA 0x00000000-0x00003FFF) to
	 * the VA 0x00000000-0x00003FFF.
	 * To do such thing we will use the first 4 PTEs of the
	 * first page table of TTBR0 (which aren't used).
	 */

	ttbcr_n = get_ttbcr() & 7;
	ttbr0_paddr = get_ttbr0() & ~((1 << (14 - ttbcr_n)) - 1);
	find_paddr(ttbr0_paddr, 0, 0xFFFFFFFF, 0x1000, &ttbr0_vaddr);

	first_page_table_paddr = (*(unsigned int *)ttbr0_vaddr) & 0xFFFFFC00;
	find_paddr(first_page_table_paddr, 0, 0xFFFFFFFF, 0x1000, &first_page_table_vaddr);

	for (i = 0; i < 4; i++)
		pt_entries[i] = page_table_entry(i << 12);

	ksceKernelCpuUnrestrictedMemcpy((void *)first_page_table_vaddr, pt_entries, sizeof(pt_entries));
	ksceKernelCpuDcacheAndL2WritebackRange((void *)first_page_table_vaddr, sizeof(pt_entries));

	asm volatile(
		"dsb\n\t"
		"isb\n\t"
		/* Drain write buffer */
		"mcr p15, 0, %0, c7, c10, 4\n"
		/* Flush I,D TLBs */
		"mcr p15, 0, %0, c8, c7, 0\n"
		"mcr p15, 0, %0, c8, c6, 0\n"
		"mcr p15, 0, %0, c8, c5, 0\n"
		"mcr p15, 0, %0, c8, c3, 0\n"
		/* Instruction barrier */
		"mcr p15, 0, %0, c7, c5, 4\n"
		: : "r"(0));
}

void _start() __attribute__ ((weak, alias ("module_start")));

int module_start(SceSize argc, const void *args)
{
	LOG("TTBR test by xerpi\n");

	unsigned long ttbcr_n;
	unsigned long ttbr0_paddr;
	unsigned long ttbr0_vaddr;
	unsigned long ttbr1_paddr;
	unsigned long ttbr1_vaddr;

	ttbcr_n = get_ttbcr() & 7;

	LOG("TTBCR.N: %d\n", ttbcr_n);

	ttbr0_paddr = get_ttbr0() & ~((1 << (14 - ttbcr_n)) - 1);
	LOG("TTBR0 base pa: 0x%08X\n", ttbr0_paddr);
	find_paddr(ttbr0_paddr, 0, 0xFFFFFFFF, 0x1000, &ttbr0_vaddr);
	LOG("Found TTBR0 at va: 0x%08X\n", ttbr0_vaddr);
	LOG("TTBR0[0]: 0x%08X\n", *(unsigned int *)ttbr0_vaddr);
	LOG("\n");

	ttbr1_paddr = get_ttbr1() & ~((1 << 14) - 1);
	LOG("TTBR1 base pa: 0x%08X\n", ttbr1_paddr);
	find_paddr(ttbr1_paddr, 0, 0xFFFFFFFF, 0x1000, &ttbr1_vaddr);
	LOG("Found TTBR1 at va: 0x%08X\n", ttbr1_vaddr);
	LOG("TTBR1[0]: 0x%08X\n", *(unsigned int *)ttbr1_vaddr);
	LOG("\n");

	tai_module_info_t sysmem_taimodinfo;
	sysmem_taimodinfo.size = sizeof(sysmem_taimodinfo);
	taiGetModuleInfoForKernel(KERNEL_PID, "SceSysmem", &sysmem_taimodinfo);
	LOG("SceSysmem modid: 0x%08X\n", sysmem_taimodinfo.modid);

	tai_module_info_t processmgr_taimodinfo;
	processmgr_taimodinfo.size = sizeof(processmgr_taimodinfo);
	taiGetModuleInfoForKernel(KERNEL_PID, "SceProcessmgr", &processmgr_taimodinfo);
	LOG("SceProcessmgr modid: 0x%08X\n", processmgr_taimodinfo.modid);

	SceKernelModuleInfo sysmem_modinfo;
	sysmem_modinfo.size = sizeof(sysmem_modinfo);
	ksceKernelGetModuleInfo(KERNEL_PID, sysmem_taimodinfo.modid, &sysmem_modinfo);

	SceKernelModuleInfo processmgr_modinfo;
	processmgr_modinfo.size = sizeof(processmgr_modinfo);
	ksceKernelGetModuleInfo(KERNEL_PID, processmgr_taimodinfo.modid, &processmgr_modinfo);

	extern int SceProcessmgrForDriver_D141C076(SceUID pid, unsigned int *data);
	unsigned int data[32];
	SceProcessmgrForDriver_D141C076(0, data);
	//for (int i = 0; i < 32; i++)
	//	LOG("0x%08X\n", data[i]);

	void *processmgr_data = processmgr_modinfo.segments[1].vaddr;

	unsigned int *SceKernelPPM0_vaddr = *(unsigned int *)(processmgr_data + (0x00A76944 - 0x00A76000));
	unsigned int *SceKernelPPM1_vaddr = *(unsigned int *)(processmgr_data + (0x00A76948 - 0x00A76000));

	LOG("SceKernelPPM0_vaddr: %p, val: 0x%08X\n", SceKernelPPM0_vaddr, *SceKernelPPM0_vaddr);
	LOG("SceKernelPPM1_vaddr: %p, val: 0x%08X\n", SceKernelPPM1_vaddr, *SceKernelPPM1_vaddr);

	for (int i = 0; i < 4; i++) {
		SceKernelSegmentInfo *seg = &sysmem_modinfo.segments[i];
		if (!seg->vaddr)
			continue;

		LOG("Scanning segment %d, %p...\n", i, seg->vaddr);

		unsigned char *end = seg->vaddr + seg->memsz;
		for (unsigned char *j = seg->vaddr; j < end; j+= 4) {
			unsigned int val = *(unsigned int *)j;
			if (val == ttbr0_vaddr) {
				LOG("Found va at %p, seg %d offset 0x%08X\n", j, i, (unsigned int)j - (unsigned int)seg->vaddr);
			} else if (val == ttbr0_paddr) {
				LOG("Found pa at %p, seg %d offset 0x%08X\n", j, i, (unsigned int)j - (unsigned int)seg->vaddr);
			}
		}
	}

	for (int i = 0; i < 4; i++) {
		SceKernelSegmentInfo *seg = &processmgr_modinfo.segments[i];
		if (!seg->vaddr)
			continue;

		LOG("Scanning segment %d, %p...\n", i, seg->vaddr);

		unsigned char *end = seg->vaddr + seg->memsz;
		for (unsigned char *j = seg->vaddr; j < end; j+= 4) {
			unsigned int val = *(unsigned int *)j;
			if (val == ttbr0_vaddr) {
				LOG("Found va at %p, seg %d offset 0x%08X\n", j, i, (unsigned int)j - (unsigned int)seg->vaddr);
			} else if (val == ttbr0_paddr) {
				LOG("Found pa at %p, seg %d offset 0x%08X\n", j, i, (unsigned int)j - (unsigned int)seg->vaddr);
			}
		}
	}


	extern void *ksceKernelGetSysbase(void);

	unsigned int *sysbase = ksceKernelGetSysbase();
	LOG("sysbase: %p\n", sysbase);
	LOG("sysbase + 0x320: 0x%08X, 0x%08X\n",
		sysbase[0x320 / 4], *(unsigned int *)sysbase[0x320 / 4]);

	LOG("-------------------------------------\n");
	for (int i = 549; i < 560; i++) {
		LOG("sysbase[%d]: 0x%08X\n", i, sysbase[i]);
		if ((sysbase[i] == ttbr0_paddr) || (sysbase[i] == ttbr0_vaddr)) {
			LOG("Found 0x%08X at sysbase[%d] (addr %p)\n", sysbase[i], i, &sysbase[i]);
			for (unsigned j = sysbase; j < sysbase +1024; j += 4) {
				if (*(unsigned int *)j == sysbase[i]) {
					LOG("  0x%08X at 0x%08X\n", sysbase[i], j);
				}
			}
		}
	}

	for (int i = 0; i < 4; i++) {
		SceKernelSegmentInfo *seg = &sysmem_modinfo.segments[i];
		if (!seg->vaddr)
			continue;

		LOG("Scanning segment %d, %p...\n", i, seg->vaddr);

		unsigned char *end = seg->vaddr + seg->memsz;
		for (unsigned char *j = seg->vaddr; j < end; j+= 4) {
			unsigned int val = *(unsigned int *)j;
			if (val == sysbase[549]) {
				LOG("Found va at %p, seg %d offset 0x%08X\n", j, i, (unsigned int)j - (unsigned int)seg->vaddr);
			} else if (val == &sysbase[549]) {
				LOG("Found pa at %p, seg %d offset 0x%08X\n", j, i, (unsigned int)j - (unsigned int)seg->vaddr);
			}
		}
	}

	unsigned char *end = (unsigned char *)(0x00004000 + 0x00006000);
	for (unsigned char *j = (unsigned char *)0x00004000; j < end; j+= 4) {
		unsigned int val = *(unsigned int *)j;

		if (val == ttbr0_vaddr) {
			LOG("Found va at %p\n", j);
		} else if (val == ttbr0_paddr) {
			LOG("Found pa at %p\n", j);
		}
	}

	//first_page_table_paddr = (*(unsigned int *)ttbr0_vaddr) & 0xFFFFFC00;
	//find_paddr(first_page_table_paddr, 0, 0xFFFFFFFF, 0x1000, &first_page_table_vaddr);

	return SCE_KERNEL_START_SUCCESS;

}

int module_stop(SceSize argc, const void *args)
{

	return SCE_KERNEL_STOP_SUCCESS;
}
