#include <psp2kern/kernel/modulemgr.h>
#include <psp2kern/kernel/threadmgr.h>
#include <psp2kern/kernel/sysmem.h>
#include <psp2kern/kernel/cpu.h>
#include <psp2kern/udcd.h>
#include <psp2kern/display.h>
#include "iftu.h"
#include "log.h"
#include <taihen.h>

#define ALIGN(x, a) (((x) + ((a) - 1)) & ~((a) - 1))

#define RGBA8(r, g, b, a)      ((((a)&0xFF)<<24) | (((b)&0xFF)<<16) | (((g)&0xFF)<<8) | (((r)&0xFF)<<0))

#define RED   RGBA8(255, 0,   0,   255)
#define GREEN RGBA8(0,   255, 0,   255)
#define BLUE  RGBA8(0,   0,   255, 255)
#define CYAN  RGBA8(0,   255, 255, 255)
#define LIME  RGBA8(50,  205, 50,  255)
#define PURP  RGBA8(147, 112, 219, 255)
#define WHITE RGBA8(255, 255, 255, 255)
#define BLACK RGBA8(0,   0,   0,   255)

#define LOG(s, ...) \
	do { \
		char __buffer[256]; \
		snprintf(__buffer, sizeof(__buffer), s, ##__VA_ARGS__); \
	} while (0)

static int ioremap(uintptr_t paddr, unsigned int size, SceUID *uid, void **addr);
static SceUID ifturegA_uid[2], ifturegB_uid[2], iftucreg_uid[2];
static unsigned char *ifturegA_addr[2], *ifturegB_addr[2], *iftucreg_addr[2];
static unsigned char saved_ifturegA[2][0x1000];
static unsigned char saved_ifturegB[2][0x1000];
static unsigned char saved_iftucreg[2][0x1000];
static SceUID fb_uid[4];
static unsigned char *fb_addr[4];
static unsigned int fb_paddr[4];

#define SCREEN_PITCH 960
#define SCREEN_HEIGHT 544
#define FB_SIZE (SCREEN_PITCH * SCREEN_HEIGHT * 4)

static SceUID thread_id;
static int thread_run;

extern int ksceKernelMemcpyKernelToUserForPid(int pid, void *dst, const void *src, unsigned int size);
extern int ksceAppMgrIsExclusiveProcessRunning(const char *name);

static void save_iftu()
{
	ksceKernelCpuUnrestrictedMemcpy(saved_ifturegA[0], ifturegA_addr[0], 0x1000);
	ksceKernelCpuUnrestrictedMemcpy(saved_ifturegB[0], ifturegB_addr[0], 0x1000);
	ksceKernelCpuUnrestrictedMemcpy(saved_iftucreg[0], iftucreg_addr[0], 0x1000);
	/*ksceKernelCpuUnrestrictedMemcpy(saved_ifturegA[1], ifturegA_addr[1], 0x1000);
	ksceKernelCpuUnrestrictedMemcpy(saved_ifturegB[1], ifturegB_addr[1], 0x1000);
	ksceKernelCpuUnrestrictedMemcpy(saved_iftucreg[1], iftucreg_addr[1], 0x1000);*/
}

static void restore_iftu()
{
	ksceKernelCpuUnrestrictedMemcpy(ifturegA_addr[0], saved_ifturegA[0], 0x1000);
	ksceKernelCpuUnrestrictedMemcpy(ifturegB_addr[0], saved_ifturegB[0], 0x1000);
	ksceKernelCpuUnrestrictedMemcpy(iftucreg_addr[0], saved_iftucreg[0], 0x1000);
	/*ksceKernelCpuUnrestrictedMemcpy(ifturegA_addr[1], saved_ifturegA[1], 0x1000);
	ksceKernelCpuUnrestrictedMemcpy(ifturegB_addr[1], saved_ifturegB[1], 0x1000);
	ksceKernelCpuUnrestrictedMemcpy(iftucreg_addr[1], saved_iftucreg[1], 0x1000);*/
}

void fill_fb(void *addr, unsigned int color)
{
	int i, j;

	for (i = 0; i < SCREEN_HEIGHT; i++) {
		for (j = 0; j < SCREEN_PITCH; j++) {
			if (j % 64 < 32 || i % 64 < 32)
				*(unsigned int *)((char *)addr + i * SCREEN_PITCH * 4 + j * 4) = color;
			else
				*(unsigned int *)((char *)addr + i * SCREEN_PITCH * 4 + j * 4) = ~color | 0xFF000000;
		}
	}

	ksceKernelCpuDcacheAndL2WritebackRange(addr, FB_SIZE);
}

static int thread(SceSize args, void *argp)
{
	SceIftucRegs *iftu0creg = (SceIftucRegs *)iftucreg_addr[0];
	SceIftuRegs *iftu0regA = (SceIftuRegs *)ifturegA_addr[0];
	SceIftuRegs *iftu0regB = (SceIftuRegs *)ifturegB_addr[0];
	/*SceIftucRegs *iftu1creg = (SceIftucRegs *)iftucreg_addr[1];
	SceIftuRegs *iftu1regA = (SceIftuRegs *)ifturegA_addr[1];
	SceIftuRegs *iftu1regB = (SceIftuRegs *)ifturegB_addr[1];*/

	static unsigned int buff0[960*4];
	static unsigned int buff1[960*4];

	for (int i = 0; i < sizeof(buff0) / sizeof(*buff0); i++)
		buff0[i] = 0xFF0000FF;
	for (int i = 0; i < sizeof(buff1) / sizeof(*buff1); i++)
		buff1[i] = 0xFF00FF00;

	fill_fb(fb_addr[0], RED);
	fill_fb(fb_addr[1], GREEN);
	fill_fb(fb_addr[2], BLUE);
	fill_fb(fb_addr[3], PURP);

	unsigned int a = 0;

	save_iftu();

	while (thread_run) {
		int inc = a / 10000;

		iftu0regA->plane_config[0].dst_x = inc % 960;
		//iftu0regA->plane_config[1].dst_y = (int)(a / 10) % 544;
		iftu0creg->subctrl[0].cfg_select = 0;

		//iftu0regB->plane_config[0].dst_x = (int)(a / 10) % 960;
		iftu0regB->plane_config[0].dst_y = inc % 544;
		//iftu0regB->plane_config[1].dst_y = inc % 544;
		iftu0regB->plane_config[0].control = 0;
		iftu0creg->subctrl[1].cfg_select = 0;
		//iftu0regB->plane_config[1].control = 0;
		//iftu0regB->alpha_control = 1;
		//iftu0regB->crtc_mask |= 0b10;

		iftu0regA->plane_config[0].paddr = fb_paddr[0]; // Red
		iftu0regA->plane_config[1].paddr = fb_paddr[1]; // Green
		iftu0regB->plane_config[0].paddr = fb_paddr[2]; // Blue
		iftu0regB->plane_config[1].paddr = fb_paddr[3]; // Purple
		for (int i = 0; i <  SCREEN_PITCH; i++)
			*((unsigned int *)fb_addr[2] + i) = 0;
		//ksceKernelCpuDcacheAndL2WritebackRange(fb_addr[2], FB_SIZE);

		a++;
		/*SceDisplayFrameBufInfo fb[2];
		fb[0].size = sizeof(fb[0]);
		fb[1].size = sizeof(fb[1]);
		ksceDisplayGetFrameBufInfoForPid(-1, 0, 0, &fb[0]);
		ksceDisplayGetFrameBufInfoForPid(-1, 0, 1, &fb[1]);

		int exclusive = ksceAppMgrIsExclusiveProcessRunning(NULL);

		if (exclusive && fb[0].framebuf.base) {
			ksceKernelMemcpyKernelToUserForPid(fb[0].pid, fb[0].framebuf.base + 960*4*4, buff0, sizeof(buff0));
		}

		if (!exclusive && fb[1].framebuf.base) {
			ksceKernelMemcpyKernelToUserForPid(fb[1].pid, fb[1].framebuf.base, buff1, sizeof(buff1));
		}*/
	}

	restore_iftu();

	return 0;
}

void _start() __attribute__((weak, alias("module_start")));

int module_start(SceSize argc, const void *args)
{
	log_reset();

	LOG("iftutest by xerpi\n");

	ioremap(0xE5020000, 0x1000, &ifturegA_uid[0], (void **)&ifturegA_addr[0]);
	ioremap(0xE5021000, 0x1000, &ifturegB_uid[0], (void **)&ifturegB_addr[0]);
	ioremap(0xE5022000, 0x1000, &iftucreg_uid[0], (void **)&iftucreg_addr[0]);
	ioremap(0xE5030000, 0x1000, &ifturegA_uid[1], (void **)&ifturegA_addr[1]);
	ioremap(0xE5031000, 0x1000, &ifturegB_uid[1], (void **)&ifturegB_addr[1]);
	ioremap(0xE5032000, 0x1000, &iftucreg_uid[1], (void **)&iftucreg_addr[1]);

	LOG("Iftu0RegA addr: %p\n", ifturegA_addr[0]);
	LOG("Iftu0RegB addr: %p\n", ifturegB_addr[0]);
	LOG("Iftu0cReg addr: %p\n", iftucreg_addr[0]);
	LOG("Iftu1RegA addr: %p\n", ifturegA_addr[1]);
	LOG("Iftu1RegB addr: %p\n", ifturegB_addr[1]);
	LOG("Iftu1cReg addr: %p\n", iftucreg_addr[1]);

	const unsigned int fb_size_align = ALIGN(FB_SIZE, 256 * 1024);
	for (int i = 0; i < 4; i++) {
		fb_uid[i] = ksceKernelAllocMemBlock("fb", 0x40404006 , fb_size_align, NULL);
		ksceKernelGetMemBlockBase(fb_uid[i], (void **)&fb_addr[i]);

		ksceKernelGetPaddr(fb_addr[i], &fb_paddr[i]);
	}

	thread_run = 1;
	thread_id = ksceKernelCreateThread("thread", thread,
					   0x3C, 0x1000, 0, 0x10000, 0);
	ksceKernelStartThread(thread_id, 0, NULL);

	LOG("module_start done successfully!\n");

	return SCE_KERNEL_START_SUCCESS;
}

int module_stop(SceSize argc, const void *args)
{
	thread_run = 0;

	ksceKernelWaitThreadEnd(thread_id, NULL, NULL);
	ksceKernelDeleteThread(thread_id);

	ksceKernelFreeMemBlock(ifturegA_uid[0]);
	ksceKernelFreeMemBlock(ifturegB_uid[0]);
	ksceKernelFreeMemBlock(iftucreg_uid[0]);
	ksceKernelFreeMemBlock(ifturegA_uid[1]);
	ksceKernelFreeMemBlock(ifturegB_uid[1]);
	ksceKernelFreeMemBlock(iftucreg_uid[1]);

	for (int i = 0; i < 4; i++) {
		ksceKernelFreeMemBlock(fb_uid[i]);
	}

	return SCE_KERNEL_STOP_SUCCESS;
}

static int ioremap(uintptr_t paddr, unsigned int size, SceUID *uid, void **addr)
{
	int ret;
	SceUID mem_uid;
	unsigned int mem_size;
	void *mem_addr;

	mem_size = ALIGN(size, 4096);

	SceKernelAllocMemBlockKernelOpt opt;
	memset(&opt, 0, sizeof(opt));
	opt.size = sizeof(opt);
	opt.attr = 2;
	opt.paddr = paddr;
	mem_uid = ksceKernelAllocMemBlock("ioremap", 0x20100206, mem_size, &opt);
	if (mem_uid < 0)
		return mem_uid;

	ret = ksceKernelGetMemBlockBase(mem_uid, &mem_addr);
	if (ret < 0) {
		ksceKernelFreeMemBlock(mem_uid);
		return ret;
	}

	if (uid)
		*uid = mem_uid;
	if (addr)
		*addr = mem_addr;

	return 0;
}
