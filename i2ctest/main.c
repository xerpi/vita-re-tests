#include <stdio.h>
#include <psp2kern/kernel/cpu.h>
#include <psp2kern/kernel/modulemgr.h>
#include <psp2kern/kernel/threadmgr.h>
#include <psp2kern/kernel/sysmem.h>
#include <psp2kern/io/fcntl.h>
#include <psp2kern/display.h>
#include <psp2kern/lowio/i2c.h>
#include <psp2kern/lowio/gpio.h>
#include "utils.h"
#include "log.h"
#include "draw.h"

#define LOG(s, ...) \
	do { \
		char buffer[256]; \
		snprintf(buffer, sizeof(buffer), s, ##__VA_ARGS__); \
		uart_print(0, buffer); \
		console_print(buffer); \
	} while (0)

static SceUID thid = -1;
static volatile int run = 1;

extern int ksceUartReadAvailable(int device);
extern int ksceUartWrite(int device, unsigned char data);
extern int ksceUartRead(int device);
extern int ksceUartInit(int device);

extern int ScePervasiveForDriver_18DD8043(int device);
extern int ScePervasiveForDriver_788B6C61(int device);
extern int ScePervasiveForDriver_A7CE7DCC(int device);
extern int ScePervasiveForDriver_EFD084D8(int device);

extern int SceSysconForDriver_62155962(int);
extern int SceSysconForDriver_FF86F4C5(void);

static void uart_print(int bus, const char *str)
{
	while (*str) {
		ksceUartWrite(bus, *str);
		if (*str == '\n')
			ksceUartWrite(bus, '\r');
		str++;
	}
}

/*static const unsigned char used_addrs[] = {
	0x18, 0x34, 0x74, 0x70, 0x7A, 0x7C, 0xD2
};*/

#define HDMI_I2C_BUS		1
#define HDMI_I2C_ADDR		0x7A
#define HDMI_I2C_CEC_ADDR	0x7C

static unsigned char hdmi_i2c_cmd_write_read_1(unsigned char addr, unsigned char reg)
{
	unsigned char write_buffer = reg;
	unsigned char read_buffer;

	ksceI2cTransferWriteRead(HDMI_I2C_BUS,
		addr, &write_buffer, sizeof(write_buffer),
		addr, &read_buffer, sizeof(read_buffer));

	return read_buffer;
}

static int thread(SceSize args, void *argp)
{
/*
I2C test by xerpi
0: 0x00000000
0x42: 0x00000030
0xD6: 0x00000048
0xE1: 0x0000007C
0xFB: 0x000000AA
0x80: 0x00000004
0x81: 0x00000007
0x82: 0x00000013
0x83: 0x00000057
0x7F: 0x00000000


1: 0x00000000
0x42: 0x00000070
0xD6: 0x00000048
0xE1: 0x0000007C
0xFB: 0x000000AA
0x80: 0x00000004
0x81: 0x00000007
0x82: 0x00000013
0x83: 0x00000057
0x7F: 0x00000000
*/

	/*LOG("0: 0x%08X\n", SceSysconForDriver_62155962(0));
	ksceKernelDelayThread(1000 * 1000);

	LOG("0x42: 0x%08X\n", hdmi_i2c_cmd_write_read_1(HDMI_I2C_ADDR, 0x42));
	LOG("0xD6: 0x%08X\n", hdmi_i2c_cmd_write_read_1(HDMI_I2C_ADDR, 0xD6));
	LOG("0xE1: 0x%08X\n", hdmi_i2c_cmd_write_read_1(HDMI_I2C_ADDR, 0xE1));
	LOG("0xFB: 0x%08X\n", hdmi_i2c_cmd_write_read_1(HDMI_I2C_ADDR, 0xFB));
	LOG("0x80: 0x%08X\n", hdmi_i2c_cmd_write_read_1(HDMI_I2C_CEC_ADDR, 0x80));
	LOG("0x81: 0x%08X\n", hdmi_i2c_cmd_write_read_1(HDMI_I2C_CEC_ADDR, 0x81));
	LOG("0x82: 0x%08X\n", hdmi_i2c_cmd_write_read_1(HDMI_I2C_CEC_ADDR, 0x82));
	LOG("0x83: 0x%08X\n", hdmi_i2c_cmd_write_read_1(HDMI_I2C_CEC_ADDR, 0x83));
	LOG("0x7F: 0x%08X\n", hdmi_i2c_cmd_write_read_1(HDMI_I2C_CEC_ADDR, 0x7F));
	LOG("\n");
	ksceKernelDelayThread(1000 * 1000);
	LOG("\n1: 0x%08X\n", SceSysconForDriver_62155962(1));
	ksceKernelDelayThread(1000 * 1000);
	LOG("0x42: 0x%08X\n", hdmi_i2c_cmd_write_read_1(HDMI_I2C_ADDR, 0x42));
	LOG("0xD6: 0x%08X\n", hdmi_i2c_cmd_write_read_1(HDMI_I2C_ADDR, 0xD6));
	LOG("0xE1: 0x%08X\n", hdmi_i2c_cmd_write_read_1(HDMI_I2C_ADDR, 0xE1));
	LOG("0xFB: 0x%08X\n", hdmi_i2c_cmd_write_read_1(HDMI_I2C_ADDR, 0xFB));
	LOG("0x80: 0x%08X\n", hdmi_i2c_cmd_write_read_1(HDMI_I2C_CEC_ADDR, 0x80));
	LOG("0x81: 0x%08X\n", hdmi_i2c_cmd_write_read_1(HDMI_I2C_CEC_ADDR, 0x81));
	LOG("0x82: 0x%08X\n", hdmi_i2c_cmd_write_read_1(HDMI_I2C_CEC_ADDR, 0x82));
	LOG("0x83: 0x%08X\n", hdmi_i2c_cmd_write_read_1(HDMI_I2C_CEC_ADDR, 0x83));
	LOG("0x7F: 0x%08X\n", hdmi_i2c_cmd_write_read_1(HDMI_I2C_CEC_ADDR, 0x7F));
	LOG("\n");
	ksceKernelDelayThread(1000 * 1000);*/

	int y = console_get_y();

	while (run) {
		console_set_y(y);

		LOG("0x3E: 0x%08X\n", hdmi_i2c_cmd_write_read_1(HDMI_I2C_ADDR, 0x3E));

		//hdmi_i2c_cmd_write_read_1(HDMI_I2C_ADDR, 0x42, &data);

		/*
		Main:
		0x42: 0x00000070
		0xD6: 0x00000048
		0xE1: 0x0000007C
		0xFB: 0x000000AA
		CEC:
		0x80: 0x00000004
		0x81: 0x00000007
		0x82: 0x00000013
		0x83: 0x00000057
		0x7F: 0x00000000
		*/

		/*LOG("0x42: 0x%08X\n", hdmi_i2c_cmd_write_read_1(HDMI_I2C_ADDR, 0x42));
		LOG("0xD6: 0x%08X\n", hdmi_i2c_cmd_write_read_1(HDMI_I2C_ADDR, 0xD6));
		LOG("0xE1: 0x%08X\n", hdmi_i2c_cmd_write_read_1(HDMI_I2C_ADDR, 0xE1));
		LOG("0xFB: 0x%08X\n", hdmi_i2c_cmd_write_read_1(HDMI_I2C_ADDR, 0xFB));
		LOG("0x80: 0x%08X\n", hdmi_i2c_cmd_write_read_1(HDMI_I2C_CEC_ADDR, 0x80));
		LOG("0x81: 0x%08X\n", hdmi_i2c_cmd_write_read_1(HDMI_I2C_CEC_ADDR, 0x81));
		LOG("0x82: 0x%08X\n", hdmi_i2c_cmd_write_read_1(HDMI_I2C_CEC_ADDR, 0x82));
		LOG("0x83: 0x%08X\n", hdmi_i2c_cmd_write_read_1(HDMI_I2C_CEC_ADDR, 0x83));
		LOG("0x7F: 0x%08X\n", hdmi_i2c_cmd_write_read_1(HDMI_I2C_CEC_ADDR, 0x7F));
		LOG("\n");*/

		ksceKernelDelayThread(500 * 1000);

		//unsigned char data[] = {0xD6, 0b00 << 6};
		//ksceI2cTransferWrite(HDMI_I2C_BUS, HDMI_I2C_ADDR, data, sizeof(data));

		/*for (int i = 0; i < 32; i++)
			ksceGpioPortClear(0, 1 << i);*/
	}


	return 0;
}

void _start() __attribute__ ((weak, alias ("module_start")));

int module_start(SceSize argc, const void *args)
{
	ScePervasiveForDriver_EFD084D8(0); // Turn on clock
	ScePervasiveForDriver_A7CE7DCC(0); // Out of reset

	ksceUartInit(0);

	//log_reset();
	map_framebuffer();
	LOG("I2C test by xerpi\n");

	// SceSysconForDriver_FF86F4C5: 0x01030603
	LOG("SceSysconForDriver_FF86F4C5: 0x%08X\n", SceSysconForDriver_FF86F4C5());

	thid = ksceKernelCreateThread("i2ctest", thread, 0x00, 0x1000, 0, 1 << 0, 0);

	ksceKernelStartThread(thid, 0, NULL);

	return SCE_KERNEL_START_SUCCESS;

}

int module_stop(SceSize argc, const void *args)
{
	run = 0;
	ksceKernelWaitThreadEnd(thid, NULL, NULL);

	unmap_framebuffer();

	return SCE_KERNEL_STOP_SUCCESS;
}

int ioremap(uintptr_t paddr, unsigned int size, SceUID *uid, void **addr)
{
	int ret;
	SceUID mem_uid;
	unsigned int mem_size;
	void *mem_addr;

	mem_size = ALIGN(size, 4096);

	SceKernelAllocMemBlockKernelOpt opt;
	memset(&opt, 0, sizeof(opt));
	opt.size = sizeof(opt);
	opt.attr = 2;
	opt.paddr = paddr;
	mem_uid = ksceKernelAllocMemBlock("ioremap", 0x20100206, mem_size, &opt);
	if (mem_uid < 0)
		return mem_uid;

	ret = ksceKernelGetMemBlockBase(mem_uid, &mem_addr);
	if (ret < 0) {
		ksceKernelFreeMemBlock(mem_uid);
		return ret;
	}

	if (uid)
		*uid = mem_uid;
	if (addr)
		*addr = mem_addr;

	return 0;
}

int alloc_phycont(unsigned int size, SceUID *uid, void **addr)
{
	int ret;
	SceUID mem_uid;
	unsigned int mem_size;
	void *mem_addr;

	mem_size = ALIGN(size, 4096);

	SceKernelAllocMemBlockKernelOpt opt;
	memset(&opt, 0, sizeof(opt));
	opt.size = sizeof(opt);
	opt.attr = 0x200004;
	opt.alignment = 0x1000;
	mem_uid = ksceKernelAllocMemBlock("phycont", 0x30808006, mem_size, &opt);
	if (mem_uid < 0)
		return mem_uid;

	ret = ksceKernelGetMemBlockBase(mem_uid, &mem_addr);
	if (ret < 0) {
		ksceKernelFreeMemBlock(mem_uid);
		return ret;
	}

	if (uid)
		*uid = mem_uid;
	if (addr)
		*addr = mem_addr;

	return 0;
}
