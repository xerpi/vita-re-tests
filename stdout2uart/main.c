#include <psp2kern/kernel/debug.h>
#include <psp2kern/kernel/modulemgr.h>
#include <psp2kern/kernel/uart.h>
#include <psp2kern/lowio/pervasive.h>

static int uart0_putchar(void *args, char c)
{
	ksceUartWrite(0, c);

	if (c == '\n')
		ksceUartWrite(0, '\r');

	return 0;
}

void _start() __attribute__ ((weak, alias ("module_start")));

int module_start(SceSize argc, const void *args)
{
	kscePervasiveUartClockEnable(0);
	kscePervasiveUartResetDisable(0);

	ksceUartInit(0);

	ksceKernelRegisterDebugPutcharHandler(uart0_putchar, NULL);

	return SCE_KERNEL_START_SUCCESS;
}
