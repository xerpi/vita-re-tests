#include <stdio.h>
#include <stdint.h>

enum vita_type {
	VITA_TYPE_1000,
	VITA_TYPE_2000,
	VITA_TYPE_PSTV
};

struct modelinfo {
	enum vita_type vita_type;
	uint16_t model;
	uint16_t device_type;
	uint16_t device_config;
	uint16_t type;
	uint32_t unkd4;
};

static const struct modelinfo modelinfo_db[] = {
	// Vita type      Model Devtype  Devcfg   Type     unkD4
	{VITA_TYPE_1000, 0x0100, 0x0501, 0x1000, 0x0300, 0x00406000},
	{VITA_TYPE_1000, 0x0100, 0x0501, 0x1000, 0x0300, 0x00406002},
	{VITA_TYPE_1000, 0x0100, 0x0401, 0x1000, 0x0300, 0x00406000},
	{VITA_TYPE_1000, 0x0100, 0x0401, 0x1000, 0x0300, 0x00406002},
	{VITA_TYPE_1000, 0x0100, 0x0201, 0x1000, 0x0300, 0x00406000},
	{VITA_TYPE_2000, 0x0100, 0x0501, 0x1400, 0x0300, 0x00805038},
	{VITA_TYPE_2000, 0x0100, 0x0301, 0x1400, 0x0300, 0x00805038},
	{VITA_TYPE_PSTV, 0x0100, 0x0701, 0x0102, 0x0300, 0x00703030},
	{VITA_TYPE_PSTV, 0x0100, 0x0401, 0x0202, 0x0300, 0x00723030},
};

int sysroot_model_is_unk(const struct modelinfo *info);
int sysroot_model_is_dolce(const struct modelinfo *info);

int sysroot_model_is_vita(const struct modelinfo *info)
{
	const uint16_t device_type = __builtin_bswap16(info->device_type);

	if ((device_type - 0x100 > 0x11) || sysroot_model_is_dolce(info)) {
		const uint32_t unkd4_mask = info->unkd4 & 0xFF0000;

		if (!sysroot_model_is_unk(info))
			return 0;

		if ((unkd4_mask == 0x700000) || (unkd4_mask == 0x720000) || (unkd4_mask == 0x510000))
			return 0;
	}

	return 1;
}

int sysroot_model_is_dolce(const struct modelinfo *info)
{
	const uint16_t device_config = __builtin_bswap16(info->device_config);
	const uint16_t device_type = __builtin_bswap16(info->device_type);
	const uint32_t unkd4_mask = info->unkd4 & 0xFF0000;

	if (device_type == 0x101 && device_config == 0x408)
		return 1;

	if (device_config & 0x200)
		return 1;

	if (!sysroot_model_is_unk(info))
		return 0;

	if ((unkd4_mask == 0x700000) || (unkd4_mask == 0x720000) || (unkd4_mask == 0x510000))
		return 1;

	return 0;
}

int sysroot_model_is_vita2k(const struct modelinfo *info)
{
	const uint32_t unkd4_mask = info->unkd4 & 0xFF0000;

	if (sysroot_model_is_dolce(info))
		return 0;

	if (unkd4_mask == 0x800000)
		return 1;

	return 0;
}

int sysroot_model_is_unk(const struct modelinfo *info)
{
	const uint16_t device_config = __builtin_bswap16(info->device_config);
	const uint16_t device_type = __builtin_bswap16(info->device_type);
	const uint16_t type = __builtin_bswap16(info->type);

	if (device_type != 0x103 || device_config != 0x10)
		return 0;

	if (type <= 0x24)
		return 1;

	return 0;
}

int main()
{
	for (int i = 0; i < sizeof(modelinfo_db) / sizeof(modelinfo_db[0]); i++) {
		int is_vita = sysroot_model_is_vita(&modelinfo_db[i]);
		int is_dolce = sysroot_model_is_dolce(&modelinfo_db[i]);
		int is_2000 = sysroot_model_is_vita2k(&modelinfo_db[i]);
		int is_unk = sysroot_model_is_unk(&modelinfo_db[i]);

		printf("%s: is Vita: %d, is Dolce: %d, is 2000: %d, is Unk: %d\n",
			(modelinfo_db[i].vita_type == VITA_TYPE_1000) ? "Vita 1000"
				: (modelinfo_db[i].vita_type == VITA_TYPE_2000) ? "Vita 2000"
					: "Vita TV  ",
			is_vita, is_dolce, is_2000, is_unk);

	}

	return 0;
}
