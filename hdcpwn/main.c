#include <psp2kern/kernel/modulemgr.h>
#include <psp2kern/lowio/i2c.h>

/*
* http://www.analog.com/media/en/technical-documentation/user-guides/ADV7511_Programming_Guide.pdf
*/

void _start() __attribute__ ((weak, alias ("module_start")));

int module_start(SceSize argc, const void *args)
{
	unsigned char read_buff;
	unsigned char write_buff = 0xAF;

	ksceI2cTransferWriteRead(1, 0x7A, &write_buff, sizeof(write_buff),
				    0x7A, &read_buff, sizeof(read_buff));

	unsigned char buffer[] = {
		0xAF,
		read_buff & ~((1 << 7) | (1 << 4))
	};

	ksceI2cTransferWrite(1, 0x7A, buffer, sizeof(buffer));

	return SCE_KERNEL_START_SUCCESS;
}

int module_stop(SceSize argc, const void *args)
{
	return SCE_KERNEL_STOP_SUCCESS;
}
