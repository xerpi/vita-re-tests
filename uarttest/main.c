#include <psp2kern/kernel/cpu.h>
#include <psp2kern/kernel/modulemgr.h>
#include <psp2kern/kernel/threadmgr.h>
#include <psp2kern/kernel/sysmem.h>
#include <psp2kern/io/fcntl.h>
#include <psp2kern/display.h>
#include "utils.h"
#include "log.h"
#include "draw.h"

#define LOG(s, ...) \
	do { \
		char buffer[256]; \
		snprintf(buffer, sizeof(buffer), s, ##__VA_ARGS__); \
		console_print(buffer); \
	} while (0)

static SceUID thid = -1;
static volatile int run = 1;

extern int ksceUartInit(int port);
extern int ksceUartReadAvailable(int port);
extern int ksceUartRead(int port);
extern int ksceUartWrite(int port, unsigned char data);
extern int kscePervasiveUartSetBaudrate(int port, unsigned int baudrate);

extern int kscePervasiveUartClockDisable(int device);
extern int kscePervasiveUartResetEnable(int device);
extern int kscePervasiveUartResetDisable(int device);
extern int kscePervasiveUartClockEnable(int device);

int ioremap(uintptr_t paddr, unsigned int size, SceUID *uid, void **addr);

#define UART_REGS(base, i) ((void *)((uintptr_t)(base) + (i) * 0x10000))
#define UARTCLKGEN_REGS(base, i) ((void *)((uintptr_t)(base) + (i) * 4))

static unsigned int uart_read_fifo_data_available(void *base, int device)
{
	return ((unsigned int *)UART_REGS(base, device))[0x1A] & 0x3F;
}

static void uart_wait_ready(void *base, int device)
{
	volatile unsigned int *ptr = UART_REGS(base, device);
        while (!(ptr[0xA] & 0x200))
		asm volatile("dmb sy\n\t");
}

unsigned int uart_read(void *base, int device)
{
        unsigned int result;

	volatile unsigned int *ptr = UART_REGS(base, device);

	while (!(ptr[0x1A] << 0x1A))
		asm volatile("dmb sy\n\t");
        result = ptr[0x1E];
        ptr[0x15] = 0x77F;

        return result;
}

void uart_write(void *base, int device, unsigned int data)
{
	volatile unsigned int *ptr = UART_REGS(base, device);

	while (!(ptr[0xA] & 0x100))
		asm volatile("dmb sy\n\t");

        ptr[0x1C] = data;
}

static void uart_print(int bus, const char *str)
{
	while (*str)
		ksceUartWrite(bus, *str++);
}

#if 0
static int thread(SceSize args, void *argp)
{
	int i;
	SceUID uart_regs_uid;
	SceUID uartclkgen_regs_uid;
	void *uart_base_addr;
	void *uartclkgen_base_addr;

	ioremap(0xE2030000, 0x70000, &uart_regs_uid, &uart_base_addr);
	ioremap(0xE3105000, 0x1000, &uartclkgen_regs_uid, &uartclkgen_base_addr);

	LOG("SceUartReg addr: %p\n", uart_base_addr);
	LOG("SceUartClkgenReg addr: %p\n", uartclkgen_base_addr);

	for (i = 0; i < 7; i++) {
		kscePervasiveUartClockEnable(i); // Turn on clock
		kscePervasiveUartResetDisable(i); // Out of reset
	}

	for (i = 0; i < 7; i++) {
		int j;
		volatile unsigned int *uart_regs = UART_REGS(uart_base_addr, i);
		volatile unsigned int *uartclkgen_regs = UARTCLKGEN_REGS(uartclkgen_base_addr, i);
#if 1
		uart_regs[1] = 0; // disable device
		asm volatile("dmb sy\n\t");

		*uartclkgen_regs = 0x1001A; // Baudrate = 115200

		uart_regs[8] = 3;
		uart_regs[4] = 1;
		uart_regs[0xC] = 0;
		uart_regs[0x18] = 0x303;
		uart_regs[0x10] = 0;
		uart_regs[0x14] = 0;
		uart_regs[0x19] = 0x10001;

		asm volatile("dmb sy\n\t");
		uart_regs[1] = 1; // enable device
		asm volatile("dmb sy\n\t");

		uart_wait_ready(uart_base_addr, i);

		LOG("Device %d, init manually.\n", i);
#else
		//int ret = ksceUartInit(i);
		//LOG("Device %d, init: %d\n", i, ret);
#endif
	}

	int y = console_get_y();

	while (run) {
		console_set_y(y);

		for (i = 0; i < 7; i++) {
			LOG("Uart %d\n", i);
			ksceUartWrite(i, '0' + i);
			ksceUartWrite(i, '\n');
			ksceUartWrite(i, '\r');
			//int ret = ksceUartWrite(i, 'A');
			//ksceUartWrite(i, '\n');
			//LOG("Write %i: 0x%08X\n", i, ret);
			//uart_write(uart_base_addr, i, 0xAA);
			//LOG("Write manually %i\n", i);
			if (ksceUartReadAvailable(i) > 0)
				LOG("Read %i: 0x%08X\n", i, ksceUartRead(i));
		}

		/*for (i = 0; i < 7; i++) {
			//if (i == 5)
			//	continue;

			int available = ksceUartReadAvailable(i);
			LOG("%i Read available: 0x%08X\n", i, available);

			if (available > 0)
				LOG("  %i Read: 0x%08X\n", i, ksceUartRead(i));

			//ksceUartWrite(i, 0xAA);
		}*/

		ksceKernelDelayThread(25 * 1000);
	}

	ksceKernelFreeMemBlock(uart_regs_uid);
	ksceKernelFreeMemBlock(uartclkgen_regs_uid);

	for (i = 0; i < 7; i++) {
		kscePervasiveUartResetEnable(i); // Reset
		kscePervasiveUartClockDisable(i); // Turn off clock
	}

	return 0;
}
#endif

static int thread(SceSize args, void *argp)
{
	ksceKernelDelayThread(250 * 1000);

	kscePervasiveUartClockEnable(0); // Turn on clock
	kscePervasiveUartResetDisable(0); // Out of reset

	ksceUartInit(0);
	//kscePervasiveUartSetBaudrate(0, 9600);

	while (1) {
		/*if (ksceUartReadAvailable(0) > 0) {
			LOG("UART0 read: %c\n", ksceUartRead(0));
		}*/

		uart_print(0, "Hi from the PSVita!\n\r");
	}

	kscePervasiveUartResetEnable(0); // Reset
	kscePervasiveUartClockDisable(0); // Turn off clock

	return 0;
}

void _start() __attribute__ ((weak, alias ("module_start")));

int module_start(SceSize argc, const void *args)
{
	//log_reset();
	//map_framebuffer();
	//LOG("UART test by xerpi\n");

	thid = ksceKernelCreateThread("uarttest", thread, 0x00, 0x1000, 0, 1 << 0, 0);

	ksceKernelStartThread(thid, 0, NULL);

	return SCE_KERNEL_START_SUCCESS;

}

int module_stop(SceSize argc, const void *args)
{
	run = 0;
	ksceKernelWaitThreadEnd(thid, NULL, NULL);

	return SCE_KERNEL_STOP_SUCCESS;
}

int ioremap(uintptr_t paddr, unsigned int size, SceUID *uid, void **addr)
{
	int ret;
	SceUID mem_uid;
	unsigned int mem_size;
	void *mem_addr;

	mem_size = ALIGN(size, 4096);

	SceKernelAllocMemBlockKernelOpt opt;
	memset(&opt, 0, sizeof(opt));
	opt.size = sizeof(opt);
	opt.attr = 2;
	opt.paddr = paddr;
	mem_uid = ksceKernelAllocMemBlock("ioremap", 0x20100206, mem_size, &opt);
	if (mem_uid < 0)
		return mem_uid;

	ret = ksceKernelGetMemBlockBase(mem_uid, &mem_addr);
	if (ret < 0) {
		ksceKernelFreeMemBlock(mem_uid);
		return ret;
	}

	if (uid)
		*uid = mem_uid;
	if (addr)
		*addr = mem_addr;

	return 0;
}

int alloc_phycont(unsigned int size, SceUID *uid, void **addr)
{
	int ret;
	SceUID mem_uid;
	unsigned int mem_size;
	void *mem_addr;

	mem_size = ALIGN(size, 4096);

	SceKernelAllocMemBlockKernelOpt opt;
	memset(&opt, 0, sizeof(opt));
	opt.size = sizeof(opt);
	opt.attr = 0x200004;
	opt.alignment = 0x1000;
	mem_uid = ksceKernelAllocMemBlock("phycont", 0x30808006, mem_size, &opt);
	if (mem_uid < 0)
		return mem_uid;

	ret = ksceKernelGetMemBlockBase(mem_uid, &mem_addr);
	if (ret < 0) {
		ksceKernelFreeMemBlock(mem_uid);
		return ret;
	}

	if (uid)
		*uid = mem_uid;
	if (addr)
		*addr = mem_addr;

	return 0;
}
