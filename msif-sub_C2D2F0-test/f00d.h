#ifndef F00D_H
#define F00D_H

#define SCE_KERNEL_SYSROOT_SELF_INDEX_RMAUTH_SM 1

typedef struct SceSblSmCommContext130 {
	uint32_t unk_0;
	uint32_t self_type; // 2 - user = 1 / kernel = 0
	char data0[0x90]; //hardcoded data
	char data1[0x90];
	uint32_t pathId; // 2 (2 = os0)
	uint32_t unk_12C;
} SceSblSmCommContext130;

typedef struct SceSblSmCommPair {
	uint32_t unk_0;
	uint32_t unk_4;
} SceSblSmCommPair;

typedef struct SceSblSmCommMsifData {
	unsigned int unk00;
	unsigned int unk04;
	unsigned int unk08;
	unsigned int unk0C;
	unsigned int unk10;
	unsigned int unk14;
	unsigned int unk18;
	unsigned int unk1C;
} SceSblSmCommMsifData;

int sblcomm_start_rmauth_sm(int *rmauth_sm_id);
int f00d_rmauth_sm_cmd_1(int *res);
int f00d_rmauth_sm_cmd_2(const uint32_t seed[8]);

#endif
