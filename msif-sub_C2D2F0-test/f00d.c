#include <psp2kern/kernel/sysmem.h>
#include <psp2kern/sblauthmgr.h>
#include <string.h>
#include "f00d.h"

extern int ksceSblSmCommStartSmFromData(int priority, const char *elf_data, int elf_size, int num1, SceSblSmCommContext130 *ctx, int *id);
extern int ksceSblSmCommStopSm(int id, SceSblSmCommPair *res);
extern int ksceSblSmCommCallFunc(int id, int command_id, int *f00d_resp, const void *data, int size);

static const unsigned char ctx_130_data[0x90] = {
	0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x08, 0x28, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x00, 0x00, 0x00,
	0xC0, 0x00, 0xF0, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF,
	0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x09,
	0x80, 0x03, 0x00, 0x00, 0xC3, 0x00, 0x00, 0x00, 0x80, 0x09,
	0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0xFF,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00
};

int sblcomm_start_rmauth_sm(int *rmauth_sm_id)
{
	int ret;
	SceKernelSysrootSelfInfo self_info;
	SceSblSmCommContext130 smcomm_ctx;

	memset(&self_info, 0, sizeof(self_info));
	self_info.size = sizeof(self_info);

	ret = ksceSysrootGetSelfInfo(SCE_KERNEL_SYSROOT_SELF_INDEX_RMAUTH_SM, &self_info);
	if (ret < 0)
		return ret;

	memset(&smcomm_ctx, 0, sizeof(smcomm_ctx));
	memcpy(smcomm_ctx.data0, ctx_130_data, 0x90);
	smcomm_ctx.pathId = 2;
	smcomm_ctx.self_type = (smcomm_ctx.self_type & 0xFFFFFFF0) | 2;

	ret = ksceSblSmCommStartSmFromData(0, self_info.self_data,
					   self_info.self_size, 0,
					   &smcomm_ctx, rmauth_sm_id);
	if (ret < 0)
		return ret;

	return 0;
}

int f00d_rmauth_sm_cmd_1(int *res)
{
	int ret;
	int rmauth_sm_id;
	int f00d_resp;
	SceSblSmCommPair stop_res;
	SceSblSmCommMsifData data;

	ret = sblcomm_start_rmauth_sm(&rmauth_sm_id);
	if (ret < 0)
		return ret;

	ret = ksceSblSmCommCallFunc(rmauth_sm_id, 1, &f00d_resp, &data, 0x20);
	if (ret < 0)
		return ret;

	ret = ksceSblSmCommStopSm(rmauth_sm_id, &stop_res);
	if (ret < 0)
		return ret;

	*res = data.unk10;

	return f00d_resp;
}

int f00d_rmauth_sm_cmd_2(const uint32_t seed[8])
{
	int ret;
	int rmauth_sm_id;
	int f00d_resp;
	SceSblSmCommPair stop_res;
	uint32_t buffer[8];

	ret = sblcomm_start_rmauth_sm(&rmauth_sm_id);
	if (ret < 0)
		return ret;

	memcpy(buffer, seed, sizeof(buffer));
	ret = ksceSblSmCommCallFunc(rmauth_sm_id, 2, &f00d_resp, buffer, 0x20);
	if (ret < 0)
		return ret;

	ret = ksceSblSmCommStopSm(rmauth_sm_id, &stop_res);
	if (ret < 0)
		return ret;

	return f00d_resp;
}
