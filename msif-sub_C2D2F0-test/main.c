#include <psp2kern/kernel/modulemgr.h>
#include <psp2kern/kernel/threadmgr.h>
#include <psp2kern/kernel/sysmem.h>
#include <psp2kern/kernel/cpu.h>
#include <psp2kern/io/fcntl.h>
#include <psp2kern/sblauthmgr.h>
#include <psp2kern/syscon.h>
#include <psp2kern/uart.h>
#include <psp2kern/lowio/pervasive.h>
#include <taihen.h>
#include "f00d.h"
#include "log.h"
#include "draw.h"

#define MS_LOG 1
#define FB_LOG 1

#if MS_LOG
#define MS_LOG_FUNC(buff) log_write(buff, strlen(buff))
#else
#define MS_LOG_FUNC(buff) (void)0
#endif

#if FB_LOG
#define FB_LOG_FUNC(buff) console_print(buff)
#else
#define FB_LOG_FUNC(buff) (void)0
#endif

#define LOG(s, ...) \
	do { \
		static char __buffer[256]; \
		snprintf(__buffer, sizeof(__buffer), s, ##__VA_ARGS__); \
		uart_print(0, __buffer); \
		FB_LOG_FUNC(__buffer); \
		MS_LOG_FUNC(__buffer); \
	} while (0)

static void uart_print(int bus, const char *str);

extern int module_get_offset(SceUID pid, SceUID modid, int segidx, size_t offset, uintptr_t *addr);

static tai_module_info_t SceMsif_modinfo;

void _start() __attribute__((weak, alias("module_start")));

typedef struct verify_hash_ctx {
	unsigned char *r;
	unsigned char *s;
} verify_hash_ctx;

int (*sub_C2D2F0)(void *out, const void *in, int size) = NULL;
int (*f00d_cmd_2_rmauth_sm_C2D988)(const void *buff) = NULL;
int (*block_memcmp_C2EADC)(void *buffer0, void *buffer1, int nBlocks) = NULL;
int (*block_is_zero_or_memcmp_C2E3EE)(void *buffer0, void *buffer1, int nBlocks) = NULL;
void (*reverse_byte_order_C2E3AA)(void *dst, const void* src, int byte_size) = NULL;
void (*sub_C2EB0A)(void *dst, const void *src, int block_size, int bits) = NULL;
void (*bigint_mod_C2E084)(unsigned char *sha224_0, unsigned char *sha224_1, int key_size_blocks0, unsigned char *sha224_2, int key_size_blocks1) = NULL;
int (*ecdsa_verify_C8DA14)(verify_hash_ctx *ctx,
			 unsigned char secret_key[0x1C],
			 unsigned char *dec_ptr_pair[2],
			 unsigned char *dec_ptr_table[6],
			 int key_size_blocks,
			 int key_size_bytes) = NULL;
void (*bigint_mod_C8E084)(unsigned char *dst, unsigned char *src0, int block_size_arg0, unsigned char *src1, int block_size_arg1) = NULL;
int (*bigint_inv_mod_C8DBD4)(unsigned char *sha224_0, unsigned char *sha_224_1, unsigned char *sha_224_2, int key_size_blocks) = NULL;
int (*bigint_mul_C8DF74)(unsigned char *sha_224_0,
				 unsigned char *sha_224_1,
				 int key_size_blocks0,
				 unsigned char *sha_224_2,
				 int key_size_blocks1) = NULL;
int (*bigint_muladd_C8E420)(unsigned char *sha_224_0[3],
				 unsigned char *sha_224_1[3],
				 unsigned char *sha_224_2[3],
				 unsigned char *sha_224_3,
				 unsigned char *sha_224_4,
				 unsigned char *sha_224_5,
				 unsigned char *sha_224_6,
				 int key_size_blocks) = NULL;


static SceUID SceSblSsMgrForDriver_sceSblSsMgrDES64ECBEncryptForDriver_hook_uid = -1;
static tai_hook_ref_t SceSblSsMgrForDriver_sceSblSsMgrDES64ECBEncryptForDriver_ref;

#define GET_LR() \
	({ \
		unsigned int lr; \
		asm volatile("mov %0, lr\n\t" : "=r"(lr)); \
		lr; \
	})

static int module_get_code_offset_thumb(SceUID modid, size_t offset, void *addr)
{
	int ret;
	uintptr_t func_addr;

	ret = module_get_offset(KERNEL_PID, modid, 0, offset, &func_addr);
	if (addr)
		*(uintptr_t *)addr = func_addr + 1;

	return ret;
}

static int module_get_data_offset(SceUID modid, size_t offset, void *addr)
{
	int ret;
	uintptr_t offset_addr;

	ret = module_get_offset(KERNEL_PID, modid, 1, offset, &offset_addr);
	if (addr)
		*(uintptr_t *)addr = offset_addr;

	return ret;
}

static inline void print_hex(const char *name, const void *buff, int size)
{
	int i;

	LOG(name);
	for (i = 0; i < size; i++)
		LOG(" %02X", ((unsigned char *)buff)[i]);
	LOG("\n");
}

void dmac5_submit(uint32_t dst_pa, uint32_t src_pa, uint32_t len, uint32_t dmac_cmd, int keyslot)
{
	SceUID reg_memuid;
	volatile uint32_t *reg_addr;
	SceKernelAllocMemBlockKernelOpt opt;

	memset(&opt, 0, sizeof(opt));
	opt.size = sizeof(opt);
	opt.attr = SCE_KERNEL_ALLOC_MEMBLOCK_ATTR_HAS_PADDR;
	opt.paddr = 0xE0410000;
	reg_memuid = ksceKernelAllocMemBlock("SceDmacmgrDmac5Reg", 0x20100206, 0x1000, &opt);

	ksceKernelGetMemBlockBase(reg_memuid, (void **)&reg_addr);
	//DBG("DMAC5 reg block: 0x%08X | va: %p\n", reg_memuid, reg_addr);

	#define DMAC_COMMIT_WAIT \
		do { \
			reg_addr[10] = reg_addr[10]; \
			reg_addr[7] = 1; \
			while(reg_addr[9] & 1) \
				; \
		} while (0)

	reg_addr[0] = (uint32_t)src_pa;
	reg_addr[1] = (uint32_t)dst_pa;
	reg_addr[2] = len;
	reg_addr[3] = dmac_cmd;
	reg_addr[4] = keyslot;
	reg_addr[5] = 0;
	// reg_addr[8] = 0;
	reg_addr[11] = 0xE070;
	reg_addr[12] = 0x700070;
	DMAC_COMMIT_WAIT;
	asm volatile ("dmb sy");
	asm volatile ("dsb sy");

	ksceKernelFreeMemBlock(reg_memuid);
}

static int SceSblSsMgrForDriver_sceSblSsMgrDES64ECBEncryptForDriver_hook_func(char *src, char *dst, int size, int slot_id, int key_size, int mask_enable)
{
	int ret;

	LOG("sceSblSsMgrDES64ECBEncryptForDriver called from 0x%08X\n", GET_LR() - 5);

	ret = TAI_CONTINUE(int, SceSblSsMgrForDriver_sceSblSsMgrDES64ECBEncryptForDriver_ref, src, dst, size, slot_id, key_size, mask_enable);

	print_hex("  src:", src, size);
	print_hex("  dst:", dst, size);
	LOG("  size: 0x%X, key_size: %d\n", size, key_size);

	return ret;
}

static SceUID ecdsa_verify_C8DA14_hook_uid = -1;
static tai_hook_ref_t ecdsa_verify_C8DA14_hook_ref;

static SceUID bigint_mod_C8E084_hook_uid = -1;
static tai_hook_ref_t bigint_mod_C8E084_hook_ref;

static SceUID bigint_inv_mod_C8DBD4_hook_uid = -1;
static tai_hook_ref_t bigint_inv_mod_C8DBD4_hook_ref;

static SceUID bigint_muladd_C8E420_hook_uid = -1;
static tai_hook_ref_t bigint_muladd_C8E420_hook_ref;

static int ecdsa_verify_C8DA14_hook_func(verify_hash_ctx *ctx,
			 unsigned char e[0x1C],
			 unsigned char *dec_ptr_pair[2],
			 unsigned char *dec_ptr_table[6],
			 int key_size_blocks,
			 int key_size_bytes)
{

	int ret, i;

	LOG("ecdsa_verify_C8DA14:\n");
	print_hex("  r:", ctx->r, 0x1C);
	print_hex("  s:", ctx->s, 0x1C);
	print_hex("  e:", e, 0x1C);
	LOG("\n");

	for (i = 0; i < 2; i++)
		print_hex("  pair:", dec_ptr_pair[i], 0x1C);

	LOG("\n");

	for (i = 0; i < 6; i++)
		print_hex("  table:", dec_ptr_table[i], 0x1C);

	ret = TAI_CONTINUE(int, ecdsa_verify_C8DA14_hook_ref, ctx, e, dec_ptr_pair, dec_ptr_table, key_size_blocks, key_size_bytes);

	LOG("ecdsa_verify_C8DA14 returned: %d\n", ret);

	return ret;
}

static int bigint_mod_C8E084_hook_func(unsigned char *dst, unsigned char *src, int op_size, unsigned char *q, int q_size)
{
	int ret;

	LOG("bigint_mod_C8E084:\n");
	print_hex("  src:", src, 0x1C);
	print_hex("  q:", q, 0x1C);

	uart_print(0, "bbbbbbbbbbb\n");

	ret = TAI_CONTINUE(int, bigint_mod_C8E084_hook_ref, dst, src, op_size, q, q_size);

	uart_print(0, "aaaaaaaaaaaa\n");

	print_hex("  dst:", dst, 0x1C);

	return ret;
}

int bigint_inv_mod_C8DBD4_hook_func(unsigned char *dst, unsigned char *src, unsigned char *q, int key_size_blocks)
{
	int ret;

	LOG("bigint_inv_mod_C8DBD4:\n");
	print_hex("  src:", src, 0x1C);
	print_hex("  q:", q, 0x1C);

	ret = TAI_CONTINUE(int, bigint_inv_mod_C8DBD4_hook_ref, dst, src, q, key_size_blocks);

	print_hex("  dst:", dst, 0x1C);

	return ret;
}

int bigint_muladd_C8E420_hook_func(unsigned char *dst[3], unsigned char *src1[3], unsigned char *src2[3],
	unsigned char *u1, unsigned char *u2, unsigned char *G, unsigned char *Q, int key_size_blocks)
{
	int ret;

	LOG("bigint_muladd_C8E420_hook_func:\n");
	print_hex("  src1[0]:", src1[0], 0x1C);
	print_hex("  src1[1]:", src1[1], 0x1C);
	print_hex("  src1[2]:", src1[2], 0x1C);
	print_hex("  src2[0]:", src2[0], 0x1C);
	print_hex("  src2[1]:", src2[1], 0x1C);
	print_hex("  src2[2]:", src2[2], 0x1C);
	print_hex("  u1:", u1, 0x1C);
	print_hex("  u2:", u2, 0x1C);
	print_hex("  G:", G, 0x1C);
	print_hex("  Q:", Q, 0x1C);

	ret = TAI_CONTINUE(int, bigint_muladd_C8E420_hook_ref, dst, src1, src2, u1, u2, G, Q, key_size_blocks);

	print_hex("  dst[0]:", dst[0], 0x1C);
	print_hex("  dst[1]:", dst[1], 0x1C);
	print_hex("  dst[2]:", dst[2], 0x1C);

	return ret;
}

int module_start(SceSize argc, const void *args)
{
	int i;

	kscePervasiveUartClockEnable(0);
	kscePervasiveUartResetDisable(0);
	ksceUartInit(0);

#if MS_LOG
	log_reset();
#endif
#if FB_LOG
	map_framebuffer();
#endif

	LOG("msif-sub_C2D2F0-test by xerpi\n\n");

	/*SceMsif_modinfo.size = sizeof(SceMsif_modinfo);
	taiGetModuleInfoForKernel(KERNEL_PID, "SceMsif", &SceMsif_modinfo);

	module_get_code_offset_thumb(SceMsif_modinfo.modid, 0xC2D2F0 - 0xC28000, &sub_C2D2F0);
	module_get_code_offset_thumb(SceMsif_modinfo.modid, 0xC2D988 - 0xC28000, &f00d_cmd_2_rmauth_sm_C2D988);
	module_get_code_offset_thumb(SceMsif_modinfo.modid, 0xC2EADC - 0xC28000, &block_memcmp_C2EADC);
	module_get_code_offset_thumb(SceMsif_modinfo.modid, 0xC2E3EE - 0xC28000, &block_is_zero_or_memcmp_C2E3EE);
	module_get_code_offset_thumb(SceMsif_modinfo.modid, 0xC2E3AA - 0xC28000, &reverse_byte_order_C2E3AA);
	module_get_code_offset_thumb(SceMsif_modinfo.modid, 0xC2EB0A - 0xC28000, &sub_C2EB0A);
	module_get_code_offset_thumb(SceMsif_modinfo.modid, 0xC2E084 - 0xC28000, &bigint_mod_C2E084);

	module_get_code_offset_thumb(SceMsif_modinfo.modid, 0xC8DA14 - 0xC88000, &ecdsa_verify_C8DA14);
	module_get_code_offset_thumb(SceMsif_modinfo.modid, 0xC8E084 - 0xC88000, &bigint_mod_C8E084);
	module_get_code_offset_thumb(SceMsif_modinfo.modid, 0xC8DBD4 - 0xC88000, &bigint_inv_mod_C8DBD4);
	module_get_code_offset_thumb(SceMsif_modinfo.modid, 0xC8DF74 - 0xC88000, &bigint_mul_C8DF74);
	module_get_code_offset_thumb(SceMsif_modinfo.modid, 0xC8E420 - 0xC88000, &bigint_muladd_C8E420);

	ecdsa_verify_C8DA14_hook_uid = taiHookFunctionOffsetForKernel(KERNEL_PID,
		&ecdsa_verify_C8DA14_hook_ref, SceMsif_modinfo.modid, 0,
		0xC8DBC0 - 0xC88000, 1, ecdsa_verify_C8DA14_hook_func);*/

	/*bigint_mod_C8E084_hook_uid = taiHookFunctionOffsetForKernel(KERNEL_PID,
		&bigint_mod_C8E084_hook_ref, SceMsif_modinfo.modid, 0,
		0xC8E084 - 0xC88000, 1, bigint_mod_C8E084_hook_func);

	bigint_inv_mod_C8DBD4_hook_uid = taiHookFunctionOffsetForKernel(KERNEL_PID,
		&bigint_inv_mod_C8DBD4_hook_ref, SceMsif_modinfo.modid, 0,
		0xC8DBD4 - 0xC88000, 1, bigint_inv_mod_C8DBD4_hook_func);

	bigint_muladd_C8E420_hook_uid = taiHookFunctionOffsetForKernel(KERNEL_PID,
		&bigint_muladd_C8E420_hook_ref, SceMsif_modinfo.modid, 0,
		0xC8E420 - 0xC88000, 1, bigint_muladd_C8E420_hook_func);*/

	/*static unsigned int a[0x100], b[0x100], c[0x100];
	memset(a, 0, sizeof(a));
	memset(b, 0, sizeof(b));
	memset(c, 0, sizeof(c));

	bigint_mod_C8E084(a, b, 7, c, 7);*/

/*
	tai_module_info_t SceKernelThreadMgr_modinfo;
	SceKernelThreadMgr_modinfo.size = sizeof(SceKernelThreadMgr_modinfo);
	taiGetModuleInfoForKernel(KERNEL_PID, "SceKernelThreadMgr", &SceKernelThreadMgr_modinfo);

	void *addr = NULL;
	module_get_data_offset(SceKernelThreadMgr_modinfo.modid, 0, &addr);

	for (SceClass **cls = *((void **)addr) + 0x2330; cls != *((void **)addr) + 0x2394; cls++) {
		LOG("offset: 0x%08X, name: %s, itemsize: 0x%X\n", (uintptr_t)cls - (uintptr_t)*((void **)addr),
			(*cls)->name, (*cls)->itemsize);
	}
*/

	tai_module_info_t SceSysmem_modinfo;
	SceSysmem_modinfo.size = sizeof(SceSysmem_modinfo);
	taiGetModuleInfoForKernel(KERNEL_PID, "SceSysmem", &SceSysmem_modinfo);

	unsigned char *addr = NULL;
	module_get_data_offset(SceSysmem_modinfo.modid, 0, (void **)&addr);
	unsigned char *start = (void *)addr;

	LOG("data start: %p\n", addr);

	for (addr += 0xD4; addr < start + 0x7604; addr += 4) {
		SceClass **cls = (SceClass **)addr;

		uintptr_t paddr;
		if (ksceKernelGetPaddr(*cls, &paddr) < 0)
			continue;

		if (ksceKernelGetPaddr((*cls)->name, &paddr) < 0)
			continue;

		LOG("offset: 0x%08X, name: %s, itemsize: 0x%X\n", (uintptr_t)addr - (uintptr_t)start,
			(*cls)->name, (*cls)->itemsize);
	}

	return SCE_KERNEL_START_SUCCESS;

	/*static unsigned char buff[4096] __attribute__((aligned(4096)));
	extern int SceMsifForDriver_get_sha224_digest_source(unsigned char *buff);

	extern int ksceMsifInit1(void);
	ksceSysconCtrlMsPower(0);
	kscePervasiveMsifResetEnable();
	kscePervasiveMsifClockDisable();
	ksceKernelDelayThread(1000 * 1000);
	kscePervasiveMsifClockEnable();
	kscePervasiveMsifResetDisable();
	ksceSysconCtrlMsPower(1);

	ksceMsifInit1();
	ksceKernelDelayThread(10000 * 1000);*/

	/* Procedure, according to RFC 6090, "KT-I". q denotes the group
	   order.
		1. Check 0 < r, s < q.
		2. s' <-- s^{-1}  (mod q)
		3. u1  <-- h * s' (mod q)
		4. u2  <-- r * s' (mod q)
		5. R = u1 G + u2 Y
		6. Signature is valid if R_x = r (mod q).
	*/

	/*bigint_mod_C8E084(pointer_table1[0], pointer_table1[0], dec_ptr_table_item0_rev);
	bigint_mod_C8E084(pointer_table1[1], pointer_table1[1], dec_ptr_table_item0_rev);

	bigint_inv_mod_C8DBD4(s, s, q);
	bigint_mul_C8DF74(u1, h, s);
	bigint_mod_C8E084(u1, u1, 2 * key_size_blocks, q);
	bigint_mul_C8DF74(u2, r, s);
	bigint_mod_C8E084(u2, u2, 2 * key_size_blocks, q);
	bigint_muladd_C8E420(R, pointer_table0, pointer_table1, u1, u2, G, Q);
	bigint_mod_C8E084(R, R, q);*/


/*int (*ecdsa_verify_C8DA14)(verify_hash_ctx *ctx,
			 unsigned char secret_key[0x1C],
			 unsigned char *dec_ptr_pair[2],
			 unsigned char *dec_ptr_table[6],
			 int key_size_blocks,
			 int key_size_bytes) = NULL;
void (*bigint_mod_C8E084)(unsigned char *dst, unsigned char *src0, int block_size_arg0, unsigned char *src1, int block_size_arg1) = NULL;
int (*bigint_inv_mod_C8DBD4)(unsigned char *dst, unsigned char *src, unsigned char *sha_224_2, int key_size_blocks) = NULL;
int (*bigint_mul_C8DF74)(unsigned char *dst,
				 unsigned char *src0,
				 int key_size_blocks0,
				 unsigned char *src1,
				 int key_size_blocks1) = NULL;
int (*bigint_muladd_C8E420)(unsigned char *dst[3],
				 unsigned char *src1[3],
				 unsigned char *src2[3],
				 unsigned char *u1,
				 unsigned char *u2,
				 unsigned char *G,
				 unsigned char *Q,
				 int key_size_blocks) = NULL;*/

	/* Block memcmp test */

	/*unsigned int array0[32], array1[32], array2[32];
	memset(array0, 0, sizeof(array0));
	memset(array1, 1, sizeof(array1));
	memset(array2, 2, sizeof(array2));
	LOG("0 vs 1: %d, %d\n", block_memcmp_C2EADC(array0, array1, 32), block_is_zero_or_memcmp_C2E3EE(array0, array1, 32));
	LOG("1 vs 1: %d, %d\n", block_memcmp_C2EADC(array1, array1, 32), block_is_zero_or_memcmp_C2E3EE(array1, array1, 32));
	LOG("1 vs 2: %d, %d\n", block_memcmp_C2EADC(array1, array2, 32), block_is_zero_or_memcmp_C2E3EE(array1, array2, 32));
	LOG("2 vs 1: %d, %d\n", block_memcmp_C2EADC(array2, array1, 32), block_is_zero_or_memcmp_C2E3EE(array2, array1, 32));

	unsigned char in[8] = {
		0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88
	};
	unsigned char out[8];
	reverse_byte_order_C2E3AA(out, in, sizeof(out));
	print_hex("In:  ", in, sizeof(in));
	print_hex("Out: ", out, sizeof(out));*/


	/*for (i = 0; i <= 0x40; i++) {
		//unsigned char in[8] = {0x01, 0x02, 0x03, 0x04, 0x00, 0x00, 0x00, 0x00};
		unsigned char in[8] = {0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8};
		unsigned char out[32];

		memset(out, 0, sizeof(out));

		sub_C2EB0A(out, in, 4, i);

		LOG("Bits: 0x%02X\n", i);
		print_hex("  In:  ", in, sizeof(in));
		print_hex("  Out: ", out, sizeof(out));
	}*/

	/*unsigned char out[0x1C], in1[0x1C], in2[0x1C];
	memset(out, 0x00, sizeof(out));
	memset(in1, 0, sizeof(in1));
	memset(in2, 0, sizeof(in2));

	in1[0] = 1;
	in2[0] = 5;

	bigint_mod_C2E084(out, in1, 0x1C, in2, 7);

	print_hex("out: ", out, sizeof(out));
	print_hex("in1: ", in1, sizeof(in1));
	print_hex("in2: ", in2, sizeof(in2));*/

	//ksceSysconCtrlMsPower(1);

	return SCE_KERNEL_START_SUCCESS;

	/* 3DES test */

	SceSblSsMgrForDriver_sceSblSsMgrDES64ECBEncryptForDriver_hook_uid = taiHookFunctionImportForKernel(KERNEL_PID,
		&SceSblSsMgrForDriver_sceSblSsMgrDES64ECBEncryptForDriver_ref, "SceMsif", 0x61E9428D, 0x37DD5CBF,
		SceSblSsMgrForDriver_sceSblSsMgrDES64ECBEncryptForDriver_hook_func);


	LOG("sub_C2D2F0: %p\n", sub_C2D2F0);
	LOG("f00d_cmd_2_rmauth_sm_C2D988: %p\n", f00d_cmd_2_rmauth_sm_C2D988);
	LOG("\n");

	#define LEN 40

	unsigned char src[LEN], dst[8];

	static const uint32_t seed1[] = {
		0x11111111, 0x11111111, 0x11111111, 0x11111111
	};

	static const uint32_t seed2[] = {
		0x22222222, 0x22222222, 0x22222222, 0x22222222
	};

	memset(src, 0, sizeof(src));
	memset(dst, 0, sizeof(dst));
	//for (i = 0; i < LEN; i++)
	//	src[i] = ((i + 1) << 4) | (i + 1);

	//f00d_cmd_2_rmauth_sm_C2D988(seed1);
	f00d_rmauth_sm_cmd_2(seed1);

	sub_C2D2F0(dst, src, sizeof(src));

	print_hex("Seed 1:", seed1, sizeof(seed1));
	print_hex("Input:", src, sizeof(src));
	print_hex("Output:", dst, sizeof(dst));

	LOG("\n");

	memset(src, 0, sizeof(src));
	memset(dst, 0, sizeof(dst));
	//for (i = 0; i < LEN; i++)
	//	src[i] = ((i + 1) << 4) | (i + 1);

	//f00d_cmd_2_rmauth_sm_C2D988(seed2);
	f00d_rmauth_sm_cmd_2(seed2);

	sub_C2D2F0(dst, src, sizeof(src));

	print_hex("Seed 2:", seed2, sizeof(seed2));
	print_hex("Input:", src, sizeof(src));
	print_hex("Output:", dst, sizeof(dst));

	return SCE_KERNEL_START_SUCCESS;
}

int module_stop(SceSize argc, const void *args)
{
	LOG("module_stop()\n");

#if FB_LOG
	unmap_framebuffer();
#endif

	if (SceSblSsMgrForDriver_sceSblSsMgrDES64ECBEncryptForDriver_hook_uid > 0) {
		taiHookReleaseForKernel(SceSblSsMgrForDriver_sceSblSsMgrDES64ECBEncryptForDriver_hook_uid,
			SceSblSsMgrForDriver_sceSblSsMgrDES64ECBEncryptForDriver_ref);
	}

	if (ecdsa_verify_C8DA14_hook_uid > 0) {
		taiHookReleaseForKernel(ecdsa_verify_C8DA14_hook_uid,
			ecdsa_verify_C8DA14_hook_ref);
	}

	return SCE_KERNEL_STOP_SUCCESS;
}

static void uart_print(int bus, const char *str)
{
	while (*str) {
		ksceUartWrite(bus, *str);
		if (*str == '\n')
			ksceUartWrite(bus, '\r');
		str++;
	}
}
