// Example program
#include <iostream>
#include <cstring>
#include <cstdio>
#include <cstdlib>


//[REVERSED]
void process_block_invert_head(unsigned int& lo, unsigned int& hi, unsigned char value)
{
	lo = value << 8;
	hi = 0;
}

//[REVERSED]
void process_block_invert_body(unsigned int& lo, unsigned int& hi, unsigned char value)
{
	unsigned int v1 = hi | 0;
	unsigned int v2 = v1 << 8;
	unsigned int v4 = lo | value;
	hi = v2 | (v4 >> 24);
	lo = v4 << 8;
}

//[REVERSED]
void process_block_invert_tail(unsigned int& lo, unsigned int& hi, unsigned char value)
{
	lo = lo | value;
	hi = hi | 0;
}

//--------

//[REVERSED]
std::uint32_t adds(std::uint32_t left, std::uint32_t right, std::uint32_t* carry)
{
	std::uint64_t l64 = left;
	std::uint64_t r64 = right;
	std::uint64_t res64 = l64 + r64;

	if((res64 & 0x0000000100000000) > 0)
		*carry = 1;
	else
		*carry = 0;

	return (std::uint32_t)res64;
}

//[REVERSED]
std::uint32_t adcs(std::uint32_t left, std::uint32_t right, std::uint32_t* carry)
{
	std::uint64_t l64 = left;
	std::uint64_t r64 = right;
	std::uint64_t res64 = l64 + r64 + *carry;

	if((res64 & 0x0000000100000000) > 0)
		*carry = 1;
	else
		*carry = 0;

	return (std::uint32_t)res64;
}

//--------

//[REVERSED]
void process_back_inverse_body(unsigned int& lo, unsigned int& hi, unsigned char value)
{
	unsigned int v1 = lo | value;
	unsigned int v2 = hi << 8;
	unsigned int v3 = v2 | (v1 >> 24);
	lo = v1 << 8;
	hi = v3 | 0;
}

//[REVERSED]
void process_back_inverse_head(unsigned int& lo, unsigned int& hi, unsigned char value)
{
	hi = 0;
	lo = 0;

	process_back_inverse_body(lo, hi, value);
}

//[REVERSED]
void process_back_inverse_tail(unsigned int& lo, unsigned int& hi, unsigned char value)
{
	lo = lo | value;
}

//--------

//[REVERSED]
int w_dmac5_command_0x41_bit_magic_C8D2F0_orig(unsigned int* some_buffer, unsigned int* tweak0_res, unsigned int* tweak1_res)
{
	//---- reverse input buffer ----

	unsigned int lo0;
	unsigned int hi0;
	process_block_invert_head(lo0, hi0, ((unsigned char*)some_buffer)[0]);
	process_block_invert_body(lo0, hi0, ((unsigned char*)some_buffer)[1]);
	process_block_invert_body(lo0, hi0, ((unsigned char*)some_buffer)[2]);
	process_block_invert_body(lo0, hi0, ((unsigned char*)some_buffer)[3]);
	process_block_invert_body(lo0, hi0, ((unsigned char*)some_buffer)[4]);
	process_block_invert_body(lo0, hi0, ((unsigned char*)some_buffer)[5]);
	process_block_invert_body(lo0, hi0, ((unsigned char*)some_buffer)[6]);
	process_block_invert_tail(lo0, hi0, ((unsigned char*)some_buffer)[7]);

	//---- carry add input buffer ----

	std::uint32_t carry0 = 0;
	std::uint32_t au0 = adds(lo0, lo0, &carry0); //technically this is a multiplication by 2
	std::uint32_t bu0 = adcs(hi0, hi0, &carry0); //technically this is a multiplication by 2 (but also with carry)

	unsigned char tweak0[8];

	tweak0[0] = (bu0 >> 24) & 0xFF;
	tweak0[1] = (bu0 >> 16) & 0xFF;
	tweak0[2] = (bu0 >>  8) & 0xFF;
	tweak0[3] = (bu0 >>  0) & 0xFF;

	tweak0[4] = (au0 >> 24) & 0xFF;
	tweak0[5] = (au0 >> 16) & 0xFF;
	tweak0[6] = (au0 >>  8) & 0xFF;

	unsigned char value00 = ((char*)some_buffer)[0];
	tweak0[7] = ((value00 & 0x80) > 0) ? (au0 ^ 0x1B)
					   : (au0 & 0xFF);

	//---- back reverse buffer ----

	unsigned int hi1;
	unsigned int lo1;
	process_back_inverse_head(lo1, hi1, tweak0[0]);
	process_back_inverse_body(lo1, hi1, tweak0[1]);
	process_back_inverse_body(lo1, hi1, tweak0[2]);
	process_back_inverse_body(lo1, hi1, tweak0[3]);
	process_back_inverse_body(lo1, hi1, tweak0[4]);
	process_back_inverse_body(lo1, hi1, tweak0[5]);
	process_back_inverse_body(lo1, hi1, tweak0[6]);
	process_back_inverse_tail(lo1, hi1, tweak0[7]);

	//---- carry add buffer ----

	std::uint32_t carry1 = 0;
	std::uint32_t au1 = adds(lo1, lo1, &carry1); //technically this is a multiplication by 2
	std::uint32_t bu1 = adcs(hi1, hi1, &carry1); //technically this is a multiplication by 2 (but also with carry)

	//---- retrieve bytes

	unsigned char d[4];
	d[0] = (unsigned char)(au1);
	d[1] = (unsigned char)(au1 >>  8);
	d[2] = (unsigned char)(au1 >> 16);
	d[3] = (unsigned char)(au1 >> 24);

	unsigned char e[4];
	e[0] = (unsigned char)(bu1);
	e[1] = (unsigned char)(bu1 >>  8);
	e[2] = (unsigned char)(bu1 >> 16);
	e[3] = (unsigned char)(bu1 >> 24);

	unsigned char f[4];
	f[0] = (unsigned char)(bu1);
	f[1] = (unsigned char)(bu1 <<  8);
	f[2] = (unsigned char)(bu1 << 16);
	f[3] = (unsigned char)(bu1 << 24);

	//---- looks like all this code does is inverting the values (except for 0x1B handling) ----

	unsigned char tweak1[8];

	tweak1[0] = e[3];
	tweak1[1] = e[2];
	tweak1[2] = e[1];
	tweak1[3] = e[0];

	tweak1[4] = d[3] | f[1];
	tweak1[5] = d[2] | f[2];
	tweak1[6] = d[1] | f[3];

	unsigned char value01 = tweak0[0];
	tweak1[7] = ((value01 & 0x80) > 0) ? (d[0] ^ 0x1B)
					   :  d[0];

	//---- copy results ----

	memcpy((char*)tweak0_res, (char*)tweak0, 8);
	memcpy((char*)tweak1_res, (char*)tweak1, 8);

	return 0;
}

void byte_array_to_uint64_t(unsigned char* x, std::uint64_t* y)
{
   std::uint64_t c0 = (((std::uint64_t)x[0]) << 56);
   std::uint64_t c1 = (((std::uint64_t)x[1]) << 48);
   std::uint64_t c2 = (((std::uint64_t)x[2]) << 40);
   std::uint64_t c3 = (((std::uint64_t)x[3]) << 32);
   std::uint64_t c4 = (((std::uint64_t)x[4]) << 24);
   std::uint64_t c5 = (((std::uint64_t)x[5]) << 16);
   std::uint64_t c6 = (((std::uint64_t)x[6]) <<  8);
   std::uint64_t c7 = (((std::uint64_t)x[7]) <<  0);
   *y = c0 | c1 | c2 | c3 | c4 | c5 | c6 | c7;
}

void uint64_t_to_byte_array(std::uint64_t x, unsigned char* y)
{
   (y)[0] = (unsigned char)((x) >> 56);
   (y)[1] = (unsigned char)((x) >> 48);
   (y)[2] = (unsigned char)((x) >> 40);
   (y)[3] = (unsigned char)((x) >> 32);
   (y)[4] = (unsigned char)((x) >> 24);
   (y)[5] = (unsigned char)((x) >> 16);
   (y)[6] = (unsigned char)((x) >> 8);
   (y)[7] = (unsigned char)(x);
}

//[REVERSED]
int w_dmac5_command_0x41_bit_magic_C8D2F0(unsigned char* tweak_input, unsigned char* tweak0_res, unsigned char* tweak1_res)
{
   //first round - multiply by 2

   std::uint64_t i64_0;
   byte_array_to_uint64_t(tweak_input, &i64_0);

   std::uint64_t mul64_0 = i64_0 * 2;

   uint64_t_to_byte_array(mul64_0, tweak0_res);

   tweak0_res[7] = ((tweak_input[0] & 0x80) > 0) ? (tweak0_res[7] ^ 0x1B)
                                                 :  tweak0_res[7];

   //second round - multiply by 2

   std::uint64_t i64_1;
   byte_array_to_uint64_t(tweak0_res, &i64_1);

   std::uint64_t mul64_1 = i64_1 * 2;

   uint64_t_to_byte_array(mul64_1, tweak1_res);

   tweak1_res[7] = ((tweak0_res[0] & 0x80) > 0) ? (tweak1_res[7] ^ 0x1B)
                                                :  tweak1_res[7];

   return 0;
}

int main()
{
	std::uint32_t in0[2] = {0xA0030001, 0x80000000};
	std::uint32_t res00[2], res01[2];

	std::uint32_t in1[2] = {0xA0030001, 0x80000000};
	std::uint32_t res10[2], res11[2];

	w_dmac5_command_0x41_bit_magic_C8D2F0_orig((unsigned int *)in0,
		(unsigned int *)res00, (unsigned int *)res01);

	printf("in[0]:   0x%08X, in[1]:   0x%08X\n", in0[0], in0[1]);
	printf("res0[0]: 0x%08X, res0[1]: 0x%08X\n", res00[0], res00[1]);
	printf("res1[0]: 0x%08X, res1[1]: 0x%08X\n", res01[0], res01[1]);

	w_dmac5_command_0x41_bit_magic_C8D2F0((unsigned char *)in1,
		(unsigned char *)res10, (unsigned char *)res11);

	printf("\n");

	printf("in[0]:   0x%08X, in[1]:   0x%08X\n", in1[0], in1[1]);
	printf("res0[0]: 0x%08X, res0[1]: 0x%08X\n", res10[0], res10[1]);
	printf("res1[0]: 0x%08X, res1[1]: 0x%08X\n", res11[0], res11[1]);
}
