#include <psp2kern/kernel/modulemgr.h>
#include <psp2kern/kernel/threadmgr.h>
#include <psp2kern/kernel/sysmem.h>
#include <psp2kern/lowio/pervasive.h>
#include <psp2kern/uart.h>

static SceUID thid;
static int run = 1;

static int thread(SceSize args, void *argp)
{
	int i = 0;

	ksceKernelDelayThread(250 * 1000);

	//for (i = 0; i < 7; i++) {
		kscePervasiveUartClockEnable(i); // Turn on clock
		kscePervasiveUartResetDisable(i); // Out of reset
		kscePervasiveUartSetBaudrate(i, 115200);
		ksceUartInit(i);
	//}

	while (run) {
		//for (i = 0; i < 7; i++) {
			ksceUartWrite(i, '0' + i);
			ksceUartWrite(i, '\n');
			ksceUartWrite(i, '\r');

			ksceKernelDelayThread(10 * 1000);
		//}
	}

	kscePervasiveUartResetEnable(0); // Reset
	kscePervasiveUartClockDisable(0); // Turn off clock

	return 0;
}

void _start() __attribute__ ((weak, alias ("module_start")));

int module_start(SceSize argc, const void *args)
{
	thid = ksceKernelCreateThread("uarttest", thread, 0x00, 0x1000, 0, 1 << 0, 0);

	ksceKernelStartThread(thid, 0, NULL);

	return SCE_KERNEL_START_SUCCESS;

}

int module_stop(SceSize argc, const void *args)
{
	run = 0;
	ksceKernelWaitThreadEnd(thid, NULL, NULL);

	return SCE_KERNEL_STOP_SUCCESS;
}
