.arch armv7a

.section .vitalink.fstubs.SceModulemgrForKernel,"ax",%progbits

	.align 4
	.global ksceKernelUmountBootfs
	.type ksceKernelUmountBootfs, %function
ksceKernelUmountBootfs:
.if GEN_WEAK_EXPORTS
	.word 0x00000008
.else
	.word 0x00000000
.endif //GEN_WEAK_EXPORTS
	.word 0x92C9FFC2
	.word 0xBD61AD4D
	.align 4

