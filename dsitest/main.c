#include <psp2kern/kernel/modulemgr.h>
#include <psp2kern/kernel/sysmem.h>
#include <psp2kern/kernel/debug.h>
#include <psp2kern/kernel/sysclib.h>
#include <psp2kern/lowio/dsi.h>
#include <psp2kern/lowio/pervasive.h>

#define ALIGN(x, a) (((x) + ((a) - 1)) & ~((a) - 1))

#define printf ksceKernelPrintf

static SceUID thid;
static SceUID dsiregs_uid;
static void *dsiregs_addr = NULL;

static int ioremap(uintptr_t paddr, unsigned int size, SceUID *uid, void **addr);

static int thread(SceSize args, void *argp)
{
	static const int head = 1;
	int ret;

	printf("thread started\n");

	ret = ksceDsiGetPixelClock(head);
	printf("ksceDsiGetPixelClock: %d\n", ret);

	ret = kscePervasiveDsiSetPixelClock(head, ret);
	printf("kscePervasiveDsiSetPixelClock: %d\n", ret);

	ret = kscePervasiveDsiClockEnable(head, 0xf);
	printf("kscePervasiveDsiClockEnable: %d\n", ret);

	ret = kscePervasiveDsiResetDisable(head, 7);
	printf("kscePervasiveDsiResetDisable: %d\n", ret);

	//ret = ksceDsiEnableHead(head);
	//printf("ksceDsiEnableHead: %d\n", ret);

	uint16_t val16;
	uint32_t val32;

	for (int i = 0; i < 0x1000; i += 4) {
		val16 = *(volatile uint16_t *)(dsiregs_addr + i);
		val32 = *(volatile uint32_t *)(dsiregs_addr + i);

		if (val16 != 0)
			printf("0x%x val16: 0x%04X\n", i, val16);
		if (val32 != 0)
				printf("0x%x val32: 0x%08X\n", i, val32);
	}

	return 0;
}

void _start() __attribute__ ((weak, alias("module_start")));

int module_start(SceSize args, void *argp)
{
	printf("dsitest\n");

	ioremap(0xE5060000, 0x1000, &dsiregs_uid, &dsiregs_addr);

	printf("DSI1 regs mapped to: %p\n", dsiregs_addr);

	thid = ksceKernelCreateThread("thread", thread, 0x00, 0x1000, 0, 1 << 0, 0);

	ksceKernelStartThread(thid, 0, NULL);

	ksceKernelWaitThreadEnd(thid, NULL, NULL);

	ksceKernelFreeMemBlock(dsiregs_uid);

	return SCE_KERNEL_START_SUCCESS;
}

int ioremap(uintptr_t paddr, unsigned int size, SceUID *uid, void **addr)
{
	int ret;
	SceUID mem_uid;
	unsigned int mem_size;
	void *mem_addr;

	mem_size = ALIGN(size, 4096);

	SceKernelAllocMemBlockKernelOpt opt;
	memset(&opt, 0, sizeof(opt));
	opt.size = sizeof(opt);
	opt.attr = SCE_KERNEL_ALLOC_MEMBLOCK_ATTR_HAS_PADDR;
	opt.paddr = paddr;
	mem_uid = ksceKernelAllocMemBlock("ioremap", 0x20100206, mem_size, &opt);
	if (mem_uid < 0)
		return mem_uid;

	ret = ksceKernelGetMemBlockBase(mem_uid, &mem_addr);
	if (ret < 0) {
		ksceKernelFreeMemBlock(mem_uid);
		return ret;
	}

	if (uid)
		*uid = mem_uid;
	if (addr)
		*addr = mem_addr;

	return 0;
}
