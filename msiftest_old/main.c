#include <stdio.h>
#include <math.h>
#include <taihen.h>
#include <psp2kern/kernel/cpu.h>
#include <psp2kern/kernel/modulemgr.h>
#include <psp2kern/kernel/threadmgr.h>
#include <psp2kern/kernel/sysmem.h>
#include <psp2kern/io/fcntl.h>
#include <psp2kern/io/stat.h>
#include <psp2kern/io/stat.h>
#include <psp2kern/display.h>
#include <psp2kern/lowio/i2c.h>
#include <psp2kern/lowio/gpio.h>
#include <psp2kern/registrymgr.h>
#include <psp2kern/kernel/intrmgr.h>
#include "utils.h"
#include "log.h"
#include "draw.h"
#include "utils.h"

#define LOG(s, ...) \
	do { \
		char buffer[256]; \
		snprintf(buffer, sizeof(buffer), s, ##__VA_ARGS__); \
		uart_print(0, buffer); \
		console_print(buffer); \
		if (0) log_write(buffer, strlen(buffer)); \
	} while (0)

#define LOG_HEX(x) LOG("0x%08X\n", x)

void _start() __attribute__ ((weak, alias ("module_start")));

static void uart_print(int bus, const char *str);

extern int module_get_offset(SceUID pid, SceUID modid, int segidx, size_t offset, uintptr_t *addr);

extern int ksceUartReadAvailable(int device);
extern int ksceUartWrite(int device, unsigned char data);
extern int ksceUartRead(int device);
extern int ksceUartInit(int device);

extern int ScePervasiveForDriver_18DD8043(int device);
extern int ScePervasiveForDriver_788B6C61(int device);
extern int ScePervasiveForDriver_A7CE7DCC(int device);
extern int ScePervasiveForDriver_EFD084D8(int device);
extern int kscePervasiveMsifResetEnable();
extern int kscePervasiveMsifResetDisable();
extern int kscePervasiveMsifClockEnable();
extern int kscePervasiveMsifClockDisable();
extern int kscePervasiveMsifSetClock(int clock);
extern int ScePervasiveForDriver_5E20D7D5_sdif_unk();
extern int ScePervasiveForDriver_E6728E30_sdif_unk();

extern int ksceSysconCtrlMsPower(int power);
extern int ksceSysconCtrlHdmiCecPower(int power);

extern int ksceMsifReadSector(int sector, char *buffer, int nSectors);
extern int ksceMsifWriteSector(int sector, char *buffer, int nSectors);
extern int ksceMsifEnableSlowMode();
extern int ksceMsifDisableSlowMode();
extern int ksceMsifGetSlowModeState();
extern int ksceMsifInit1(void *);
extern int ksceMsifInit2(void *);

int ioremap(uintptr_t paddr, unsigned int size, SceUID *uid, void **addr);

static SceUID msif_uid;
static unsigned char *msif_addr;
static int run = 1;
static SceUID thid;
static	tai_module_info_t SceMsif_modinfo, SceSyscon_modinfo;

#define MSIF_BASE_ADDR ((void *)msif_addr)

static void msif_set_clock(unsigned int clock)
{
	unsigned int val;

	if (clock == 1)
		val = 4;
	else if (clock <= 4)
		val = 5;
	else
		val = 6;

	kscePervasiveMsifSetClock(val);
}

void msif_init(void)
{
	kscePervasiveMsifResetEnable();
	kscePervasiveMsifSetClock(4);
	kscePervasiveMsifResetDisable();
	kscePervasiveMsifClockEnable();
}

static void msif_init1_sub_C286C4(void)
{
	unsigned int val;
	unsigned short tmp;

	msif_set_clock(1);

	/* sub_C28A74 */
	val = readl(MSIF_BASE_ADDR + 0x30 + 0x70);

	tmp = readw(MSIF_BASE_ADDR + 0x30 - 0x2C);
	tmp |= 1;
	writew(tmp, MSIF_BASE_ADDR + 0x30 - 0x2C);

	while (1) {
		tmp = readw(MSIF_BASE_ADDR + 0x30 - 0x2C);
		if (!(tmp & 1)) {
			writel(val, MSIF_BASE_ADDR + 0x30 + 0x70);

			tmp = readw(MSIF_BASE_ADDR + 0x30 - 0x2C);
			tmp |= 4;
			writew(tmp, MSIF_BASE_ADDR + 0x30 - 0x2C);
			break;
		}
	}

	/* sub_C2AD1C */
	tmp = readw(MSIF_BASE_ADDR + 0x30 + 0x0C);
	tmp |= 0x8000;
	writew(tmp, MSIF_BASE_ADDR + 0x30 + 0x0C);

	while (1) {
		tmp = readw(MSIF_BASE_ADDR + 0x30 + 0x0C);
		if (!(tmp & 0x8000)) {
			tmp = readw(MSIF_BASE_ADDR + 0x30 + 0x0C);
			tmp &= 0xFFF8;
			tmp |= 0x4005;
			writew(tmp, MSIF_BASE_ADDR + 0x30 + 0x0C);
			break;
		}
	}
}

static void msif_sub_C2AD68(unsigned char cmd, unsigned int size, unsigned char data0, unsigned char data1)
{
	unsigned int data = (data1 << 24) | (data0 << 16) | (size << 8) | cmd;

	writew(0x8004, MSIF_BASE_ADDR + 0x30);

	while (1) {
		unsigned short tmp;

		tmp = readw(MSIF_BASE_ADDR + 0x30 + 0x08);
		if (tmp & 0x4000)
			break;

		if (0) {
			tmp = readw(MSIF_BASE_ADDR + 0x30 + 0x0C);
			tmp |= 0x8000;
			writew(tmp, MSIF_BASE_ADDR + 0x30 + 0x0C);

			while (1) {
				tmp = readw(MSIF_BASE_ADDR + 0x30 + 0x0C);
				if (!(tmp & 0x8000))
					break;
			}

			if (!(tmp & 0x8000)) {
				tmp = readw(MSIF_BASE_ADDR + 0x30 + 0x0C);
				tmp &= 0xFFF8;
				tmp |= 0x4005;
				writew(tmp, MSIF_BASE_ADDR + 0x30 + 0x0C);
				break;
			}
		}
	}

	writel(data, MSIF_BASE_ADDR + 0x30 + 0x04);
	writel(0, MSIF_BASE_ADDR + 0x30 + 0x04);

	/*while (1) {
		unsigned short tmp;

		tmp = readw(MSIF_BASE_ADDR + 0x30 + 0x08);
		LOG_HEX(tmp);
		if (tmp & 0x1300)
			break;
	}*/
}

static void msif_sub_C2A940(unsigned int size, unsigned char *buff)
{
	while (size > 0) {
		unsigned short tmp;

		LOG("Before\n");

		do {
			tmp = readw(MSIF_BASE_ADDR + 0x30 + 0x08);
			LOG_HEX(tmp);
		} while (!(tmp & 0x4000));

		LOG("After\n");

		*(unsigned int *)(buff + 0) = readl(MSIF_BASE_ADDR + 0x30 + 0x04);
		*(unsigned int *)(buff + 4) = readl(MSIF_BASE_ADDR + 0x30 + 0x04);
		buff += 8;
		size -= 8;
	}
}

static void msif_sub_C2AB7C(unsigned int size, unsigned char *buff)
{
	writew((size & 0x7FF) | 0x4000, MSIF_BASE_ADDR + 0x30);

	msif_sub_C2A940(size, buff);
}

static void msif_sub_C28CDC(unsigned char cmd, unsigned int size, unsigned char *buff)
{
	if (size - 1 > 0xFF)
		return;

	msif_sub_C2AD68(cmd, size, 0x10, 0x0F);
	msif_sub_C2AB7C(size, buff);
}

static void msif_init1_sub_C28700(unsigned int *unkC08, unsigned int *unkC20, unsigned int *unkC24)
{
	unsigned char buff[8];

	msif_sub_C28CDC(0, sizeof(buff), buff);

}

void msif_init1(void)
{
	unsigned int unkC08, unkC20, unkC24;

	msif_init1_sub_C286C4();
	msif_init1_sub_C28700(&unkC08, &unkC20, &unkC24);
}

void msif_init2(unsigned int *buffer)
{

}

typedef struct SceSysconPacket {
	struct SceSysconPacket *next;	// 0x00
	unsigned int status;			// 0x04
	SceUID semaId;			// 0x08
	unsigned int size;			// 0x0C
	unsigned char tx[32]; // tx[0..1] = cmd, tx[2] = size
	unsigned char rx[32]; // rx[0..1] = cmd?, rx[2] = size?
	unsigned int unk1[4];
	int (*callback)(struct SceSysconPacket *packet, void *argp);
	void *argp;
	unsigned int time;
	unsigned int unk2[5];
} SceSysconPacket; /* size 0x80 */

typedef struct SceSysconDebugHandlers {
	int size;
	void (*start)(SceSysconPacket *packet);
	void (*end)(SceSysconPacket *packet);
} SceSysconDebugHandlers;

typedef struct SceSyconGlobalData {
	SceSysconPacket *curPacket;
	SceSysconPacket *packetList0[0x12];
	SceSysconPacket *packetList1[0x12];
	SceSysconDebugHandlers *debug_handlers;
	unsigned int SceSpi0Reg_vaddr;
	unsigned int system_time_low;
	unsigned int unk0A0[(0x0E4 - 0x0A0) / 4];
	unsigned int cb_array[80];
	unsigned int syscon_version;
	unsigned int sysroot_hw_info;
	unsigned int sysroot_au_codec_ic_conexant[4];
	char syscon_timestamp[20];
	unsigned int timestamp_u64_0;
	unsigned int timestamp_u64_1;
	unsigned int unk258;
	unsigned int unk25C;
	unsigned int unk260;
	unsigned int SceSpi0Reg_memblock_uid;
	unsigned int SceSyscon_mutex;
	unsigned int unk26C;
	unsigned int unk270;
	unsigned int unk274;
	unsigned int unk278;
} SceSyconGlobalData; /* size = 0x278 */

static SceSyconGlobalData *syscon_data;
static int doing = 0;

static int thread(SceSize args, void *argp)
{
	uart_print(0, "\e[1;1H\e[2J");

	doing = 1;
	ksceKernelDelayThread(100);
	ksceSysconCtrlMsPower(1);
	/*kscePervasiveMsifResetEnable();
	kscePervasiveMsifClockDisable();
	kscePervasiveMsifSetClock(4);
	kscePervasiveMsifClockEnable();
	kscePervasiveMsifResetDisable();
	ksceSysconCtrlMsPower(1);*/


	//ScePervasiveForDriver_5E20D7D5_sdif_unk();
#if 0
	int y = console_get_y();
	while (run) {
		//console_set_y(y);
		//uart_print(0, "\033[0;0H");
		/*ksceSysconCtrlMsPower(0);
		ksceKernelDelayThread(2 * 1000 * 1000);
		ksceSysconCtrlMsPower(1);
		ksceKernelDelayThread(2 * 1000 * 1000);*/

		//LOG("+0x00: 0x%04X\n", readw(MSIF_BASE_ADDR));
		//LOG("+0x38: 0x%04X\n", readw(MSIF_BASE_ADDR + 0x30 + 0x08));
		//LOG("+0x3C: 0x%04X\n", readw(MSIF_BASE_ADDR + 0x30 + 0x0C));
	}
#endif

	ksceKernelDelayThread(1000 * 1000);
	doing = 0;


	//ksceSysconCtrlMsPower(1);

	return 0;
}

static tai_hook_ref_t SceLowio_ScePervasiveForDriver_F9E7B023_ref;
static SceUID SceLowio_ScePervasiveForDriver_F9E7B023_hook_uid = -1;

static tai_hook_ref_t SceLowio_ksceGpioPortSet_ref;
static SceUID SceLowio_ksceGpioPortSet_hook_uid = -1;

static tai_hook_ref_t SceLowio_ksceGpioPortClear_ref;
static SceUID SceLowio_ksceGpioPortClear_hook_uid = -1;


static SceUID SceMsif_SceMsifIns_0x3C_intr_handler_hook_uid = -1;
static tai_hook_ref_t SceMsif_SceMsifIns_0x3C_intr_handler_ref;
static SceUID SceMsif_SceMsifSmshc_0x3D_intr_handler_hook_uid = -1;
static tai_hook_ref_t SceMsif_SceMsifSmshc_0x3D_intr_handler_ref;

static SceUID SceMsif_wait_evflag_sub_C2B618_hook_uid = -1;
static tai_hook_ref_t SceMsif_wait_evflag_sub_C2B618_ref;

static tai_hook_ref_t SceSyscon_ksceSysconCtrlMsPower_ref;
static SceUID SceSyscon_ksceSysconCtrlMsPower_hook_uid = -1;

static tai_hook_ref_t SceSyscon_SceSysconForDriver_0D0B6D25_ref;
static SceUID SceSyscon_SceSysconForDriver_0D0B6D25_hook_uid = -1;

static tai_hook_ref_t SceSyscon_sceSysconCommonWrite_ref;
static SceUID SceSyscon_sceSysconCommonWrite_hook_uid = -1;

static tai_hook_ref_t SceSyscon_sceSysconPacketStart_ref;
static SceUID SceSyscon_sceSysconPacketStart_hook_uid = -1;

static tai_hook_ref_t SceSyscon_ksceSysconCmdSync_ref;
static SceUID SceSyscon_ksceSysconCmdSync_hook_uid = -1;

static tai_hook_ref_t SceSyscon_ksceSysconCmdExecAsync_ref;
static SceUID SceSyscon_ksceSysconCmdExecAsync_hook_uid = -1;

static tai_hook_ref_t SceSyscon_SceSysconCmdclr_sceSysconGpioIntr_handler_ref;
static SceUID SceSyscon_SceSysconCmdclr_sceSysconGpioIntr_handler_hook_uid = -1;

static int SceLowio_ScePervasiveForDriver_F9E7B023_hook_func(void)
{
	LOG("SceLowio_ScePervasiveForDriver_F9E7B023()\n");

	//return 0;

	return TAI_CONTINUE(int, SceLowio_ScePervasiveForDriver_F9E7B023_ref);
}

static int log_gpio = 0;
static int SceLowio_ksceGpioPortSet_hook_func(int bus, int port)
{
	if (log_gpio)
		LOG("SceLowio_ksceGpioPortSet(%d, %d)\n", bus, port);


	return TAI_CONTINUE(int, SceLowio_ksceGpioPortSet_ref, bus, port);
}

static int SceLowio_ksceGpioPortClear_hook_func(int bus, int port)
{
	if (log_gpio)
		LOG("SceLowio_ksceGpioPortClear(%d, %d)\n", bus, port);


	return TAI_CONTINUE(int, SceLowio_ksceGpioPortClear_ref, bus, port);
}


static int SceSyscon_ksceSysconCtrlMsPower_hook_func(int enable)
{
	LOG("SceSyscon_ksceSysconCtrlMsPower(%d)\n", enable);

	//return 0;

	return TAI_CONTINUE(int, SceSyscon_ksceSysconCtrlMsPower_ref, enable);
}

static int SceSyscon_SceSysconForDriver_0D0B6D25_hook_func()
{
	LOG("SceSyscon_SceSysconForDriver_0D0B6D25()\n");

	//return 0;

	return TAI_CONTINUE(int, SceSyscon_SceSysconForDriver_0D0B6D25_ref);
}

static int SceSyscon_sceSysconCommonWrite_hook_func(unsigned int data, unsigned short cmd, unsigned char len)
{
	LOG("SceSyscon_sceSysconCommonWrite(%d, 0x%04X, %d)\n", data, cmd, len);

	//log_gpio = 1;

	if (cmd == 0x89B) {
		//LOG("sceSysconCommonWrite before curPacket: %p\n", syscon_data->curPacket);
		/*for (int i = 0; i < 0x12; i++) {
			LOG("  packetList0[%d]: %p\n", i, syscon_data->packetList0[i]);
		}
		for (int i = 0; i < 0x12; i++) {
			LOG("  packetList1[%d]: %p\n", i, syscon_data->packetList1[i]);
		}*/
	}

	int ret = TAI_CONTINUE(int, SceSyscon_sceSysconCommonWrite_ref, data, cmd, len);

	if (cmd == 0x89B) {
		//LOG("sceSysconCommonWrite after curPacket: %p\n", syscon_data->curPacket);
		/*for (int i = 0; i < 0x12; i++) {
			LOG("  packetList0[%d]: %p\n", i, syscon_data->packetList0[i]);
		}
		for (int i = 0; i < 0x12; i++) {
			LOG("  packetList1[%d]: %p\n", i, syscon_data->packetList1[i]);
		}*/
	}


	log_gpio = 0;

	return ret;
}

static int SceSyscon_sceSysconPacketStart_hook_func(SceSysconPacket *packet)
{
	unsigned int lr;
	asm volatile("mov %0, lr\n\t" : "=r"(lr));

	if (packet->tx[0] == 0x9B && packet->tx[1] == 0x08) {
		unsigned short cmd = packet->tx[0] | (packet->tx[1] << 8);
		LOG("SceSyscon_sceSysconPacketStart(%p), lr: 0x%08X\n", packet, lr);

		LOG("curPacket: %p\n", syscon_data->curPacket);
		LOG("curPacket->next: %p\n", syscon_data->curPacket->next);

		LOG("cmd: 0x%04X\n", cmd);
		LOG("tx[0]: 0x%02X\n", packet->tx[0]);
		LOG("tx[1]: 0x%02X\n", packet->tx[1]);
		LOG("tx[2]: 0x%02X\n", packet->tx[2]);
		LOG("tx[3]: 0x%02X\n", packet->tx[3]);
		LOG("tx[4]: 0x%02X\n", packet->tx[4]);
		LOG("tx[5]: 0x%02X\n", packet->tx[5]);
		LOG("tx[6]: 0x%02X\n", packet->tx[6]);
	}

	int ret = TAI_CONTINUE(int, SceSyscon_sceSysconPacketStart_ref, packet);

	if (packet->tx[0] == 0x9B && packet->tx[1] == 0x08) {
		LOG("SceSyscon_sceSysconPacketStart(%p), after ret: 0x%08X\n", packet, ret);
		//LOG("curPacket: %p\n", syscon_data->curPacket);
		//LOG("curPacket->next: %p\n", syscon_data->curPacket->next);
		LOG("+0x00: 0x%08x\n", *(unsigned int *)&packet->rx[0]);
		LOG("+0x04: 0x%08x\n", *(unsigned int *)&packet->rx[4]);
	}

	return ret;
}

static int SceSyscon_ksceSysconCmdSync_hook_func(SceSysconPacket *packet, unsigned int flags)
{
	if (packet->tx[0] == 0x9B && packet->tx[1] == 0x08) {
		LOG("SceSyscon_ksceSysconCmdSync(%p, 0x%08X)\n", packet, flags);
		//LOG("\n\nunk: 0x%08X\n", syscon_data->unk0A0[0x10]);
	}

	int ret = TAI_CONTINUE(int, SceSyscon_ksceSysconCmdSync_ref, packet, flags);

	if (packet->tx[0] == 0x9B && packet->tx[1] == 0x08) {
		//LOG("unk: 0x%08X\n", syscon_data->unk0A0[0x10]);
		LOG("SceSyscon_ksceSysconCmdSync(%p, 0x%08X) after ret: 0x%08X\n", packet, flags, ret);
		LOG("+0x00: 0x%08x\n", *(unsigned int *)&packet->rx[0]);
		LOG("+0x04: 0x%08x\n", *(unsigned int *)&packet->rx[4]);
	}

	return ret;
}

static int SceSyscon_ksceSysconCmdExecAsync_hook_func(SceSysconPacket *packet, int flags, int (*callback)(SceSysconPacket*, void*), void *argp)
{
	unsigned short cmd = packet->tx[0] | (packet->tx[1] << 8);

	//if (cmd != 0x0104 && doing)
	//	return 0;

	//LOG("ksceSysconCmdExecAsync cmd: 0x%04X\n", cmd);

	int ret = TAI_CONTINUE(int, SceSyscon_ksceSysconCmdExecAsync_ref, packet, flags, callback, argp);

	return ret;
}


static int SceSyscon_SceSysconCmdclr_sceSysconGpioIntr_handler_hook_func(int subintr_code, int a1, int a2)
{
	//LOG("SceSyscon_SceSysconCmdclr_sceSysconGpioIntr_handler(0x%08X, %d, %d)\n", subintr_code, a1, a2);

	SceSysconPacket *cur_packet = syscon_data->curPacket;

	unsigned short cmd = cur_packet->tx[0] | (cur_packet->tx[1] << 8);

	//LOG("curPacket: %p\n", cur_packet);
	//LOG("curPacket CMD: 0x%04X\n", cmd);
	//LOG("curPacket->next: %p\n", cur_packet->next);

	int ret = TAI_CONTINUE(int, SceSyscon_SceSysconCmdclr_sceSysconGpioIntr_handler_ref, subintr_code, a1, a2);

	return ret;
}

static int SceMsif_SceMsifIns_0x3C_intr_handler_hook_func(int code, int arg)
{
	//LOG("SceMsif_SceMsifIns_0x3C_intr_handler_hook_func()\n");

	//static int i = 0;
	//i++;
	/*if (i == 2) {
		ksceSysconCtrlMsPower(0);
		kscePervasiveMsifResetEnable();
		kscePervasiveMsifClockDisable();

		msif_init();
		ksceSysconCtrlMsPower(1);
		msif_init1();
	}*/

	//return 0;

	return TAI_CONTINUE(int, SceMsif_SceMsifIns_0x3C_intr_handler_ref, code, arg);
}

static int SceMsif_SceMsifSmshc_0x3D_intr_handler_hook_func(int code, int arg)
{
	/*volatile int i = 0;
	for (i = 0; i < 0x100000; i++)
		;*/
	//LOG("SceMsif_SceMsifSmshc_0x3D_intr_handler_hook_func()\n");

	/*LOG("+0x00: 0x%04X\n", *(unsigned short *)(msif_addr));

	*(unsigned short *)(msif_addr) |= 8;
	dmb();

	LOG("+0x00: 0x%04X\n", *(unsigned short *)(msif_addr));


	return 0;*/

	return TAI_CONTINUE(int, SceMsif_SceMsifSmshc_0x3D_intr_handler_ref, code, arg);
}

static int SceMsif_wait_evflag_sub_C2B618_hook_func(SceUID uid, int ms)
{
	//LOG("SceMsif_wait_evflag_sub_C2B618_hook_func(0x%08X, 0x%08X)\n", uid, ms);

	return TAI_CONTINUE(int, SceMsif_wait_evflag_sub_C2B618_ref, uid, ms);
}


int module_start(SceSize argc, const void *args)
{
	ScePervasiveForDriver_EFD084D8(0); // Turn on clock
	ScePervasiveForDriver_A7CE7DCC(0); // Out of reset
	ksceUartInit(0);
	//log_reset();
	map_framebuffer();
	LOG("\n\nmsiftest by xerpi\n");

	ioremap(0xE0900000, 0x1000, &msif_uid, (void **)&msif_addr);
	LOG("MSIF addr: %p\n", msif_addr);

	SceMsif_modinfo.size = sizeof(SceMsif_modinfo);
	taiGetModuleInfoForKernel(KERNEL_PID, "SceMsif", &SceMsif_modinfo);

	SceSyscon_modinfo.size = sizeof(SceSyscon_modinfo);
	taiGetModuleInfoForKernel(KERNEL_PID, "SceSyscon", &SceSyscon_modinfo);

	module_get_offset(KERNEL_PID, SceSyscon_modinfo.modid, 1, 0, (uintptr_t *)&syscon_data);

	/*
	 * Function hooks
	 */
	SceLowio_ScePervasiveForDriver_F9E7B023_hook_uid = taiHookFunctionExportForKernel(KERNEL_PID,
		&SceLowio_ScePervasiveForDriver_F9E7B023_ref, "SceLowio", 0xE692C727,
		0xF9E7B023, SceLowio_ScePervasiveForDriver_F9E7B023_hook_func);

	SceLowio_ksceGpioPortSet_hook_uid = taiHookFunctionExportForKernel(KERNEL_PID,
		&SceLowio_ksceGpioPortSet_ref, "SceLowio", 0xF0EF5743,
		0xD454A584, SceLowio_ksceGpioPortSet_hook_func);

	SceLowio_ksceGpioPortClear_hook_uid = taiHookFunctionExportForKernel(KERNEL_PID,
		&SceLowio_ksceGpioPortClear_ref, "SceLowio", 0xF0EF5743,
		0xF6310435, SceLowio_ksceGpioPortClear_hook_func);

	SceSyscon_ksceSysconCtrlMsPower_hook_uid = taiHookFunctionExportForKernel(KERNEL_PID,
		&SceSyscon_ksceSysconCtrlMsPower_ref, "SceSyscon", 0x60A35F64,
		0x710A7CF0, SceSyscon_ksceSysconCtrlMsPower_hook_func);

	SceSyscon_SceSysconForDriver_0D0B6D25_hook_uid = taiHookFunctionExportForKernel(KERNEL_PID,
		&SceSyscon_SceSysconForDriver_0D0B6D25_ref, "SceSyscon", 0x60A35F64,
		0x0D0B6D25, SceSyscon_SceSysconForDriver_0D0B6D25_hook_func);

	SceSyscon_sceSysconCommonWrite_hook_uid = taiHookFunctionOffsetForKernel(KERNEL_PID,
		&SceSyscon_sceSysconCommonWrite_ref, SceSyscon_modinfo.modid, 0,
		0x00BDB0C8 - 0x00BD8000, 1, SceSyscon_sceSysconCommonWrite_hook_func);

	SceSyscon_sceSysconPacketStart_hook_uid = taiHookFunctionOffsetForKernel(KERNEL_PID,
		&SceSyscon_sceSysconPacketStart_ref, SceSyscon_modinfo.modid, 0,
		0x00BD803C - 0x00BD8000, 1, SceSyscon_sceSysconPacketStart_hook_func);

	SceSyscon_ksceSysconCmdSync_hook_uid = taiHookFunctionOffsetForKernel(KERNEL_PID,
		&SceSyscon_ksceSysconCmdSync_ref, SceSyscon_modinfo.modid, 0,
		0x00BDA5E4 - 0x00BD8000, 1, SceSyscon_ksceSysconCmdSync_hook_func);

	SceSyscon_SceSysconCmdclr_sceSysconGpioIntr_handler_hook_uid = taiHookFunctionOffsetForKernel(KERNEL_PID,
		&SceSyscon_SceSysconCmdclr_sceSysconGpioIntr_handler_ref, SceSyscon_modinfo.modid, 0,
		0x00BD864C - 0x00BD8000, 1, SceSyscon_SceSysconCmdclr_sceSysconGpioIntr_handler_hook_func);

	SceSyscon_ksceSysconCmdExecAsync_hook_uid = taiHookFunctionExportForKernel(KERNEL_PID,
		&SceSyscon_ksceSysconCmdExecAsync_ref, "SceSyscon", 0x60A35F64,
		0xC2224E82, SceSyscon_ksceSysconCmdExecAsync_hook_func);

	SceMsif_SceMsifIns_0x3C_intr_handler_hook_uid = taiHookFunctionOffsetForKernel(KERNEL_PID,
		&SceMsif_SceMsifIns_0x3C_intr_handler_ref, SceMsif_modinfo.modid, 0,
		0x00C2B75C - 0x00C28000, 1, SceMsif_SceMsifIns_0x3C_intr_handler_hook_func);

	SceMsif_SceMsifSmshc_0x3D_intr_handler_hook_uid = taiHookFunctionOffsetForKernel(KERNEL_PID,
		&SceMsif_SceMsifSmshc_0x3D_intr_handler_ref, SceMsif_modinfo.modid, 0,
		0x00C2B7B8 - 0x00C28000, 1, SceMsif_SceMsifSmshc_0x3D_intr_handler_hook_func);

	SceMsif_wait_evflag_sub_C2B618_hook_uid = taiHookFunctionOffsetForKernel(KERNEL_PID,
		&SceMsif_wait_evflag_sub_C2B618_ref, SceMsif_modinfo.modid, 0,
		0x00C2B618 - 0x00C28000, 1, SceMsif_wait_evflag_sub_C2B618_hook_func);


	thid = ksceKernelCreateThread("disptest", thread, 0x00, 0x1000, 0, 1 << 0, 0);

	ksceKernelStartThread(thid, 0, NULL);

	/*unsigned char buffer[512] = {0};
	int ret = ksceMsifReadSector(0, buffer, 1);
	LOG("ret: 0x%08X\n", ret);
	for (int i = 0; i < 10; i++)
		LOG("0x%02X\n", buffer[i]);
	LOG("%s\n", buffer);
	LOG("0x%02X 0x%02X\n", buffer[0x1FE], buffer[0x1FF]);*/

	return SCE_KERNEL_START_SUCCESS;
}

int module_stop(SceSize argc, const void *args)
{
	run = 0;
	ksceKernelWaitThreadEnd(thid, NULL, NULL);

	if (SceLowio_ScePervasiveForDriver_F9E7B023_hook_uid > 0) {
		taiHookReleaseForKernel(SceLowio_ScePervasiveForDriver_F9E7B023_hook_uid,
			SceLowio_ScePervasiveForDriver_F9E7B023_ref);
	}

	if (SceLowio_ksceGpioPortSet_hook_uid > 0) {
		taiHookReleaseForKernel(SceLowio_ksceGpioPortSet_hook_uid,
			SceLowio_ksceGpioPortSet_ref);
	}

	if (SceLowio_ksceGpioPortClear_hook_uid > 0) {
		taiHookReleaseForKernel(SceLowio_ksceGpioPortClear_hook_uid,
			SceLowio_ksceGpioPortClear_ref);
	}

	if (SceSyscon_ksceSysconCtrlMsPower_hook_uid > 0) {
		taiHookReleaseForKernel(SceSyscon_ksceSysconCtrlMsPower_hook_uid,
			SceSyscon_ksceSysconCtrlMsPower_ref);
	}

	if (SceSyscon_SceSysconForDriver_0D0B6D25_hook_uid > 0) {
		taiHookReleaseForKernel(SceSyscon_SceSysconForDriver_0D0B6D25_hook_uid,
			SceSyscon_SceSysconForDriver_0D0B6D25_ref);
	}

	if (SceSyscon_sceSysconCommonWrite_hook_uid > 0) {
		taiHookReleaseForKernel(SceSyscon_sceSysconCommonWrite_hook_uid,
			SceSyscon_sceSysconCommonWrite_ref);
	}

	if (SceSyscon_sceSysconPacketStart_hook_uid > 0) {
		taiHookReleaseForKernel(SceSyscon_sceSysconPacketStart_hook_uid,
			SceSyscon_sceSysconPacketStart_ref);
	}

	if (SceSyscon_ksceSysconCmdSync_hook_uid > 0) {
		taiHookReleaseForKernel(SceSyscon_ksceSysconCmdSync_hook_uid,
			SceSyscon_ksceSysconCmdSync_ref);
	}

	if (SceSyscon_ksceSysconCmdExecAsync_hook_uid > 0) {
		taiHookReleaseForKernel(SceSyscon_ksceSysconCmdExecAsync_hook_uid,
			SceSyscon_ksceSysconCmdExecAsync_ref);
	}

	if (SceSyscon_SceSysconCmdclr_sceSysconGpioIntr_handler_hook_uid > 0) {
		taiHookReleaseForKernel(SceSyscon_SceSysconCmdclr_sceSysconGpioIntr_handler_hook_uid,
			SceSyscon_SceSysconCmdclr_sceSysconGpioIntr_handler_ref);
	}

	if (SceMsif_SceMsifIns_0x3C_intr_handler_hook_uid > 0) {
		taiHookReleaseForKernel(SceMsif_SceMsifIns_0x3C_intr_handler_hook_uid,
			SceMsif_SceMsifIns_0x3C_intr_handler_ref);
	}

	if (SceMsif_SceMsifSmshc_0x3D_intr_handler_hook_uid > 0) {
		taiHookReleaseForKernel(SceMsif_SceMsifSmshc_0x3D_intr_handler_hook_uid,
			SceMsif_SceMsifSmshc_0x3D_intr_handler_ref);
	}

	if (SceMsif_wait_evflag_sub_C2B618_hook_uid > 0) {
		taiHookReleaseForKernel(SceMsif_wait_evflag_sub_C2B618_hook_uid,
			SceMsif_wait_evflag_sub_C2B618_ref);
	}

	ksceKernelFreeMemBlock(msif_uid);

	unmap_framebuffer();

	return SCE_KERNEL_STOP_SUCCESS;
}

static void uart_print(int bus, const char *str)
{
	while (*str) {
		ksceUartWrite(bus, *str);
		if (*str == '\n')
			ksceUartWrite(bus, '\r');
		str++;
	}
}


int ioremap(uintptr_t paddr, unsigned int size, SceUID *uid, void **addr)
{
	int ret;
	SceUID mem_uid;
	unsigned int mem_size;
	void *mem_addr;

	mem_size = ALIGN(size, 4096);

	SceKernelAllocMemBlockKernelOpt opt;
	memset(&opt, 0, sizeof(opt));
	opt.size = sizeof(opt);
	opt.attr = 2;
	opt.paddr = paddr;
	mem_uid = ksceKernelAllocMemBlock("ioremap", 0x20100206, mem_size, &opt);
	if (mem_uid < 0)
		return mem_uid;

	ret = ksceKernelGetMemBlockBase(mem_uid, &mem_addr);
	if (ret < 0) {
		ksceKernelFreeMemBlock(mem_uid);
		return ret;
	}

	if (uid)
		*uid = mem_uid;
	if (addr)
		*addr = mem_addr;

	return 0;
}
