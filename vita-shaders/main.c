#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <psp2/ctrl.h>
#include <psp2/kernel/processmgr.h>
#include <psp2/kernel/modulemgr.h>
#include "shacccg.h"
#include "debugScreen.h"

#define printf(...) psvDebugScreenPrintf(__VA_ARGS__)

#define LIBSHACCCG_PATH "ux0:data/libshacccg.suprx"

#define SHADER_FILE "ux0:data/shader_v.cg"
#define SHADER_OUTPUT "ux0:data/shader_v.gxp"

#define error(...) \
	do { \
		printf(__VA_ARGS__); \
		wait_key_press(); \
		sceKernelExitProcess(0); \
	} while (0)

void wait_key_press();

static long shader_size;
static void *shader_data;
static SceShaccCgSourceFile source_file;

SceShaccCgSourceFile *cb_open_file(const char *fileName,
	const SceShaccCgSourceLocation *includedFrom,
	const SceShaccCgCompileOptions *compileOptions,
	const char **errorString)
{
	printf("SceShaccCgCallbackOpenFile(%s)\n", fileName);

	source_file.fileName = fileName;
	source_file.text = shader_data;
	source_file.size = shader_size;

	return &source_file;
}

int main()
{
	FILE *fp;

	psvDebugScreenInit();

	printf("Shader compiler\n");

	int ret = sceKernelLoadStartModule(LIBSHACCCG_PATH, 0, NULL, 0, NULL, NULL);
	if (ret < 0) {
		error("Error loading " LIBSHACCCG_PATH ": 0x%08X\n", ret);
		wait_key_press();
		return -1;
	} else {
		printf("Module loaded successfully!\n");
	}

	/* Read shader file */

	fp = fopen(SHADER_FILE, "rb");
	if (!fp) {
		error("%s: cannot open, errno %d\n", SHADER_FILE, errno);
		wait_key_press();
		return -2;
	}

	fseek(fp, 0, SEEK_END);
	shader_size = ftell(fp);
	fseek(fp, 0, SEEK_SET);

	shader_data = malloc(shader_size);

	fread(shader_data, shader_size, 1, fp);

	fclose(fp);

	// Register malloc and free function pointers
	ret = SceShaccCg_6F01D573(malloc, free);
	printf("SceShaccCg_6F01D573: 0x%08x\n", ret);

	SceShaccCgCallbackList cb_list;
	sceShaccCgInitializeCallbackList(&cb_list, SCE_SHACCCG_TRIVIAL);

	cb_list.openFile = cb_open_file;

	SceShaccCgCompileOptions options;
	sceShaccCgInitializeCompileOptions(&options);

	options.mainSourceFile = SHADER_FILE;
	options.targetProfile =  SCE_SHACCCG_PROFILE_VP;
	options.entryFunctionName = "main";
	options.macroDefinitionCount = 1;
	const char *str = "SHADER_API_PSM";
	options.macroDefinitions = &str;
	options.useFx = 1;
	options.optimizationLevel = 3;
	options.useFastmath = 1;
	options.useFastint = 0;
	options.performanceWarnings = 0;
	options.warningLevel = 1;
	options.pedantic = 3;

	const SceShaccCgCompileOutput *r = sceShaccCgCompileProgram(&options, &cb_list, 0);
	printf("sceShaccCgCompileProgram: %p\n", r);

	// print infos/warnings/errors
	printf("diagnosticCount %lu\n", r->diagnosticCount);
	for (int i = 0; i < r->diagnosticCount; ++i) {
		const SceShaccCgDiagnosticMessage *e = &r->diagnostics[i];
		uint32_t x = 0, y = 0;
		if (e->location) {
			x = e->location->lineNumber;
			y = e->location->columnNumber;
		}
		switch (e->level) {
		case SCE_SHACCCG_DIAGNOSTIC_LEVEL_INFO:
			printf("INFO (%lu, %lu):  \n", x, y);
			break;
		case SCE_SHACCCG_DIAGNOSTIC_LEVEL_WARNING:
			printf("WARN (%lu, %lu):  \n", x, y);
			break;
		case SCE_SHACCCG_DIAGNOSTIC_LEVEL_ERROR:
			printf("ERROR (%lu, %lu):  \n", x, y);
			break;
		}
		//printf("%s\n", e->message);
	}

	if (r->programData) {
		FILE *output = fopen(SHADER_OUTPUT, "wb");
		if (!output) {
			error("%s: cannot open for write, errno %d\n", SHADER_OUTPUT, errno);
			wait_key_press();
			return -2;
		}
		fwrite(r->programData, r->programSize, 1, output);
		fclose(output);
	} else {
		return -1;
	}

	sceShaccCgDestroyCompileOutput(r);

	free(shader_data);

	psvDebugScreenSetFgColor(COLOR_CYAN);
	printf("Shader compiled to " SHADER_OUTPUT "\n");
	psvDebugScreenSetFgColor(COLOR_WHITE);

	wait_key_press();

	return 0;

}

void wait_key_press()
{
	SceCtrlData pad;

	printf("Press START to continue.\n");

	while (1) {
		sceCtrlPeekBufferPositive(0, &pad, 1);
		if (pad.buttons & SCE_CTRL_START)
			break;
		else if (pad.buttons & SCE_CTRL_CIRCLE)
			printf("Button O pressed!\n");
		sceKernelDelayThread(100 * 1000);
	}
}
