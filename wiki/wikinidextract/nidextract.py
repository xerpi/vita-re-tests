#!/usr/bin/env python3

import sys
from html.parser import HTMLParser
from html.entities import name2codepoint

cur_tag = ""
cur_lib = ""
cur_func = ""
in_h3 = False

class MyHTMLParser(HTMLParser):


	def handle_starttag(self, tag, attrs):
		global cur_tag
		global in_h3

		print("Start tag:", tag)
		cur_tag = tag
		if tag == "h3":
			in_h3 = True


	def handle_data(self, data):
		global cur_tag
		global cur_lib
		global cur_func
		print("cur: ", cur_tag, cur_lib)
		if cur_tag == "h2":
			if cur_tag.startswith("Sce"):
				cur_lib = data
				print(cur_lib)
		elif cur_tag == "h3":
			if cur_tag.startswith("sce"):
				cur_func = data
		elif cur_tag == "td":
			if cur_tag.startswith("0x"):
				nid = data
				print("  " + cur_func + ": " + hex(nid))

		#print("Data     :", data)


parser = MyHTMLParser()
file = open(sys.argv[1], "r")
parser.feed(file.read())
