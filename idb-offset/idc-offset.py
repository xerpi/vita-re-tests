#!/usr/bin/env python3

import sys
import re

def usage():
	print("idc-offset by xerpi")
	print("Usage:\n\t" + sys.argv[0] + " new_text_addr new_data_addr")

def patch_idc(idc, new_text_addr, new_data_addr):
	text_start = None
	text_end = None
	data_start = None
	data_end = None

	for line in idc:
		# First try to find the .text and .data addresses
		if text_start == None or data_start == None:
			m = re.search(r'^\s*SegCreate\((0X[0-9a-fA-F]+),(0X[0-9a-fA-F]+),(0X[0-9a-fA-F]+)', line)
			if m:
				seg_start = int(m.groups(0)[0], 16)
				seg_end = int(m.groups(0)[1], 16)
				seg_type = int(m.groups(0)[2], 16)

				if seg_type == 1:
					text_start = seg_start
					text_end = seg_end
				elif seg_type == 2:
					data_start = seg_start
					data_end = seg_end

		# Search hex numbers
		nums_str = re.findall(r'0X[0-9A-F]+', line, re.I)
		for num_str in nums_str:
			num = int(num_str, 16)

			if text_start != None:
				if num >= text_start and num <= text_end:
					new = (num - text_start) + new_text_addr
					line = line.replace(num_str, hex(new).upper())

			if data_start != None:
				if num >= data_start and num <= data_end:
					new = (num - data_start) + new_data_addr
					line = line.replace(num_str, hex(new).upper())

		print(line, end='')


def main():
	if len(sys.argv) < 4:
		usage()
	else:
		with open(sys.argv[1]) as idc:
			patch_idc(idc,
				int(sys.argv[2], 16),
				int(sys.argv[3], 16))

if __name__ == "__main__":
	main()
