#include <psp2kern/kernel/cpu.h>
#include <psp2kern/kernel/modulemgr.h>
#include <psp2kern/kernel/threadmgr.h>
#include <psp2kern/kernel/sysmem.h>
#include <psp2kern/io/fcntl.h>
#include <psp2kern/display.h>
#include "draw.h"
#include "log.h"

#define LOG(s, ...) \
	do { \
		char buffer[256]; \
		snprintf(buffer, sizeof(buffer), s, ##__VA_ARGS__); \
		console_print(buffer); \
		log_write(buffer, strlen(buffer)); \
	} while (0)

void _start() __attribute__ ((weak, alias ("module_start")));

#define ALIGN(x, a) (((x) + ((a) - 1)) & ~((a) - 1))

#define LINUX_FILENAME "ux0:/linux/zImage"
#define DTB_FILENAME "ux0:/linux/vita.dtb"

#define SCRATCHPAD_ADDR ((void *)0x00000000)
#define SCREEN_PITCH 1024
#define SCREEN_W 960
#define SCREEN_H 544

extern const unsigned char _binary_payload_bin_start;
extern const unsigned char _binary_payload_bin_size;

static const unsigned int payload_size = (unsigned int)&_binary_payload_bin_size;
static const void *const payload_addr = (void *)&_binary_payload_bin_start;

static unsigned long get_cpu_id(void);
static unsigned long get_ttbr0(void);
static unsigned long get_ttbcr(void);
static unsigned long get_paddr(unsigned long vaddr);
static int find_paddr(unsigned long paddr, unsigned long vaddr, unsigned int size,
		      unsigned int step, unsigned long *found_vaddr);
static unsigned long page_table_entry(unsigned long paddr);
static void map_scratchpad(void);

static int payload_trampoline_thread(SceSize args, void *argp)
{
	ksceKernelCpuDisableInterrupts();
	asm volatile("cpsid if\n");

	/*
	 * Map the scratchpad to VA 0x00000000-0x00003FFF.
	 */
	map_scratchpad();

	ksceKernelCpuDcacheWritebackInvalidateAll();
	ksceKernelCpuIcacheInvalidateAll();

	asm volatile(
		/* TLB invalidate */
		"mcr p15, 0, %0, c8, c6, 0\n"
		"mcr p15, 0, %0, c8, c5, 0\n"
		"mcr p15, 0, %0, c8, c7, 0\n"
		"mcr p15, 0, %0, c8, c3, 0\n"
		/* Branch predictor invalidate all */
		"mcr p15, 0, %0, c7, c5, 6\n"
		/* Branch predictor invalidate all (IS) */
		"mcr p15, 0, %0, c7, c1, 6\n"
		/* Instruction cache invalidate all (PoU) */
		"mcr p15, 0, %0, c7, c5, 0\n"
		/* Instruction cache invalidate all (PoU, IS) */
		"mcr p15, 0, %0, c7, c1, 0\n"
		/* Instruction barrier */
		"mcr p15, 0, %0, c7, c5, 4\n"
		/* Data barrier */
		"dmb\n"
		"dsb\n"
		: : "r"(0));

	/*
	 * Jump to the payload
	 */
	asm volatile("bx %0\n"
		: : "r"(0));

	while (1)
		;

	return 0;
}

int module_start(SceSize argc, const void *args)
{
	log_reset();
	map_framebuffer();
	LOG("lowiostuff by xerpi\n");

	/*
	 * Map the scratchpad to VA 0x00000000-0x00003FFF.
	 */
	map_scratchpad();
	LOG("Scratchpad mapped\n");

	/*
	 * Copy the payload to the scratchpad.
	 */
	ksceKernelCpuUnrestrictedMemcpy(SCRATCHPAD_ADDR, payload_addr, payload_size);
	ksceKernelCpuDcacheWritebackRange(SCRATCHPAD_ADDR, payload_size);
	ksceKernelCpuIcacheAndL2WritebackInvalidateRange(SCRATCHPAD_ADDR, payload_size);

	/*
	 * Start a thread for each CPU.
	 */
	int i;
	for (i = 0; i < 4; i++) {
		SceUID thid = ksceKernelCreateThread("trampoline",
			payload_trampoline_thread, 0x00, 0x1000, 0, 1 << i, 0);

		ksceKernelStartThread(thid, 0, NULL);
	}

	/*
	 * Wait forever...
	 */
	while (1)
		ksceKernelDelayThread(1000);

	return SCE_KERNEL_START_SUCCESS;
}

int module_stop(SceSize argc, const void *args)
{
	unmap_framebuffer();

	return SCE_KERNEL_STOP_SUCCESS;
}

unsigned long get_cpu_id(void)
{
	unsigned long mpidr;

	asm volatile("mrc p15, 0, %0, c0, c0, 5\n" : "=r"(mpidr));

	return mpidr & 3;
}

unsigned long get_ttbr0(void)
{
	unsigned long ttbr0;

	asm volatile("mrc p15, 0, %0, c2, c0, 0\n" : "=r"(ttbr0));

	return ttbr0;
}

unsigned long get_ttbcr(void)
{
	unsigned long ttbcr;

	asm volatile("mrc p15, 0, %0, c2, c0, 2\n" : "=r"(ttbcr));

	return ttbcr;
}

unsigned long get_paddr(unsigned long vaddr)
{
	unsigned long paddr;

	ksceKernelGetPaddr((void *)vaddr, (uintptr_t *)&paddr);

	return paddr;
}

int find_paddr(unsigned long paddr, unsigned long vaddr, unsigned int size, unsigned int step, unsigned long *found_vaddr)
{
	unsigned long vaddr_end = vaddr + size;

	for (; vaddr < vaddr_end; vaddr += step) {
		unsigned long cur_paddr = get_paddr(vaddr);

		if ((cur_paddr & ~(step - 1)) == (paddr & ~(step - 1))) {
			if (found_vaddr)
				*found_vaddr = vaddr;
			return 1;
		}
	}

	return 0;
}

unsigned long page_table_entry(unsigned long paddr)
{
	unsigned long base_addr = paddr >> 12;
	unsigned long XN        = 0b0;   /* XN disabled */
	unsigned long C_B       = 0b10;  /* Outer and Inner Write-Through, no Write-Allocate */
	unsigned long AP_1_0    = 0b11;  /* Full access */
	unsigned long TEX_2_0   = 0b000; /* Outer and Inner Write-Through, no Write-Allocate */
	unsigned long AP_2      = 0b0;   /* Full access */
	unsigned long S         = 0b1;   /* Shareable */
	unsigned long nG        = 0b0;   /* Global translation */

	return  (base_addr << 12) |
		(nG        << 11) |
		(S         << 10) |
		(AP_2      <<  9) |
		(TEX_2_0   <<  6) |
		(AP_1_0    <<  4) |
		(C_B       <<  2) |
		(1         <<  1) |
		(XN        <<  0);
}

void map_scratchpad(void)
{
	int i;
	unsigned long ttbcr_n;
	unsigned long ttbr0_paddr;
	unsigned long ttbr0_vaddr;
	unsigned long first_page_table_paddr;
	unsigned long first_page_table_vaddr;
	unsigned long pt_entries[4];

	/*
	 * Identity-map the start of the scratchpad (PA 0x00000000-0x00003FFF) to
	 * the VA 0x00000000-0x00003FFF.
	 * To do such thing we will use the first 4 PTEs of the
	 * first page table of TTBR0 (which aren't used).
	 */

	ttbcr_n = get_ttbcr() & 7;
	ttbr0_paddr = get_ttbr0() & ~((1 << (14 - ttbcr_n)) - 1);
	find_paddr(ttbr0_paddr, 0, 0xFFFFFFFF, 0x1000, &ttbr0_vaddr);

	first_page_table_paddr = (*(unsigned int *)ttbr0_vaddr) & 0xFFFFFC00;
	find_paddr(first_page_table_paddr, 0, 0xFFFFFFFF, 0x1000, &first_page_table_vaddr);

	LOG("ttbr0_paddr: 0x%08lX\n", ttbr0_paddr);
	LOG("ttbr0_vaddr: 0x%08lX\n", ttbr0_vaddr);
	LOG("First page table paddr: 0x%08lX\n", first_page_table_paddr);
	LOG("First page table vaddr: 0x%08lX\n", first_page_table_vaddr);
	LOG("\n");

	for (i = 0; i < 4; i++)
		pt_entries[i] = page_table_entry(i << 12);

	ksceKernelCpuUnrestrictedMemcpy((void *)first_page_table_vaddr, pt_entries, sizeof(pt_entries));
	ksceKernelCpuDcacheAndL2WritebackRange((void *)first_page_table_vaddr, sizeof(pt_entries));

	asm volatile(
		/* Drain write buffer */
		"mcr p15, 0, %0, c7, c10, 4\n"
		/* Flush I,D TLBs */
		"mcr p15, 0, %0, c8, c7, 0\n"
		"mcr p15, 0, %0, c8, c6, 0\n"
		"mcr p15, 0, %0, c8, c5, 0\n"
		"mcr p15, 0, %0, c8, c3, 0\n"
		/* Instruction barrier */
		"mcr p15, 0, %0, c7, c5, 4\n"
		: : "r"(0));
}

int alloc_phycont(unsigned int size, SceUID *uid, void **addr)
{
	int ret;
	SceUID mem_uid;
	unsigned int mem_size;
	void *mem_addr;

	mem_size = ALIGN(size, 4096);

	SceKernelAllocMemBlockKernelOpt opt;
	memset(&opt, 0, sizeof(opt));
	opt.size = sizeof(opt);
	opt.attr = 0x200004;
	opt.alignment = 0x1000;
	mem_uid = ksceKernelAllocMemBlock("phycont", 0x30808006, mem_size, &opt);
	if (mem_uid < 0)
		return mem_uid;

	ret = ksceKernelGetMemBlockBase(mem_uid, &mem_addr);
	if (ret < 0) {
		ksceKernelFreeMemBlock(mem_uid);
		return ret;
	}

	if (uid)
		*uid = mem_uid;
	if (addr)
		*addr = mem_addr;

	return 0;
}
