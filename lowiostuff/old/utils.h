#ifndef UTILS_H
#define UTILS_H

#define ALIGN(x, a) (((x) + ((a) - 1)) & ~((a) - 1))
#define abs(x) ((x) < 0 ? (-x) : (x))

#endif
