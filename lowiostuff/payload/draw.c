#include <stdio.h>
#include <string.h>
#include "draw.h"

#define FB_ADDR	0x20200000
#define FB_PITCH 1024
#define FB_W 960
#define FB_H 544

extern const unsigned char msx_font[];

static int console_x = 16;
static int console_y = 16;

void clear_screen()
{
	memset((void *)FB_ADDR, 0x00, FB_PITCH * FB_H * 4);
	//ksceKernelCpuDcacheWritebackRange(fb.base, SCREEN_PITCH * SCREEN_H * 4);
}

void draw_pixel(uint32_t x, uint32_t y, uint32_t color)
{
	uint32_t *p = &((uint32_t *)FB_ADDR)[x + y * FB_PITCH];
	*p = color;
	//ksceKernelCpuDcacheWritebackRange(p, sizeof(*p));
}

void draw_rectangle(uint32_t x, uint32_t y, uint32_t w, uint32_t h, uint32_t color)
{
	int i, j;
	for (i = 0; i < h; i++) {
		for (j = 0; j < w; j++) {
			draw_pixel(x + j, y + i, color);
		}
	}
}

void draw_circle(uint32_t x, uint32_t y, uint32_t radius, uint32_t color)
{
	int r2 = radius * radius;
	int area = r2 << 2;
	int rr = radius << 1;

	int i;
	for (i = 0; i < area; i++) {
		int tx = (i % rr) - radius;
		int ty = (i / rr) - radius;

		if (tx * tx + ty * ty <= r2) {
			draw_pixel(x + tx, y + ty, color);
		}
	}
}

void font_draw_char(int x, int y, uint32_t color, char c)
{
	unsigned char *font = (unsigned char *)(msx_font + (c - (uint32_t)' ') * 8);
	int i, j, pos_x, pos_y;
	for (i = 0; i < 8; ++i) {
		pos_y = y + i*2;
		for (j = 0; j < 8; ++j) {
			pos_x = x + j*2;
			if ((*font & (128 >> j))) {
				draw_pixel(pos_x + 0, pos_y + 0, color);
				draw_pixel(pos_x + 1, pos_y + 0, color);
				draw_pixel(pos_x + 0, pos_y + 1, color);
				draw_pixel(pos_x + 1, pos_y + 1, color);
			}
		}
		++font;
	}
}

void font_draw_string(int x, int y, uint32_t color, const char *string)
{
	if (string == NULL) return;

	int startx = x;
	const char *s = string;

	while (*s) {
		if (*s == '\n') {
			x = startx;
			y += 16;
		} else if (*s == ' ') {
			x += 16;
		} else if(*s == '\t') {
			x += 16*4;
		} else {
			font_draw_char(x, y, color, *s);
			x += 16;
		}
		++s;
	}
}

void console_putch(char c)
{
	if (c == '\n') {
		console_x = 16;
		console_y += 16;
		draw_rectangle(0, console_y, SCREEN_W, 16, BLACK);
	} else if (c == ' ') {
		console_x += 16;
	} else if (c == '\t') {
		console_x += 16 * 4;
	} else {
		font_draw_char(console_x, console_y, WHITE, c);
		console_x += 16;
	}

	if (console_x > SCREEN_W)
		console_x = 16;

	if (console_y + 16 > SCREEN_H) {
		console_y = 16;
		draw_rectangle(0, console_y, SCREEN_W, 16, BLACK);
	}
}

void console_print(const char *s)
{
	if (!s)
		return;

	for (; *s; s++)
		console_putch(*s);
}

int console_get_y(void)
{
	return console_y;
}

void console_set_y(int y)
{
	console_y = y;
	draw_rectangle(0, console_y, SCREEN_W, 16, BLACK);
}
