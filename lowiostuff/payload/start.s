	.cpu cortex-a9
	.align 4
	.code 32

	.text

	.global _start
_start:
	# Disable interrupts and enter SVC mode
	cpsid if, #0x13

	# DACR unrestricted
	mov r0, #0xFFFFFFFF
	mcr p15, 0, r0, c3, c0, 0

	# Disable MMU and caches
	mrc p15, 0, r0, c1, c0, 0
	bic r0, #0b101
	mcr p15, 0, r0, c1, c0, 0
	isb

	# Read CPU ID
	mrc p15, 0, r0, c0, c0, 5
	and r0, r0, #3

	# Setup the SP at the end of the identity map (scratchpad)
	mov sp, #0x00004000
	sub sp, r0, lsl #8

	# Barrier
	ldr r0, =cpu_sync
	ldr r1, [r0]
	add r1, #1
	str r1, [r0]

barrier_check:
	cmp r1, #4
	beq barrier_cont

	ldr r1, [r0]
	b barrier_check

barrier_cont:

	# Jump to the C code
	b main

1:	b 1b

# Variables
cpu_sync: .word 0

	.ltorg
