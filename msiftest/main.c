#include <stdio.h>
#include <math.h>
#include <taihen.h>
#include <psp2kern/kernel/cpu.h>
#include <psp2kern/kernel/modulemgr.h>
#include <psp2kern/kernel/threadmgr.h>
#include <psp2kern/kernel/sysmem.h>
#include <psp2kern/io/fcntl.h>
#include <psp2kern/io/stat.h>
#include <psp2kern/io/stat.h>
#include <psp2kern/display.h>
#include <psp2kern/lowio/i2c.h>
#include <psp2kern/lowio/gpio.h>
#include <psp2kern/registrymgr.h>
#include <psp2kern/kernel/intrmgr.h>
#include <psp2kern/io/fcntl.h>
#include <psp2kern/lowio/pervasive.h>
#include <psp2kern/uart.h>
#include "utils.h"
#include "log.h"
#include "draw.h"
#include "utils.h"

#define LOG(s, ...) \
	do { \
		char __buffer[128]; \
		snprintf(__buffer, sizeof(__buffer), s, ##__VA_ARGS__); \
		uart_print(0, __buffer); \
		if (0) console_print(__buffer); \
		if (0) log_write(__buffer, strlen(__buffer)); \
	} while (0)

#define LOG_HEX(x) LOG("0x%08X\n", x)

void _start() __attribute__ ((weak, alias ("module_start")));

static void uart_print(int bus, const char *str);

static inline void print_hex(const char *name, const void *buff, int size)
{
	int i;

	LOG(name);
	for (i = 0; i < size; i++)
		LOG(" %02X", ((unsigned char *)buff)[i]);
	LOG("\n");
}

extern int module_get_offset(SceUID pid, SceUID modid, int segidx, size_t offset, uintptr_t *addr);

extern void *ksceKernelGetSysbase(void);

extern int ScePervasiveForDriver_5E20D7D5_sdif_unk();
extern int ScePervasiveForDriver_E6728E30_sdif_unk();

extern int ksceSysconCtrlMsPower(int power);
extern int ksceSysconCtrlHdmiCecPower(int power);

extern int ksceMsifReadSector(int sector, char *buffer, int nSectors);
extern int ksceMsifWriteSector(int sector, char *buffer, int nSectors);
extern int ksceMsifEnableSlowMode();
extern int ksceMsifDisableSlowMode();
extern int ksceMsifGetSlowModeState();
//extern int ksceMsifInit(void);
extern int ksceMsifGetMsInfo(void *);

int ioremap(uintptr_t paddr, unsigned int size, SceUID *uid, void **addr);

static SceUID msif_uid;
static unsigned char *msif_addr;
static tai_module_info_t SceMsif_modinfo, SceSyscon_modinfo;

#define MSIF_BASE_ADDR ((void *)msif_addr)
#define MS_SECTOR_SIZE	512

static SceUID SceMsif_SceMsifIns_0x3C_intr_handler_hook_uid = -1;
static tai_hook_ref_t SceMsif_SceMsifIns_0x3C_intr_handler_ref;
static SceUID SceMsif_SceMsifSmshc_0x3D_intr_handler_hook_uid = -1;
static tai_hook_ref_t SceMsif_SceMsifSmshc_0x3D_intr_handler_ref;

static SceUID SceMsif_wait_evflag_sub_C2B618_hook_uid = -1;
static tai_hook_ref_t SceMsif_wait_evflag_sub_C2B618_ref;

static SceUID SceMsif_ms_read_short_data_sub_C2A448_hook_uid = -1;
static tai_hook_ref_t SceMsif_ms_read_short_data_sub_C2A448_ref;

static SceUID SceMsif_ms_write_short_data_sub_C2A3A8_hook_uid = -1;
static tai_hook_ref_t SceMsif_ms_write_short_data_sub_C2A3A8_ref;

static SceUID SceMsif_ms_read_long_data_sub_C2A4E8_hook_uid = -1;
static tai_hook_ref_t SceMsif_ms_read_long_data_sub_C2A4E8_ref;

static SceUID SceSblSsMgr_ksceSblSsMgrGetRandomDataCrop_hook_uid = -1;
static tai_hook_ref_t SceSblSsMgr_ksceSblSsMgrGetRandomDataCrop_ref;

static tai_hook_ref_t SceSyscon_ksceSysconCtrlMsPower_ref;
static SceUID SceSyscon_ksceSysconCtrlMsPower_hook_uid = -1;


static int SceSyscon_ksceSysconCtrlMsPower_hook_func(int enable)
{
	LOG("SceSyscon_ksceSysconCtrlMsPower(%d)\n\n", enable);

	//return 0;

	return TAI_CONTINUE(int, SceSyscon_ksceSysconCtrlMsPower_ref, enable);
}

static int SceMsif_SceMsifIns_0x3C_intr_handler_hook_func(int code, int arg)
{
	LOG("SceMsif_SceMsifIns_0x3C_intr_handler_hook_func\n");

	//return 0;

	return TAI_CONTINUE(int, SceMsif_SceMsifIns_0x3C_intr_handler_ref, code, arg);
}

static int SceMsif_SceMsifSmshc_0x3D_intr_handler_hook_func(int code, int arg)
{
	/* Logging here causes the card to timeout :( */
	//LOG("SceMsif_SceMsifSmshc_0x3D_intr_handler_hook_func\n");

	//return 0;

	return TAI_CONTINUE(int, SceMsif_SceMsifSmshc_0x3D_intr_handler_ref, code, arg);
}

static int SceMsif_wait_evflag_sub_C2B618_hook_func(SceUID uid, int ms)
{
	LOG("SceMsif_wait_evflag_sub_C2B618_hook_func(0x%08X, 0x%08X)\n", uid, ms);

	//if (doing)
	//	return 0;

	return TAI_CONTINUE(int, SceMsif_wait_evflag_sub_C2B618_ref, uid, ms);
}

static int SceMsif_ms_read_short_data_sub_C2A448_hook_func(void *subctx, int cmd, int size, void *buff, int delay)
{
	int ret;

	LOG("SceMsif_ms_read_short_data(%p, 0x%02X, 0x%02X, %p, %d)\n",
		subctx, cmd, size, buff, delay);

	ret = TAI_CONTINUE(int, SceMsif_ms_read_short_data_sub_C2A448_ref,
		subctx, cmd, size, buff, delay);

	print_hex("", buff, size);

	return ret;
}

static int SceMsif_ms_write_short_data_sub_C2A3A8_hook_func(void *subctx, int cmd, int size, void *buff, int delay)
{
	LOG("SceMsif_ms_write_short_data(%p, 0x%02X, 0x%02X, %p, %d)\n",
		subctx, cmd, size, buff, delay);

	/*static unsigned char mybuff[] = {
		0x67, 0x41, 0xD4, 0x38, 0x06, 0x6F, 0x7D, 0xF7, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
	};*/

	//if (cmd == 0x48)
	//	buff = mybuff;

	print_hex("", buff, size);

	return TAI_CONTINUE(int, SceMsif_ms_write_short_data_sub_C2A3A8_ref,
		subctx, cmd, size, buff, delay);
}

static int SceMsif_ms_read_long_data_sub_C2A4E8_hook_func(void *subctx, int cmd, void *buff, int delay)
{
	int ret;

	LOG("SceMsif_ms_read_long_data(%p, 0x%02X, %p, %d)\n",
		subctx, cmd, buff, delay);

	ret = TAI_CONTINUE(int, SceMsif_ms_read_long_data_sub_C2A4E8_ref,
		subctx, cmd, buff, delay);

	print_hex("", buff, 512);

	return ret;
}

static int SceSblSsMgr_ksceSblSsMgrGetRandomDataCrop_hook_func(unsigned char *buff, unsigned int size, int unk)
{
	int ret;

	LOG("ksceSblSsMgrGetRandomDataCrop(%p, 0x%08X, 0x%08X)\n",
		buff, size, unk);

	ret = TAI_CONTINUE(int, SceSblSsMgr_ksceSblSsMgrGetRandomDataCrop_ref,
		buff, size, unk);


	//memset(buff, 0x00, size);

	print_hex("", buff, size);

	return ret;
}


int module_start(SceSize argc, const void *args)
{
	kscePervasiveUartClockEnable(0);
	kscePervasiveUartResetDisable(0);
	ksceUartInit(0);

	//log_reset();
	//map_framebuffer();
	//uart_print(0, "\e[1;1H\e[2J");
	LOG("\n\nmsiftest by xerpi\n");

	//ioremap(0xE0900000, 0x1000, &msif_uid, (void **)&msif_addr);
	//LOG("MSIF addr: %p\n", msif_addr);

	SceMsif_modinfo.size = sizeof(SceMsif_modinfo);
	taiGetModuleInfoForKernel(KERNEL_PID, "SceMsif", &SceMsif_modinfo);
	LOG("SceMsif modid: 0x%08X\n", SceMsif_modinfo.modid);

	SceSyscon_modinfo.size = sizeof(SceSyscon_modinfo);
	taiGetModuleInfoForKernel(KERNEL_PID, "SceSyscon", &SceSyscon_modinfo);

	/*
	 * Function hooks
	 */
	SceSyscon_ksceSysconCtrlMsPower_hook_uid = taiHookFunctionExportForKernel(KERNEL_PID,
		&SceSyscon_ksceSysconCtrlMsPower_ref, "SceSyscon", 0x60A35F64,
		0x710A7CF0, SceSyscon_ksceSysconCtrlMsPower_hook_func);

	SceMsif_SceMsifIns_0x3C_intr_handler_hook_uid = taiHookFunctionOffsetForKernel(KERNEL_PID,
		&SceMsif_SceMsifIns_0x3C_intr_handler_ref, SceMsif_modinfo.modid, 0,
		0x00C2B75C - 0x00C28000, 1, SceMsif_SceMsifIns_0x3C_intr_handler_hook_func);

	SceMsif_SceMsifSmshc_0x3D_intr_handler_hook_uid = taiHookFunctionOffsetForKernel(KERNEL_PID,
		&SceMsif_SceMsifSmshc_0x3D_intr_handler_ref, SceMsif_modinfo.modid, 0,
		0x00C2B7B8 - 0x00C28000, 1, SceMsif_SceMsifSmshc_0x3D_intr_handler_hook_func);

	/*SceMsif_wait_evflag_sub_C2B618_hook_uid = taiHookFunctionOffsetForKernel(KERNEL_PID,
		&SceMsif_wait_evflag_sub_C2B618_ref, SceMsif_modinfo.modid, 0,
		0x00C2B618 - 0x00C28000, 1, SceMsif_wait_evflag_sub_C2B618_hook_func);*/

	SceMsif_ms_read_short_data_sub_C2A448_hook_uid = taiHookFunctionOffsetForKernel(KERNEL_PID,
		&SceMsif_ms_read_short_data_sub_C2A448_ref, SceMsif_modinfo.modid, 0,
		0x00C2A448 - 0x00C28000, 1, SceMsif_ms_read_short_data_sub_C2A448_hook_func);

	SceMsif_ms_write_short_data_sub_C2A3A8_hook_uid = taiHookFunctionOffsetForKernel(KERNEL_PID,
		&SceMsif_ms_write_short_data_sub_C2A3A8_ref, SceMsif_modinfo.modid, 0,
		0x00C2A3A8 - 0x00C28000, 1, SceMsif_ms_write_short_data_sub_C2A3A8_hook_func);

	SceMsif_ms_read_long_data_sub_C2A4E8_hook_uid = taiHookFunctionOffsetForKernel(KERNEL_PID,
		&SceMsif_ms_read_long_data_sub_C2A4E8_ref, SceMsif_modinfo.modid, 0,
		0x00C2A4E8 - 0x00C28000, 1, SceMsif_ms_read_long_data_sub_C2A4E8_hook_func);

	SceSblSsMgr_ksceSblSsMgrGetRandomDataCrop_hook_uid = taiHookFunctionExportForKernel(KERNEL_PID,
		&SceSblSsMgr_ksceSblSsMgrGetRandomDataCrop_ref, "SceSblSsMgr", 0x61E9428D,
		0x4DD1B2E5, SceSblSsMgr_ksceSblSsMgrGetRandomDataCrop_hook_func);


	LOG("Init done\n");

	/* Other stuff */

	/*unsigned char buffer[512] = {0};
	int ret = ksceMsifReadSector(0, buffer, 1);
	LOG("ret: 0x%08X\n", ret);
	for (int i = 0; i < 10; i++)
		LOG("0x%02X\n", buffer[i]);
	LOG("%s\n", buffer);
	LOG("0x%02X 0x%02X\n", buffer[0x1FE], buffer[0x1FF]);*/

	return SCE_KERNEL_START_SUCCESS;
}

int module_stop(SceSize argc, const void *args)
{
	LOG("module_stop()\n");

	while (1)
		ksceKernelDelayThread(100 * 1000);

	if (SceSyscon_ksceSysconCtrlMsPower_hook_uid > 0) {
		taiHookReleaseForKernel(SceSyscon_ksceSysconCtrlMsPower_hook_uid,
			SceSyscon_ksceSysconCtrlMsPower_ref);
	}

	if (SceMsif_SceMsifIns_0x3C_intr_handler_hook_uid > 0) {
		taiHookReleaseForKernel(SceMsif_SceMsifIns_0x3C_intr_handler_hook_uid,
			SceMsif_SceMsifIns_0x3C_intr_handler_ref);
	}

	if (SceMsif_SceMsifSmshc_0x3D_intr_handler_hook_uid > 0) {
		taiHookReleaseForKernel(SceMsif_SceMsifSmshc_0x3D_intr_handler_hook_uid,
			SceMsif_SceMsifSmshc_0x3D_intr_handler_ref);
	}

	if (SceMsif_wait_evflag_sub_C2B618_hook_uid > 0) {
		taiHookReleaseForKernel(SceMsif_wait_evflag_sub_C2B618_hook_uid,
			SceMsif_wait_evflag_sub_C2B618_ref);
	}

	if (SceMsif_ms_read_short_data_sub_C2A448_hook_uid > 0) {
		taiHookReleaseForKernel(SceMsif_ms_read_short_data_sub_C2A448_hook_uid,
			SceMsif_ms_read_short_data_sub_C2A448_ref);
	}

	if (SceMsif_ms_write_short_data_sub_C2A3A8_hook_uid > 0) {
		taiHookReleaseForKernel(SceMsif_ms_write_short_data_sub_C2A3A8_hook_uid,
			SceMsif_ms_write_short_data_sub_C2A3A8_ref);
	}

	if (SceMsif_ms_read_long_data_sub_C2A4E8_hook_uid > 0) {
		taiHookReleaseForKernel(SceMsif_ms_read_long_data_sub_C2A4E8_hook_uid,
			SceMsif_ms_read_long_data_sub_C2A4E8_ref);
	}

	if (SceSblSsMgr_ksceSblSsMgrGetRandomDataCrop_hook_uid > 0) {
		taiHookReleaseForKernel(SceSblSsMgr_ksceSblSsMgrGetRandomDataCrop_hook_uid,
			SceSblSsMgr_ksceSblSsMgrGetRandomDataCrop_ref);
	}

	ksceKernelFreeMemBlock(msif_uid);

	//unmap_framebuffer();

	return SCE_KERNEL_STOP_SUCCESS;
}

static void uart_print(int bus, const char *str)
{
	while (*str) {
		ksceUartWrite(bus, *str);
		if (*str == '\n')
			ksceUartWrite(bus, '\r');
		str++;
	}
}

int ioremap(uintptr_t paddr, unsigned int size, SceUID *uid, void **addr)
{
	int ret;
	SceUID mem_uid;
	unsigned int mem_size;
	void *mem_addr;

	mem_size = ALIGN(size, 4096);

	SceKernelAllocMemBlockKernelOpt opt;
	memset(&opt, 0, sizeof(opt));
	opt.size = sizeof(opt);
	opt.attr = 2;
	opt.paddr = paddr;
	mem_uid = ksceKernelAllocMemBlock("ioremap", 0x20100206, mem_size, &opt);
	if (mem_uid < 0)
		return mem_uid;

	ret = ksceKernelGetMemBlockBase(mem_uid, &mem_addr);
	if (ret < 0) {
		ksceKernelFreeMemBlock(mem_uid);
		return ret;
	}

	if (uid)
		*uid = mem_uid;
	if (addr)
		*addr = mem_addr;

	return 0;
}
