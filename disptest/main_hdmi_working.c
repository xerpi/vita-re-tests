#include <stdio.h>
#include <math.h>
#include <taihen.h>
#include <psp2kern/kernel/cpu.h>
#include <psp2kern/kernel/modulemgr.h>
#include <psp2kern/kernel/threadmgr.h>
#include <psp2kern/kernel/sysmem.h>
#include <psp2kern/io/fcntl.h>
#include <psp2kern/io/stat.h>
#include <psp2kern/io/stat.h>
#include <psp2kern/display.h>
#include <psp2kern/lowio/i2c.h>
#include <psp2kern/lowio/gpio.h>
#include <psp2kern/registrymgr.h>
#include <psp2kern/kernel/intrmgr.h>
#include "adv7511.h"
#include "utils.h"
#include "log.h"
#include "draw.h"

#define LOG(s, ...) \
	do { \
		char buffer[256]; \
		snprintf(buffer, sizeof(buffer), s, ##__VA_ARGS__); \
		uart_print(0, buffer); \
		if (0) console_print(buffer); \
	} while (0)

void _start() __attribute__ ((weak, alias ("module_start")));

extern int ksceUartReadAvailable(int device);
extern int ksceUartWrite(int device, unsigned char data);
extern int ksceUartRead(int device);
extern int ksceUartInit(int device);

extern int ScePervasiveForDriver_18DD8043(int device);
extern int ScePervasiveForDriver_788B6C61(int device);
extern int ScePervasiveForDriver_A7CE7DCC(int device);
extern int ScePervasiveForDriver_EFD084D8(int device);

extern int SceSysconForDriver_62155962(int);
extern int SceSysconForDriver_FF86F4C5(void);

static void uart_print(int bus, const char *str)
{
	while (*str) {
		ksceUartWrite(bus, *str);
		if (*str == '\n')
			ksceUartWrite(bus, '\r');
		str++;
	}
}

typedef unsigned int u32;
int ioremap(uintptr_t paddr, unsigned int size, SceUID *uid, void **addr);

#define RGBA8(r, g, b, a)      ((((a)&0xFF)<<24) | (((b)&0xFF)<<16) | (((g)&0xFF)<<8) | (((r)&0xFF)<<0))

#define RED   RGBA8(255, 0,   0,   255)
#define GREEN RGBA8(0,   255, 0,   255)
#define BLUE  RGBA8(0,   0,   255, 255)
#define CYAN  RGBA8(0,   255, 255, 255)
#define LIME  RGBA8(50,  205, 50,  255)
#define PURP  RGBA8(147, 112, 219, 255)
#define WHITE RGBA8(255, 255, 255, 255)
#define BLACK RGBA8(0,   0,   0,   255)

static int run = 1;
static SceUID thid;
static SceUID iftu1regA_uid, iftu1regB_uid, iftu1creg_uid;
static unsigned char *iftu1regA_addr, *iftu1regB_addr, *iftu1creg_addr;
static SceUID dsi1_uid;
static unsigned char *dsi1_addr;

typedef unsigned char u8;

#define BIT(x) (1 << (x))
#define ARRAY_SIZE(x) (sizeof(x) / sizeof((x)[0]))

#define GPIO_PORT_HDMI_BRIDGE	15

#define HDMI_I2C_BUS		1
#define HDMI_I2C_ADDR		0x7A
#define HDMI_I2C_CEC_ADDR	0x7C

struct reg_sequence {
	unsigned char reg;
	unsigned char cmd;
};

static const struct reg_sequence adv7511_fixed_registers[] = {
	{ 0x98, 0x03 },
	{ 0x9a, 0xe0 },
	{ 0x9c, 0x30 },
	{ 0x9d, 0x61 },
	{ 0xa2, 0xa4 },
	{ 0xa3, 0xa4 },
	{ 0xe0, 0xd0 },
	{ 0xf9, 0x00 },
	{ 0x55, 0x02 },
};

static const struct reg_sequence adv7533_fixed_registers[] = {
	{ 0x16, 0x20 },
	{ 0x9a, 0xe0 },
	{ 0xba, 0x70 },
	{ 0xde, 0x82 },
	{ 0xe4, 0x40 },
	{ 0xe5, 0x80 },
};

static const struct reg_sequence adv7533_cec_fixed_registers[] = {
	{ 0x15, 0xd0 },
	{ 0x17, 0xd0 },
	{ 0x24, 0x20 },
	{ 0x57, 0x11 },
	{ 0x05, 0xc8 },
};

static inline void hdmi_i2c_cmd_write_1(unsigned char addr, unsigned char reg, unsigned char data)
{
	unsigned char buff[2] = {reg, data};
	ksceI2cTransferWrite(HDMI_I2C_BUS, addr, buff, sizeof(buff));
}

static inline void hdmi_i2c_cmd_write_read(unsigned char addr, unsigned char reg, unsigned char *data, int size)
{
	unsigned char write_buff = reg;
	ksceI2cTransferWriteRead(HDMI_I2C_BUS,
		addr, &write_buff, sizeof(write_buff),
		addr, data, size);
}

static inline void hdmi_i2c_cmd_write_read_1(unsigned char addr, unsigned char reg, unsigned char *data)
{
	hdmi_i2c_cmd_write_read(addr, reg, data, sizeof(*data));
}

void hdmi_i2c_update_bits(unsigned char addr, unsigned char reg,
			  unsigned char mask, unsigned char val)
{
	unsigned char data;

	hdmi_i2c_cmd_write_read_1(addr, reg, &data);
	data &= ~mask;
	data |= val & mask;
	hdmi_i2c_cmd_write_1(addr, reg, data);
}

#define hdmi_write(addr,reg, data) \
	hdmi_i2c_cmd_write_1(addr, reg, data)

static inline u8 hdmi_read(u8 addr, u8 reg)
{
	unsigned char data;
	hdmi_i2c_cmd_write_read_1(addr, reg, &data);
	return data;
}

void hdmi_set_bit(u8 addr, u8 reg, u8 bit)
{
	u8 val;
	val = hdmi_read(addr, reg);
	val |= bit;
	hdmi_write(addr, reg, val);
}

void hdmi_clr_bit(u8 addr, u8 reg, u8 bit)
{
	u8 val;
	val = hdmi_read(addr, reg);
	val &= ~bit;
	hdmi_write(addr, reg, val);
}


static int thread(SceSize args, void *argp)
{
	//uart_print(0, "\e[1;1H\e[2J");

	ksceKernelMaskIntr(249);
	//ksceKernelMaskIntr(210);

	/* Poweroff */
	hdmi_i2c_update_bits(HDMI_I2C_ADDR, ADV7511_REG_POWER,
			   ADV7511_POWER_POWER_DOWN, ADV7511_POWER_POWER_DOWN);
	hdmi_write(HDMI_I2C_CEC_ADDR, 0x03, 0x0b);
	hdmi_write(HDMI_I2C_CEC_ADDR, 0x27, 0x0b);

	ksceKernelDelayThread(5 * 1000);

	/* Reset HDMI */
	ksceGpioPortSet(0, GPIO_PORT_HDMI_BRIDGE);
	ksceKernelDelayThread(10 * 1000);
	ksceGpioPortClear(0, GPIO_PORT_HDMI_BRIDGE);
	ksceKernelDelayThread(5 * 1000);

	hdmi_i2c_cmd_write_1(HDMI_I2C_ADDR, ADV7511_REG_CEC_I2C_ADDR,
		     HDMI_I2C_CEC_ADDR);

	unsigned char status;
	do {
		hdmi_i2c_cmd_write_read_1(HDMI_I2C_ADDR, ADV7511_REG_STATUS, &status);
	} while (!(status & ADV7511_STATUS_HPD));

	hdmi_i2c_update_bits(HDMI_I2C_ADDR, ADV7511_REG_POWER,
			   ADV7511_POWER_POWER_DOWN, 0);

	hdmi_i2c_update_bits(HDMI_I2C_ADDR, ADV7511_REG_POWER2,
			   ADV7511_REG_POWER2_HPD_SRC_MASK,
			   ADV7511_REG_POWER2_HPD_SRC_HPD);

	for (int i = 0; i < ARRAY_SIZE(adv7533_fixed_registers); i++)
		hdmi_i2c_cmd_write_1(HDMI_I2C_ADDR,
				     adv7533_fixed_registers[i].reg,
				     adv7533_fixed_registers[i].cmd);

	for (int i = 0; i < ARRAY_SIZE(adv7533_cec_fixed_registers); i++)
		hdmi_i2c_cmd_write_1(HDMI_I2C_CEC_ADDR,
				     adv7533_cec_fixed_registers[i].reg,
				     adv7533_cec_fixed_registers[i].cmd);

	static const int test_mode = 1;
	static const unsigned char lanes = 3;

	if (test_mode) {
		/* set pixel clock auto mode */
		hdmi_write(HDMI_I2C_CEC_ADDR, 0x16, 0x00);
	} else {
		static const u8 clock_div_by_lanes[] = { 6, 4, 3 }; /* 2, 3, 4 lanes */
		/* set pixel clock divider mode */
		hdmi_write(HDMI_I2C_CEC_ADDR, 0x16,
			clock_div_by_lanes[lanes - 2] << 3);
	}

	if (test_mode) {
		unsigned int htotal = 1650;
		unsigned int vtotal = 750;
		unsigned int hsw = 0x28;
		unsigned int hfp = 0x6E;
		unsigned int hbp = 0xDC;
		unsigned int vsw = 5;
		unsigned int vfp = 5;
		unsigned int vbp = 0x14;

		/* horizontal porch params */
		hdmi_write(HDMI_I2C_CEC_ADDR, 0x28, htotal >> 4);
		hdmi_write(HDMI_I2C_CEC_ADDR, 0x29,
			     (htotal << 4) & 0xff);
		hdmi_write(HDMI_I2C_CEC_ADDR, 0x2a, hsw >> 4);
		hdmi_write(HDMI_I2C_CEC_ADDR, 0x2b, (hsw << 4) & 0xff);
		hdmi_write(HDMI_I2C_CEC_ADDR, 0x2c, hfp >> 4);
		hdmi_write(HDMI_I2C_CEC_ADDR, 0x2d, (hfp << 4) & 0xff);
		hdmi_write(HDMI_I2C_CEC_ADDR, 0x2e, hbp >> 4);
		hdmi_write(HDMI_I2C_CEC_ADDR, 0x2f, (hbp << 4) & 0xff);

		/* vertical porch params */
		hdmi_write(HDMI_I2C_CEC_ADDR, 0x30, vtotal >> 4);
		hdmi_write(HDMI_I2C_CEC_ADDR, 0x31,
			     (vtotal << 4) & 0xff);
		hdmi_write(HDMI_I2C_CEC_ADDR, 0x32, vsw >> 4);
		hdmi_write(HDMI_I2C_CEC_ADDR, 0x33, (vsw << 4) & 0xff);
		hdmi_write(HDMI_I2C_CEC_ADDR, 0x34, vfp >> 4);
		hdmi_write(HDMI_I2C_CEC_ADDR, 0x35, (vfp << 4) & 0xff);
		hdmi_write(HDMI_I2C_CEC_ADDR, 0x36, vbp >> 4);
		hdmi_write(HDMI_I2C_CEC_ADDR, 0x37, (vbp << 4) & 0xff);

		/* set number of dsi lanes */
		hdmi_write(HDMI_I2C_CEC_ADDR, 0x1C, 3 << 4);

		hdmi_clr_bit(HDMI_I2C_CEC_ADDR, 0x05, 0x20);
		hdmi_clr_bit(HDMI_I2C_CEC_ADDR, 0x05, 0x10);

		/* Set CEC Power Mode = Always Active. Clear bits before*/
		hdmi_clr_bit(HDMI_I2C_CEC_ADDR, 0xBE, 0x03);
		hdmi_set_bit(HDMI_I2C_CEC_ADDR, 0xBE, 0x01);

		/* Fixed Register Settings per PG*/
		hdmi_write(HDMI_I2C_CEC_ADDR, 0x24, 0x20);
		hdmi_write(HDMI_I2C_CEC_ADDR, 0x26, 0x3C);
		hdmi_write(HDMI_I2C_ADDR, 0x9A, 0xE0);
		hdmi_write(HDMI_I2C_ADDR, 0x9B, 0x19);
		hdmi_write(HDMI_I2C_ADDR, 0xBA, 0x70);
		hdmi_write(HDMI_I2C_ADDR, 0xDE, 0x82);
		hdmi_write(HDMI_I2C_ADDR, 0xE4, 0x40);
		hdmi_write(HDMI_I2C_ADDR, 0xE5, 0x80);

		/* reset internal timing generator */
		hdmi_write(HDMI_I2C_CEC_ADDR, 0x27, 0xcb);
		hdmi_write(HDMI_I2C_CEC_ADDR, 0x27, 0x8b);
		hdmi_write(HDMI_I2C_CEC_ADDR, 0x27, 0xcb);
	} else {
		/* disable internal timing generator */
		hdmi_write(HDMI_I2C_CEC_ADDR, 0x27, 0x0b);
	}

	/* 09-03 AVI Infoframe - RGB - 16-9 Aspect Ratio */
	hdmi_write(HDMI_I2C_ADDR, 0x55, 0x10);
	hdmi_write(HDMI_I2C_ADDR, 0x56, 0x28);
	/* 04-04 GC Packet Enable */
	hdmi_write(HDMI_I2C_ADDR, 0x40, 0x80);
	/* 04-06 GC Colour Depth - 24 Bit */
	hdmi_write(HDMI_I2C_ADDR, 0x4c, 0x04);
	/* 04-09 Down Dither Output Colour Depth - 8 Bit (default) */
	hdmi_write(HDMI_I2C_ADDR, 0x49, 0x00);
	/* 07-01 CEC Power Mode - Always Active */
	hdmi_write(HDMI_I2C_CEC_ADDR, 0xbe, 0x3d);

	/* HDMI Output Settings*/
	/* Set HDMI/DVI Mode Select = HDMI Mode Enabled - bit 1*/
	hdmi_set_bit(HDMI_I2C_ADDR, 0xAF, 0x2);
	/* Enable GC Packet */
	hdmi_set_bit(HDMI_I2C_ADDR, 0x40, 0x80);
	/* Set Color Depth to 24 Bits/Pixel. Clear field before*/
	hdmi_clr_bit(HDMI_I2C_ADDR, 0x4C, 0x0F);
	hdmi_set_bit(HDMI_I2C_ADDR, 0x4C, 0x04);
	/* Set Active Format Aspect Ratio = 4:3 (Center) Clear field before*/
	hdmi_clr_bit(HDMI_I2C_ADDR, 0x56, 0x0F);
	hdmi_set_bit(HDMI_I2C_ADDR, 0x56, 0x09);
	/* Set V1P2 Enable = +1.2V*/
	hdmi_set_bit(HDMI_I2C_ADDR, 0xE4, 0xC0);

	if (test_mode)
		/*enable test mode */
		hdmi_write(HDMI_I2C_CEC_ADDR, 0x55, 0x80);
	else
		/* disable test mode */
		hdmi_write(HDMI_I2C_CEC_ADDR, 0x55, 0x00);

	/* enable hdmi */
	hdmi_write(HDMI_I2C_CEC_ADDR, 0x03, 0x89);

	hdmi_i2c_cmd_write_1(HDMI_I2C_ADDR, 0xAF, 0b10010110); // 0x96 = 0b10010110
	hdmi_i2c_cmd_write_1(HDMI_I2C_CEC_ADDR, 0x81, 0x07);

	/* Wait for the BKSV flag */
	unsigned char data;
	do {
		hdmi_i2c_cmd_write_read_1(HDMI_I2C_ADDR, 0x97, &data);
	} while (!(data & (1 << 6)));

	hdmi_i2c_cmd_write_1(HDMI_I2C_ADDR, 0x97, (data & ~0xBF) | (1 << 6));

	/*enable test mode */
	//hdmi_write(HDMI_I2C_CEC_ADDR, 0x55, 0x80);

	LOG("HDMI done!\n");

	int y = console_get_y();
	while (run) {
		//console_set_y(y);

		/*unsigned char reg_0x92[6];
		hdmi_i2c_cmd_write_read(HDMI_I2C_CEC_ADDR, 0x92, reg_0x92, sizeof(reg_0x92));

		for (int i = 0; i < sizeof(reg_0x92); i++)
			LOG("reg 0x%02X: 0x%02X\n", 0x92 + i, reg_0x92[i]);

		LOG("\n");
		ksceKernelDelayThread(250 * 1000);*/
	}

	hdmi_write(HDMI_I2C_CEC_ADDR, 0x55, 0x00);
	ksceKernelUnmaskIntr(249);
	//ksceKernelUnmaskIntr(210);

	return 0;
}

static SceUID SceHdmi_subintr_handler_hook;
static tai_hook_ref_t SceHdmi_subintr_handler_ref;

static SceUID handle_DDC_Controller_Error_interrupt_hook;
static tai_hook_ref_t handle_DDC_Controller_Error_interrupt_ref;

static SceUID handle_HPD_interrupt_hook;
static tai_hook_ref_t handle_HPD_interrupt_ref;

static SceUID handle_HDCP_authenticated_interrupt_hook;
static tai_hook_ref_t handle_HDCP_authenticated_interrupt_ref;

static SceUID handle_EDID_ready_interrupt_hook;
static tai_hook_ref_t handle_EDID_ready_interrupt_ref;

static SceUID handle_BKSV_flag_interrupt_hook;
static tai_hook_ref_t handle_BKSV_flag_interrupt_ref;

static SceUID handle_rest_interrupts_hook;
static tai_hook_ref_t handle_rest_interrupts_ref;

static SceUID sub_BF0B9C_hook;
static tai_hook_ref_t sub_BF0B9C_ref;


/*
SceHdmiIntr_subintr_handler_patched
reg 0x92: 0x6A
reg 0x93: 0xA4
reg 0x94: 0xBD
reg 0x95: 0x9F
reg 0x96: 0x01
reg 0x97: 0x00
HDMI done!
SceHdmiIntr_subintr_handler_patched
reg 0x92: 0x00
reg 0x93: 0x00
reg 0x94: 0x00
reg 0x95: 0x00
reg 0x96: 0x38
reg 0x97: 0x98
*/

static int SceHdmiIntr_subintr_handler_patched(void)
{
	/*static int i = 0;
	if (i++ > 1)
		return 0;*/

	LOG("SceHdmiIntr_subintr_handler_patched\n");

	unsigned char reg_0x92[6];
	hdmi_i2c_cmd_write_read(HDMI_I2C_CEC_ADDR, 0x92, reg_0x92, sizeof(reg_0x92));

	for (int i = 0; i < sizeof(reg_0x92); i++)
		LOG("reg 0x%02X: 0x%02X\n", 0x92 + i, reg_0x92[i]);

	return TAI_CONTINUE(int, SceHdmi_subintr_handler_ref);
}

static int handle_DDC_Controller_Error_interrupt_patched(unsigned char reg_0x92[6])
{
	LOG("handle_DDC_Controller_Error_interrupt_patched\n");

	return TAI_CONTINUE(int, handle_DDC_Controller_Error_interrupt_ref, reg_0x92);
}

static int handle_HPD_interrupt_patched(unsigned char reg_0x92[6])
{
	LOG("handle_HPD_interrupt_patched\n");

	return TAI_CONTINUE(int, handle_HPD_interrupt_ref, reg_0x92);
}

static int handle_HDCP_authenticated_interrupt_patched(unsigned char reg_0x92[6])
{
	LOG("handle_HDCP_authenticated_interrupt_patched\n");

	return TAI_CONTINUE(int, handle_HDCP_authenticated_interrupt_ref, reg_0x92);
}

static int handle_EDID_ready_interrupt_patched(unsigned char reg_0x92[6])
{
	LOG("handle_EDID_ready_interrupt_patched\n");

	return TAI_CONTINUE(int, handle_EDID_ready_interrupt_ref, reg_0x92);
}

static int handle_BKSV_flag_interrupt_patched(unsigned char reg_0x92[6])
{
	LOG("handle_BKSV_flag_interrupt_patched\n");

	return TAI_CONTINUE(int, handle_BKSV_flag_interrupt_ref, reg_0x92);
}

static int handle_rest_interrupts_patched(unsigned char reg_0x92[6])
{
	LOG("handle_rest_interrupts_patched\n");


	for (int i = 0; i < 6; i++)
		LOG("reg 0x%02X: 0x%02X\n", 0x92 + i, reg_0x92[i]);


	LOG("sub_BF16B8(0x%08X, 0x%02X)\n", (reg_0x92[5] << 0x10) | (reg_0x92[4] << 8) | reg_0x92[1], reg_0x92[5]);

	return TAI_CONTINUE(int, handle_rest_interrupts_ref, reg_0x92);
}

static int sub_BF0B9C_patched(void)
{
	LOG("sub_BF0B9C_patched\n");

	return TAI_CONTINUE(int, sub_BF0B9C_ref);
}

int module_start(SceSize argc, const void *args)
{
	ScePervasiveForDriver_EFD084D8(0); // Turn on clock
	ScePervasiveForDriver_A7CE7DCC(0); // Out of reset
	ksceUartInit(0);
	map_framebuffer();
	LOG("\n\ndisptest by xerpi\n");

	ioremap(0xE5060000, 0x1000, &dsi1_uid, (void **)&dsi1_addr);

	ioremap(0xE5030000, 0x1000, &iftu1regA_uid, (void **)&iftu1regA_addr);
	ioremap(0xE5031000, 0x1000, &iftu1regB_uid, (void **)&iftu1regB_addr);
	ioremap(0xE5032000, 0x1000, &iftu1creg_uid, (void **)&iftu1creg_addr);

	LOG("Iftu1RegA addr: %p\n", iftu1regA_addr);
	LOG("Iftu1RegB addr: %p\n", iftu1regB_addr);
	LOG("Iftu1cReg addr: %p\n", iftu1creg_addr);

	int ratio;
	ksceRegMgrGetKeyInt("/CONFIG/DISPLAY", "hdmi_out_scaling_ratio", &ratio);
	LOG("raio: 0x%08X\n", ratio);

	tai_module_info_t tai_info;
	tai_info.size = sizeof(tai_module_info_t);
	taiGetModuleInfoForKernel(KERNEL_PID, "SceHdmi", &tai_info);

	SceHdmi_subintr_handler_hook = taiHookFunctionOffsetForKernel(KERNEL_PID,
		&SceHdmi_subintr_handler_ref, tai_info.modid, 0,
		0xBEC9CC - 0xBE8000, 0x1, SceHdmiIntr_subintr_handler_patched);

	handle_DDC_Controller_Error_interrupt_hook = taiHookFunctionOffsetForKernel(KERNEL_PID,
		&handle_DDC_Controller_Error_interrupt_ref, tai_info.modid, 0,
		0x00BECAEC - 0xBE8000, 0x1, handle_DDC_Controller_Error_interrupt_patched);

	handle_HPD_interrupt_hook = taiHookFunctionOffsetForKernel(KERNEL_PID,
		&handle_HPD_interrupt_ref, tai_info.modid, 0,
		0x00BECA20 - 0xBE8000, 0x1, handle_HPD_interrupt_patched);

	handle_HDCP_authenticated_interrupt_hook = taiHookFunctionOffsetForKernel(KERNEL_PID,
		&handle_HDCP_authenticated_interrupt_ref, tai_info.modid, 0,
		0x00BECA14 - 0xBE8000, 0x1, handle_HDCP_authenticated_interrupt_patched);

	handle_EDID_ready_interrupt_hook = taiHookFunctionOffsetForKernel(KERNEL_PID,
		&handle_EDID_ready_interrupt_ref, tai_info.modid, 0,
		0x00BECA08 - 0xBE8000, 0x1, handle_EDID_ready_interrupt_patched);

	handle_BKSV_flag_interrupt_hook = taiHookFunctionOffsetForKernel(KERNEL_PID,
		&handle_BKSV_flag_interrupt_ref, tai_info.modid, 0,
		0x00BEC9FC - 0xBE8000, 0x1, handle_BKSV_flag_interrupt_patched);

	handle_rest_interrupts_hook = taiHookFunctionOffsetForKernel(KERNEL_PID,
		&handle_rest_interrupts_ref, tai_info.modid, 0,
		0x00BEC9E4 - 0xBE8000, 0x1, handle_rest_interrupts_patched);

	sub_BF0B9C_hook = taiHookFunctionOffsetForKernel(KERNEL_PID,
		&sub_BF0B9C_ref, tai_info.modid, 0,
		0xBF0B9C - 0xBE8000, 0x1, sub_BF0B9C_patched);

	LOG("\n");

	thid = ksceKernelCreateThread("disptest", thread, 0x00, 0x1000, 0, 1 << 0, 0);

	ksceKernelStartThread(thid, 0, NULL);

	return SCE_KERNEL_START_SUCCESS;
}

int module_stop(SceSize argc, const void *args)
{
	run = 0;
	ksceKernelWaitThreadEnd(thid, NULL, NULL);

	ksceKernelFreeMemBlock(dsi1_uid);

	ksceKernelFreeMemBlock(iftu1regA_uid);
	ksceKernelFreeMemBlock(iftu1regB_uid);
	ksceKernelFreeMemBlock(iftu1creg_uid);

	taiHookReleaseForKernel(SceHdmi_subintr_handler_hook,
		SceHdmi_subintr_handler_ref);

	taiHookReleaseForKernel(handle_DDC_Controller_Error_interrupt_hook,
		handle_DDC_Controller_Error_interrupt_ref);

	taiHookReleaseForKernel(handle_HPD_interrupt_hook,
		handle_HPD_interrupt_ref);

	taiHookReleaseForKernel(handle_HDCP_authenticated_interrupt_hook,
		handle_HDCP_authenticated_interrupt_ref);

	taiHookReleaseForKernel(handle_EDID_ready_interrupt_hook,
		handle_EDID_ready_interrupt_ref);

	taiHookReleaseForKernel(handle_BKSV_flag_interrupt_hook,
		handle_BKSV_flag_interrupt_ref);

	taiHookReleaseForKernel(handle_rest_interrupts_hook,
		handle_rest_interrupts_ref);

	taiHookReleaseForKernel(sub_BF0B9C_hook,
		sub_BF0B9C_ref);

	unmap_framebuffer();

	return SCE_KERNEL_STOP_SUCCESS;
}

int ioremap(uintptr_t paddr, unsigned int size, SceUID *uid, void **addr)
{
	int ret;
	SceUID mem_uid;
	unsigned int mem_size;
	void *mem_addr;

	mem_size = ALIGN(size, 4096);

	SceKernelAllocMemBlockKernelOpt opt;
	memset(&opt, 0, sizeof(opt));
	opt.size = sizeof(opt);
	opt.attr = 2;
	opt.paddr = paddr;
	mem_uid = ksceKernelAllocMemBlock("ioremap", 0x20100206, mem_size, &opt);
	if (mem_uid < 0)
		return mem_uid;

	ret = ksceKernelGetMemBlockBase(mem_uid, &mem_addr);
	if (ret < 0) {
		ksceKernelFreeMemBlock(mem_uid);
		return ret;
	}

	if (uid)
		*uid = mem_uid;
	if (addr)
		*addr = mem_addr;

	return 0;
}

int alloc_phycont(unsigned int size, SceUID *uid, void **addr)
{
	int ret;
	SceUID mem_uid;
	unsigned int mem_size;
	void *mem_addr;

	mem_size = ALIGN(size, 4096);

	SceKernelAllocMemBlockKernelOpt opt;
	memset(&opt, 0, sizeof(opt));
	opt.size = sizeof(opt);
	opt.attr = 0x200004;
	opt.alignment = 0x1000;
	mem_uid = ksceKernelAllocMemBlock("phycont", 0x30808006, mem_size, &opt);
	if (mem_uid < 0)
		return mem_uid;

	ret = ksceKernelGetMemBlockBase(mem_uid, &mem_addr);
	if (ret < 0) {
		ksceKernelFreeMemBlock(mem_uid);
		return ret;
	}

	if (uid)
		*uid = mem_uid;
	if (addr)
		*addr = mem_addr;

	return 0;
}
