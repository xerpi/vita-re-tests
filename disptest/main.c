#include <stdio.h>
#include <math.h>
#include <taihen.h>
#include <psp2kern/kernel/cpu.h>
#include <psp2kern/kernel/modulemgr.h>
#include <psp2kern/kernel/threadmgr.h>
#include <psp2kern/kernel/sysmem.h>
#include <psp2kern/io/fcntl.h>
#include <psp2kern/io/stat.h>
#include <psp2kern/io/stat.h>
#include <psp2kern/display.h>
#include <psp2kern/lowio/i2c.h>
#include <psp2kern/lowio/gpio.h>
#include <psp2kern/registrymgr.h>
#include <psp2kern/kernel/intrmgr.h>
#include "adv7511.h"
#include "utils.h"
#include "log.h"
#include "draw.h"

#define LOG(s, ...) \
	do { \
		char buffer[256]; \
		snprintf(buffer, sizeof(buffer), s, ##__VA_ARGS__); \
		uart_print(0, buffer); \
		if (0) console_print(buffer); \
	} while (0)

void _start() __attribute__ ((weak, alias ("module_start")));

extern int ksceUartReadAvailable(int device);
extern int ksceUartWrite(int device, unsigned char data);
extern int ksceUartRead(int device);
extern int ksceUartInit(int device);

extern int ScePervasiveForDriver_18DD8043(int device);
extern int ScePervasiveForDriver_788B6C61(int device);
extern int ScePervasiveForDriver_A7CE7DCC(int device);
extern int ScePervasiveForDriver_EFD084D8(int device);

extern int SceSysconForDriver_62155962(int);
extern int SceSysconForDriver_FF86F4C5(void);

static void uart_print(int bus, const char *str)
{
	while (*str) {
		ksceUartWrite(bus, *str);
		if (*str == '\n')
			ksceUartWrite(bus, '\r');
		str++;
	}
}

typedef unsigned int u32;
int ioremap(uintptr_t paddr, unsigned int size, SceUID *uid, void **addr);

#define RGBA8(r, g, b, a)      ((((a)&0xFF)<<24) | (((b)&0xFF)<<16) | (((g)&0xFF)<<8) | (((r)&0xFF)<<0))

#define RED   RGBA8(255, 0,   0,   255)
#define GREEN RGBA8(0,   255, 0,   255)
#define BLUE  RGBA8(0,   0,   255, 255)
#define CYAN  RGBA8(0,   255, 255, 255)
#define LIME  RGBA8(50,  205, 50,  255)
#define PURP  RGBA8(147, 112, 219, 255)
#define WHITE RGBA8(255, 255, 255, 255)
#define BLACK RGBA8(0,   0,   0,   255)

static int run = 1;
static SceUID thid;
static SceUID iftu1regA_uid, iftu1regB_uid, iftu1creg_uid;
static unsigned char *iftu1regA_addr, *iftu1regB_addr, *iftu1creg_addr;
static SceUID dsi1_uid;
static unsigned char *dsi1_addr;

typedef struct SceIftuRegsPlaneConfig {
	unsigned int paddr;
	unsigned int unk04;
	unsigned int unk08;
	unsigned int unk0C;
	unsigned int unk10;
	unsigned int unk14;
	unsigned int unk18;
	unsigned int unk1C;
	unsigned int unk20;
	unsigned int src_x;
	unsigned int src_y;
	unsigned int unk2C;
	unsigned int unk30;
	unsigned int unk34;
	unsigned int unk38;
	unsigned int unk3C;
	unsigned int src_fb_pixelformat;
	unsigned int src_fb_width;
	unsigned int src_fb_height;
	unsigned int control;
	unsigned int unk50;
	unsigned int leftover_stride;
	unsigned int unk58;
	unsigned int unk5C;
	unsigned int vfront_porch;
	unsigned int vback_porch;
	unsigned int hfront_porch;
	unsigned int hback_porch;
	unsigned int unk70;
	unsigned int unk74;
	unsigned int unk78;
	unsigned int unk7C;
	unsigned int paddr2;
	unsigned int unk84;
	unsigned int unk88;
	unsigned int unk8C;
	unsigned int unk90;
	unsigned int unk94;
	unsigned int unk98;
	unsigned int unk9C;
	unsigned int dst_fb_pixelformat;
	unsigned int dst_fb_width;
	unsigned int dst_fb_height;
	unsigned int unkAC;
	unsigned int unkB0;
	unsigned int leftover_stride2;
	unsigned int unkB8;
	unsigned int unkBC;
	unsigned int src_w;
	unsigned int src_h;
	unsigned int dst_x;
	unsigned int dst_y;
	unsigned int dst_w;
	unsigned int dst_h;
	unsigned int unkD8;
	unsigned int unkDC;
	unsigned int unkE0;
	unsigned int unkE4;
	unsigned int unkE8;
	unsigned int unkEC;
	unsigned int unkF0;
	unsigned int unkF4;
	unsigned int unkF8;
	unsigned int unkFC;
} SceIftuRegsPlaneConfig; /* size = 0x100 */

typedef struct SceIftuRegs {
	unsigned int control;
	unsigned int crtc_mask;
	unsigned int unk008;
	unsigned int unk00C;
	unsigned int unk010;
	unsigned int unk014;
	unsigned int unk018;
	unsigned int unk01C;
	unsigned int unk020;
	unsigned int unk024;
	unsigned int unk028;
	unsigned int unk02C;
	unsigned int unk030;
	unsigned int unk034;
	unsigned int unk038;
	unsigned int unk03C;
	unsigned int interrupts;
	unsigned int unk044;
	unsigned int unk048;
	unsigned int unk04C;
	unsigned int interrupt_enable;
	unsigned int unk054;
	unsigned int unk058;
	unsigned int unk05C;
	unsigned int unk060;
	unsigned int unk064;
	unsigned int unk068;
	unsigned int unk06C;
	unsigned int unk070;
	unsigned int unk074;
	unsigned int unk078;
	unsigned int unk07C;
	unsigned int unk080;
	unsigned int unk084;
	unsigned int unk088;
	unsigned int alpha;
	unsigned int unk090;
	unsigned int unk094;
	unsigned int unk098;
	unsigned int unk09C;
	unsigned int alpha_control;
	unsigned int unk0A4;
	unsigned int unk0A8;
	unsigned int unk0AC;
	unsigned int unk0B0;
	unsigned int unk0B4;
	unsigned int unk0B8;
	unsigned int unk0BC;
	unsigned int unk0C0;
	unsigned int unk0C4;
	unsigned int unk0C8;
	unsigned int unk0CC;
	unsigned int unk0D0;
	unsigned int unk0D4;
	unsigned int unk0D8;
	unsigned int unk0DC;
	unsigned int unk0E0;
	unsigned int unk0E4;
	unsigned int unk0E8;
	unsigned int unk0EC;
	unsigned int unk0F0;
	unsigned int unk0F4;
	unsigned int unk0F8;
	unsigned int unk0FC;
	unsigned int csc_control;
	unsigned int csc_unk104;
	unsigned int csc_unk108;
	unsigned int csc_rr2;
	unsigned int csc_rg2;
	unsigned int csc_rb2;
	unsigned int csc_gr2;
	unsigned int csc_gg2;
	unsigned int csc_gb2;
	unsigned int csc_br2;
	unsigned int csc_bg2;
	unsigned int csc_bb2;
	unsigned int csc_unk130;
	unsigned int csc_unk134;
	unsigned int csc_rr;
	unsigned int csc_rg;
	unsigned int csc_rb;
	unsigned int csc_gr;
	unsigned int csc_gg;
	unsigned int csc_gb;
	unsigned int csc_br;
	unsigned int csc_bg;
	unsigned int csc_bb;
	unsigned int csc_unk15C;
	unsigned int csc_unk160;
	unsigned int csc_unk164;
	unsigned int csc_unk168;
	unsigned int unk16C;
	unsigned int unk170;
	unsigned int unk174;
	unsigned int unk178;
	unsigned int unk17C;
	unsigned int unk180;
	unsigned int unk184;
	unsigned int unk188;
	unsigned int unk18C;
	unsigned int unk190;
	unsigned int unk194;
	unsigned int unk198;
	unsigned int unk19C;
	unsigned int unk1A0;
	unsigned int unk1A4;
	unsigned int unk1A8;
	unsigned int unk1AC;
	unsigned int unk1B0;
	unsigned int unk1B4;
	unsigned int unk1B8;
	unsigned int unk1BC;
	unsigned int unk1C0;
	unsigned int unk1C4;
	unsigned int unk1C8;
	unsigned int unk1CC;
	unsigned int unk1D0;
	unsigned int unk1D4;
	unsigned int unk1D8;
	unsigned int unk1DC;
	unsigned int unk1E0;
	unsigned int unk1E4;
	unsigned int unk1E8;
	unsigned int unk1EC;
	unsigned int unk1F0;
	unsigned int unk1F4;
	unsigned int unk1F8;
	unsigned int unk1FC;
	SceIftuRegsPlaneConfig plane_config[2];
} SceIftuRegs;

typedef struct SceIftucSubRegs {
	unsigned int cfg_select;
	unsigned int unk04;
} SceIftucSubRegs;

typedef struct SceIftucRegs {
	unsigned int enable; // bit 0 = enable engine
	unsigned int control; // Plane control register. bit 0 = enable alpha blending, bit 2 = enable first plane, bit 4 = enable second plane.
	unsigned int unk08;
	unsigned int unk0C;
	SceIftucSubRegs subctrl[2];
	unsigned int alpha_control;
	unsigned int unk24;
	unsigned int unk28;
	unsigned int unk2C;
	unsigned int unk30;
	unsigned int unk34;
	unsigned int unk38;
	unsigned int unk3C;
} SceIftucRegs;

#define LOG_FIELD(x) \
	LOG(#x ": 0x%08X\n", (unsigned int)reg->x)

void log_plane_cfg(volatile SceIftuRegsPlaneConfig *reg)
{
	LOG_FIELD(paddr);
	LOG_FIELD(control);
}

void log_regs(volatile SceIftuRegs *reg, int i)
{
	reg->alpha = 0;
	reg->alpha_control = 1;
	LOG_FIELD(control);
	LOG_FIELD(crtc_mask);
	LOG_FIELD(alpha);
	LOG_FIELD(alpha_control);
	LOG_FIELD(unk180);

	LOG_FIELD(unk008);
	LOG_FIELD(unk010);
	LOG_FIELD(unk030);
	LOG_FIELD(unk054);
	LOG_FIELD(unk058);
	LOG_FIELD(unk070);
	LOG_FIELD(unk074);

	//log_plane_cfg(&reg->plane_config[0]);
	//log_plane_cfg(&reg->plane_config[1]);

	LOG("\n");
}

void log_cregs(volatile SceIftucRegs *reg)
{
	reg->control = 1;
	reg->alpha_control = 0; // No blending
	LOG_FIELD(enable);
	LOG_FIELD(control);
	LOG_FIELD(unk08);
	LOG_FIELD(unk0C);
	LOG_FIELD(subctrl[0].cfg_select);
	LOG_FIELD(subctrl[0].unk04);
	LOG_FIELD(subctrl[1].cfg_select);
	LOG_FIELD(subctrl[1].unk04);
	LOG_FIELD(alpha_control);
	LOG("\n");
}

static int thread(SceSize args, void *argp)
{
#define LOG_VAL(base, offset) \
	LOG(#base " +0x%04X: 0x%08X\n", offset, GET_VAL(base, offset))

#define SET_VAL(base, offset, val) \
	do { \
		*(u32 *)((char *)(base) + (offset)) = val; \
	} while (0)

#define GET_VAL(base, offset) (*(u32 *)((char *)(base) + (offset)))

	SceIftucRegs *iftu1creg = (SceIftucRegs *)iftu1creg_addr;
	SceIftuRegs *iftu1regA = (SceIftuRegs *)iftu1regA_addr;
	SceIftuRegs *iftu1regB = (SceIftuRegs *)iftu1regB_addr;

	//int y = console_get_y();
	uart_print(0, "\e[1;1H\e[2J");
	while (run) {
		uart_print(0, "\033[0;0H");

		//log_cregs(iftu1creg);
		log_regs(iftu1regA, 0);
		log_regs(iftu1regB, 1);

		//LOG("\n");
		//ksceKernelDelayThread(10 * 1000);
	}

	return 0;
}

int module_start(SceSize argc, const void *args)
{
	ScePervasiveForDriver_EFD084D8(0); // Turn on clock
	ScePervasiveForDriver_A7CE7DCC(0); // Out of reset
	ksceUartInit(0);
	map_framebuffer();
	LOG("\n\ndisptest by xerpi\n");

	ioremap(0xE5060000, 0x1000, &dsi1_uid, (void **)&dsi1_addr);

	ioremap(0xE5030000, 0x1000, &iftu1regA_uid, (void **)&iftu1regA_addr);
	ioremap(0xE5031000, 0x1000, &iftu1regB_uid, (void **)&iftu1regB_addr);
	ioremap(0xE5032000, 0x1000, &iftu1creg_uid, (void **)&iftu1creg_addr);

	LOG("Iftu1RegA addr: %p\n", iftu1regA_addr);
	LOG("Iftu1RegB addr: %p\n", iftu1regB_addr);
	LOG("Iftu1cReg addr: %p\n", iftu1creg_addr);

	int ratio;
	ksceRegMgrGetKeyInt("/CONFIG/DISPLAY", "hdmi_out_scaling_ratio", &ratio);
	LOG("raio: 0x%08X\n", ratio);

	LOG("\n");

	thid = ksceKernelCreateThread("disptest", thread, 0x00, 0x1000, 0, 1 << 0, 0);

	ksceKernelStartThread(thid, 0, NULL);

	return SCE_KERNEL_START_SUCCESS;
}

int module_stop(SceSize argc, const void *args)
{
	run = 0;
	ksceKernelWaitThreadEnd(thid, NULL, NULL);

	ksceKernelFreeMemBlock(dsi1_uid);

	ksceKernelFreeMemBlock(iftu1regA_uid);
	ksceKernelFreeMemBlock(iftu1regB_uid);
	ksceKernelFreeMemBlock(iftu1creg_uid);

	unmap_framebuffer();

	return SCE_KERNEL_STOP_SUCCESS;
}

int ioremap(uintptr_t paddr, unsigned int size, SceUID *uid, void **addr)
{
	int ret;
	SceUID mem_uid;
	unsigned int mem_size;
	void *mem_addr;

	mem_size = ALIGN(size, 4096);

	SceKernelAllocMemBlockKernelOpt opt;
	memset(&opt, 0, sizeof(opt));
	opt.size = sizeof(opt);
	opt.attr = 2;
	opt.paddr = paddr;
	mem_uid = ksceKernelAllocMemBlock("ioremap", 0x20100206, mem_size, &opt);
	if (mem_uid < 0)
		return mem_uid;

	ret = ksceKernelGetMemBlockBase(mem_uid, &mem_addr);
	if (ret < 0) {
		ksceKernelFreeMemBlock(mem_uid);
		return ret;
	}

	if (uid)
		*uid = mem_uid;
	if (addr)
		*addr = mem_addr;

	return 0;
}

int alloc_phycont(unsigned int size, SceUID *uid, void **addr)
{
	int ret;
	SceUID mem_uid;
	unsigned int mem_size;
	void *mem_addr;

	mem_size = ALIGN(size, 4096);

	SceKernelAllocMemBlockKernelOpt opt;
	memset(&opt, 0, sizeof(opt));
	opt.size = sizeof(opt);
	opt.attr = 0x200004;
	opt.alignment = 0x1000;
	mem_uid = ksceKernelAllocMemBlock("phycont", 0x30808006, mem_size, &opt);
	if (mem_uid < 0)
		return mem_uid;

	ret = ksceKernelGetMemBlockBase(mem_uid, &mem_addr);
	if (ret < 0) {
		ksceKernelFreeMemBlock(mem_uid);
		return ret;
	}

	if (uid)
		*uid = mem_uid;
	if (addr)
		*addr = mem_addr;

	return 0;
}
