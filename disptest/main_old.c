#include <stdio.h>
#include <math.h>
#include <psp2kern/kernel/cpu.h>
#include <psp2kern/kernel/modulemgr.h>
#include <psp2kern/kernel/threadmgr.h>
#include <psp2kern/kernel/sysmem.h>
#include <psp2kern/io/fcntl.h>
#include <psp2kern/io/stat.h>
#include <psp2kern/io/stat.h>
#include <psp2kern/display.h>
#include <psp2kern/lowio/i2c.h>
#include <psp2kern/lowio/gpio.h>
#include <psp2kern/registrymgr.h>
#include <psp2kern/kernel/intrmgr.h>
#include "utils.h"
#include "log.h"
#include "draw.h"

#define LOG(s, ...) \
	do { \
		char buffer[256]; \
		snprintf(buffer, sizeof(buffer), s, ##__VA_ARGS__); \
		uart_print(0, buffer); \
		if (0) console_print(buffer); \
	} while (0)

void _start() __attribute__ ((weak, alias ("module_start")));

extern int ksceUartReadAvailable(int device);
extern int ksceUartWrite(int device, unsigned char data);
extern int ksceUartRead(int device);
extern int ksceUartInit(int device);

extern int ScePervasiveForDriver_18DD8043(int device);
extern int ScePervasiveForDriver_788B6C61(int device);
extern int ScePervasiveForDriver_A7CE7DCC(int device);
extern int ScePervasiveForDriver_EFD084D8(int device);

extern int SceSysconForDriver_62155962(int);
extern int SceSysconForDriver_FF86F4C5(void);

static void uart_print(int bus, const char *str)
{
	while (*str) {
		ksceUartWrite(bus, *str);
		if (*str == '\n')
			ksceUartWrite(bus, '\r');
		str++;
	}
}

typedef unsigned int u32;
int ioremap(uintptr_t paddr, unsigned int size, SceUID *uid, void **addr);

#define RGBA8(r, g, b, a)      ((((a)&0xFF)<<24) | (((b)&0xFF)<<16) | (((g)&0xFF)<<8) | (((r)&0xFF)<<0))

#define RED   RGBA8(255, 0,   0,   255)
#define GREEN RGBA8(0,   255, 0,   255)
#define BLUE  RGBA8(0,   0,   255, 255)
#define CYAN  RGBA8(0,   255, 255, 255)
#define LIME  RGBA8(50,  205, 50,  255)
#define PURP  RGBA8(147, 112, 219, 255)
#define WHITE RGBA8(255, 255, 255, 255)
#define BLACK RGBA8(0,   0,   0,   255)

static int run = 1;
static SceUID thid;
static SceUID iftu1regA_uid, iftu1regB_uid, iftu1creg_uid;
static unsigned char *iftu1regA_addr, *iftu1regB_addr, *iftu1creg_addr;
static SceUID dsi1_uid;
static unsigned char *dsi1_addr;

static SceUID fb_uid[4];
static unsigned char *fb_addr[4];
static unsigned int fb_paddr[4];

#define SCREEN_PITCH 1280
#define SCREEN_HEIGHT 720
#define FB_SIZE (SCREEN_PITCH * SCREEN_HEIGHT * 4)

typedef struct SceIftuFbInfo {
	unsigned int pixelformat;
	unsigned int width;
	unsigned int height;
	unsigned int leftover_stride;
	unsigned int unk10; // always 0
	unsigned int paddr;
	unsigned int unk18; // always 0
	unsigned int unk1C; // always 0
	unsigned int unk20; // always 0
	unsigned int src_x; // in (0x10000 / 960) multiples
	unsigned int src_y; // in (0x10000 / 544) multiples
	unsigned int src_w; // in (0x10000 / 960) multiples
	unsigned int src_h; // in (0x10000 / 544) multiples
	unsigned int dst_x;
	unsigned int dst_y;
	unsigned int dst_w;
	unsigned int dst_h;
	unsigned int vfront_porch;
	unsigned int vback_porch;
	unsigned int hfront_porch;
	unsigned int hback_porch;
} SceIftuFbInfo; /* size = 0x54 */

typedef struct SceIftuRegsPlaneConfig {
	unsigned int paddr;
	unsigned int unk04;
	unsigned int unk08;
	unsigned int unk0C;
	unsigned int unk10;
	unsigned int unk14;
	unsigned int unk18;
	unsigned int unk1C;
	unsigned int unk20;
	unsigned int src_x;
	unsigned int src_y;
	unsigned int unk2C;
	unsigned int unk30;
	unsigned int unk34;
	unsigned int unk38;
	unsigned int unk3C;
	unsigned int src_fb_pixelformat;
	unsigned int src_fb_width;
	unsigned int src_fb_height;
	unsigned int control;
	unsigned int unk50;
	unsigned int leftover_stride;
	unsigned int unk58;
	unsigned int unk5C;
	unsigned int vfront_porch;
	unsigned int vback_porch;
	unsigned int hfront_porch;
	unsigned int hback_porch;
	unsigned int unk70;
	unsigned int unk74;
	unsigned int unk78;
	unsigned int unk7C;
	unsigned int paddr2;
	unsigned int unk84;
	unsigned int unk88;
	unsigned int unk8C;
	unsigned int unk90;
	unsigned int unk94;
	unsigned int unk98;
	unsigned int unk9C;
	unsigned int dst_fb_pixelformat;
	unsigned int dst_fb_width;
	unsigned int dst_fb_height;
	unsigned int unkAC;
	unsigned int unkB0;
	unsigned int leftover_stride2;
	unsigned int unkB8;
	unsigned int unkBC;
	unsigned int src_w;
	unsigned int src_h;
	unsigned int dst_x;
	unsigned int dst_y;
	unsigned int dst_w;
	unsigned int dst_h;
	unsigned int unkD8;
	unsigned int unkDC;
	unsigned int unkE0;
	unsigned int unkE4;
	unsigned int unkE8;
	unsigned int unkEC;
	unsigned int unkF0;
	unsigned int unkF4;
	unsigned int unkF8;
	unsigned int unkFC;
} SceIftuRegsPlaneConfig; /* size = 0x100 */

typedef struct SceIftuRegs {
	unsigned int control;
	unsigned int crtc_mask;
	unsigned int unk008;
	unsigned int unk00C;
	unsigned int unk010;
	unsigned int unk014;
	unsigned int unk018;
	unsigned int unk01C;
	unsigned int unk020;
	unsigned int unk024;
	unsigned int unk028;
	unsigned int unk02C;
	unsigned int unk030;
	unsigned int unk034;
	unsigned int unk038;
	unsigned int unk03C;
	unsigned int interrupts;
	unsigned int unk044;
	unsigned int unk048;
	unsigned int unk04C;
	unsigned int interrupt_enable;
	unsigned int unk054;
	unsigned int unk058;
	unsigned int unk05C;
	unsigned int unk060;
	unsigned int unk064;
	unsigned int unk068;
	unsigned int unk06C;
	unsigned int unk070;
	unsigned int unk074;
	unsigned int unk078;
	unsigned int unk07C;
	unsigned int unk080;
	unsigned int unk084;
	unsigned int unk088;
	unsigned int alpha;
	unsigned int unk090;
	unsigned int unk094;
	unsigned int unk098;
	unsigned int unk09C;
	unsigned int alpha_control;
	unsigned int unk0A4;
	unsigned int unk0A8;
	unsigned int unk0AC;
	unsigned int unk0B0;
	unsigned int unk0B4;
	unsigned int unk0B8;
	unsigned int unk0BC;
	unsigned int unk0C0;
	unsigned int unk0C4;
	unsigned int unk0C8;
	unsigned int unk0CC;
	unsigned int unk0D0;
	unsigned int unk0D4;
	unsigned int unk0D8;
	unsigned int unk0DC;
	unsigned int unk0E0;
	unsigned int unk0E4;
	unsigned int unk0E8;
	unsigned int unk0EC;
	unsigned int unk0F0;
	unsigned int unk0F4;
	unsigned int unk0F8;
	unsigned int unk0FC;
	unsigned int csc_control;
	unsigned int csc_unk104;
	unsigned int csc_unk108;
	unsigned int csc_rr2;
	unsigned int csc_rg2;
	unsigned int csc_rb2;
	unsigned int csc_gr2;
	unsigned int csc_gg2;
	unsigned int csc_gb2;
	unsigned int csc_br2;
	unsigned int csc_bg2;
	unsigned int csc_bb2;
	unsigned int csc_unk130;
	unsigned int csc_unk134;
	unsigned int csc_rr;
	unsigned int csc_rg;
	unsigned int csc_rb;
	unsigned int csc_gr;
	unsigned int csc_gg;
	unsigned int csc_gb;
	unsigned int csc_br;
	unsigned int csc_bg;
	unsigned int csc_bb;
	unsigned int csc_unk15C;
	unsigned int csc_unk160;
	unsigned int csc_unk164;
	unsigned int csc_unk168;
	unsigned int unk16C;
	unsigned int unk170;
	unsigned int unk174;
	unsigned int unk178;
	unsigned int unk17C;
	unsigned int unk180;
	unsigned int unk184;
	unsigned int unk188;
	unsigned int unk18C;
	unsigned int unk190;
	unsigned int unk194;
	unsigned int unk198;
	unsigned int unk19C;
	unsigned int unk1A0;
	unsigned int unk1A4;
	unsigned int unk1A8;
	unsigned int unk1AC;
	unsigned int unk1B0;
	unsigned int unk1B4;
	unsigned int unk1B8;
	unsigned int unk1BC;
	unsigned int unk1C0;
	unsigned int unk1C4;
	unsigned int unk1C8;
	unsigned int unk1CC;
	unsigned int unk1D0;
	unsigned int unk1D4;
	unsigned int unk1D8;
	unsigned int unk1DC;
	unsigned int unk1E0;
	unsigned int unk1E4;
	unsigned int unk1E8;
	unsigned int unk1EC;
	unsigned int unk1F0;
	unsigned int unk1F4;
	unsigned int unk1F8;
	unsigned int unk1FC;
	SceIftuRegsPlaneConfig plane_config[2];
} SceIftuRegs;

typedef struct SceIftucSubRegs {
	unsigned int cfg_select;
	unsigned int unk04;
} SceIftucSubRegs;

typedef struct SceIftucRegs {
	unsigned int enable; // bit 0 = enable engine
	unsigned int control; // Plane control register. bit 0 = enable alpha blending, bit 2 = enable first plane, bit 4 = enable second plane.
	unsigned int unk08;
	unsigned int unk0C;
	SceIftucSubRegs subctrl[2];
	unsigned int alpha_control;
	unsigned int unk24;
	unsigned int unk28;
	unsigned int unk2C;
	unsigned int unk30;
	unsigned int unk34;
	unsigned int unk38;
	unsigned int unk3C;
} SceIftucRegs;

void regs_plane_config_print(const SceIftuRegsPlaneConfig *cfg)
{
#define LOG_FIELD(x) \
	LOG(#x ": 0x%08X\n", (unsigned int)cfg->x)

	LOG_FIELD(paddr);
	//LOG_FIELD(paddr2);
	LOG_FIELD(control);
	LOG_FIELD(src_x);
	LOG_FIELD(src_y);
	LOG_FIELD(src_w);
	LOG_FIELD(src_h);
	LOG_FIELD(src_fb_pixelformat);
	LOG_FIELD(src_fb_width);
	LOG_FIELD(src_fb_height);
	LOG_FIELD(dst_fb_pixelformat);
	LOG_FIELD(dst_x);
	LOG_FIELD(dst_y);
	LOG("\n");

#undef LOG_FIELD
}

void fill_fb(void *addr, unsigned int color)
{
	int i, j;

	for (i = 0; i < SCREEN_HEIGHT; i++) {
		for (j = 0; j < SCREEN_PITCH; j++) {
			if (j % 64 < 32 || i % 64 < 32)
				*(u32 *)((char *)addr + i * SCREEN_PITCH * 4 + j * 4) = color;
			else
				*(u32 *)((char *)addr + i * SCREEN_PITCH * 4 + j * 4) = ~color | 0xFF000000;
		}
	}
}

#define ARRAY_SIZE(x) (sizeof(x) / sizeof((x)[0]))

#define dmb() asm volatile("dmb\n\t")
#define dsb() asm volatile("dsb\n\t")


#define DSI_REGS(i)			((void *)dsi1_addr)

struct dsi_timing_subsubinfo {
	unsigned int unk00;
	unsigned int unk04;
	unsigned int unk08;
	unsigned int unk0C;
	unsigned int unk10;
};

struct dsi_timing_subinfo {
	unsigned int unk00;
	unsigned int unk04;
	unsigned int unk08;
	unsigned int unk0C;
	struct dsi_timing_subsubinfo subsubinfo;
};

struct dsi_timing_info {
	unsigned int flags; // bit 3 = enable CRC?
	unsigned int pixelclock_24bpp;
	const struct dsi_timing_subinfo *subinfo_24bpp;
	unsigned int pixelclock_30bpp;
	const struct dsi_timing_subinfo *subinfo_30bpp;
	unsigned int htotal; // horizontal line time
	unsigned int vtotal; // vertical line time
	unsigned int mode; // 1 = interlaced, 0 = progressive
	unsigned int HFP;
	unsigned int HSW;
	unsigned int HBP;
	unsigned int VFP;
	unsigned int VSW;
	unsigned int VBP;
};

static struct dsi_timing_subinfo stru_A19358;

/* subinfo_24bpp */
static const struct dsi_timing_subinfo stru_BD0F08 = {0x2BCF, 6, 0xF05,  1, {0x806, 0x4906, 0x16, 3, 0x20}};
static const struct dsi_timing_subinfo stru_BD0FA0 = {0x2BCF, 6, 0x1007, 1, {0x808, 0x4906, 0x16, 3, 0x20}};
static const struct dsi_timing_subinfo stru_BD0C5C = {0x33E0, 7, 0x150A, 2, {0x40B, 0x40CA, 8,    4, 0x20}};
static const struct dsi_timing_subinfo stru_BD0E04 = {0x1519, 3, 0x703,  3, {3,     0x34BD, 7,    0, 0x20}};
static const struct dsi_timing_subinfo stru_BD0C00 = {0x39F5, 7, 0x1507, 5, {0x507, 0x4871, 0xB,  0, 0x20}};
static const struct dsi_timing_subinfo stru_BD0B34 = {0x3A03, 7, 0x1507, 5, {0x507, 0x4884, 0xB,  0, 0x20}};

/* subinfo_30bpp */
static const struct dsi_timing_subinfo stru_BD0AC4 = {0x1A60, 3, 0xB03,  3, {3,     0x41EC, 7,    0, 0x20}};
static const struct dsi_timing_subinfo stru_BD0CF0 = {0x4876, 9, 0x1F09, 5, {0x709, 0x4871, 0xB,  1, 0x20}};
static const struct dsi_timing_subinfo stru_BD0D4C = {0x488A, 9, 0x1F09, 5, {0x709, 0x4890, 0xB,  1, 0x20}};

/*                                              flags pixelclk24 subinfo_24bpp pixelclk30 subinfo_30bpp hline  vline mode HFP   HSW   HBP   VFP  VSW VBP */
static const struct dsi_timing_info stru_BD0D14 = {5,   0x223A1C, &stru_A19358, 0,        0,            0x41A, 0x252, 0, 0x14,  0x42,  4,    4,   4, 0x2A};
static const struct dsi_timing_info stru_BD0BC8 = {5,   0x223A1C, &stru_A19358, 0,        0,            0x4E2, 0x252, 0, 0x14,  0x10A, 4,    4,   4, 0x2E};
static const struct dsi_timing_info stru_BD0ED0 = {5,   0x223A1C, &stru_A19358, 0,        0,            0x55F, 0x237, 0, 0x14,  0x187, 4,    4,   4, 0xF};
static const struct dsi_timing_info stru_BD0E60 = {0xF, 0x287CF3, &stru_BD0C5C, 0,        0,            0x594, 0x307, 0, 0x72,  8,     0x18, 4,   4, 0x2F};
static const struct dsi_timing_info stru_BD0E98 = {1,   0x107AC0, &stru_BD0E04, 0x149970, &stru_BD0AC4, 0x35A, 0x20D, 0, 0x10,  0x3E,  0x3C, 9,   6, 0x1E}; // VIC 2/3
static const struct dsi_timing_info stru_BD0E28 = {1,   0x107AC0, &stru_BD0E04, 0x149970, &stru_BD0AC4, 0x35A, 0x20E, 0, 0x10,  0x3E,  0x3C, 0xA, 6, 0x1E};
static const struct dsi_timing_info stru_BD0DCC = {1,   0x107AC0, &stru_BD0E04, 0x149970, &stru_BD0AC4, 0x360, 0x271, 0, 0xC,   0x40,  0x44, 5,   5, 0x27};
static const struct dsi_timing_info stru_BD0B90 = {1,   0x107AC0, &stru_BD0E04, 0x149970, &stru_BD0AC4, 0x360, 0x272, 0, 0xC,   0x40,  0x44, 6,   5, 0x28};
static const struct dsi_timing_info stru_BD0CB8 = {0,   0x2D45F9, &stru_BD0C00, 0x389777, &stru_BD0CF0, 0x898, 0x465, 1, 0x58,  0x2C,  0x94, 2,   5, 0x10};
static const struct dsi_timing_info stru_BD0B58 = {0,   0x2D5190, &stru_BD0B34, 0x38A5F4, &stru_BD0D4C, 0xA50, 0x465, 1, 0x210, 0x2C,  0x94, 2,   5, 0x10};
static const struct dsi_timing_info stru_BD0F68 = {0,   0x2D45F9, &stru_BD0C00, 0x389777, &stru_BD0CF0, 0x672, 0x2EE, 0, 0x6E,  0x28,  0xDC, 5,   5, 0x14};
static const struct dsi_timing_info stru_BD0D70 = {0,   0x2D5190, &stru_BD0B34, 0x38A5F4, &stru_BD0D4C, 0x7BC, 0x2EE, 0, 0x1B8, 0x28,  0xDC, 5,   5, 0x14};
static const struct dsi_timing_info stru_BD0C80 = {0,   0x2D45F9, &stru_BD0C00, 0x389777, &stru_BD0CF0, 0x898, 0x465, 0, 0x58,  0x2C,  0x94, 4,   5, 0x24};
static const struct dsi_timing_info stru_BD0A8C = {0,   0x2D5190, &stru_BD0B34, 0x38A5F4, &stru_BD0D4C, 0xA50, 0x465, 0, 0x210, 0x2C,  0x94, 4,   5, 0x24};
static const struct dsi_timing_info stru_BD0AE8 = {0,   0x2D45F9, &stru_BD0C00, 0x389777, &stru_BD0CF0, 0xABE, 0x465, 0, 0x27E, 0x2C,  0x94, 4,   5, 0x24};

static struct {
	unsigned int vic;
	const struct dsi_timing_info *timing_info;
} dsi_timing_info_lookup[] = {
	{0, &stru_BD0D14},
	{0x80, &stru_BD0BC8},
	{0x20, &stru_BD0ED0},
	{0x8900, &stru_BD0E60},
	{0x8300, &stru_BD0E98},
	{0x8370, &stru_BD0E28},
	{0x8480, &stru_BD0DCC},
	{0x84F0, &stru_BD0B90},
	{0x8500, &stru_BD0CB8},
	{0x8580, &stru_BD0B58},
	{0x8600, &stru_BD0F68},
	{0x8680, &stru_BD0D70},
	{0x8710, &stru_BD0C80},
	{0x8790, &stru_BD0A8C},
	{0x8730, &stru_BD0AE8},
};

static const struct dsi_timing_info *
dsi_get_timing_info_for_vic(unsigned int vic)
{
	int i;

	for (i = 0; i < ARRAY_SIZE(dsi_timing_info_lookup); i++) {
		if (dsi_timing_info_lookup[i].vic == vic)
			return dsi_timing_info_lookup[i].timing_info;
	}

	return NULL;
}

void dsi_init(void)
{
	//if ((pervasive_read_misc(0x0000) & 0x1FF00) > 0xFF)
	//	memcpy(&stru_A19358, &stru_BD0FA0, sizeof(stru_A19358));
	//else
		memcpy(&stru_A19358, &stru_BD0F08, sizeof(stru_A19358));
}

int dsi_get_dimensions_for_vic(unsigned int vic, unsigned int *width, unsigned int *height)
{
	const struct dsi_timing_info *info = dsi_get_timing_info_for_vic(vic);
	if (!info)
		return -1;

	if (width) {
		*width = info->htotal - (info->HBP + info->HFP) - info->HSW;
		if (info->flags & (1 << 3))
			*width -= 2;
	}

	if (height) {
		if (info->mode == 1)
			*height = info->vtotal + 1 - 2 * (info->VFP + info->VSW + info->VBP);
		else
			*height = info->vtotal - (info->VBP + info->VSW) - info->VFP;
	}

	return 0;
}

void dsi_enable_bus(int bus, unsigned int vic)
{
	static const int pixel_size = 24;
	static const int lanes = 3;
	static const int unk07 = 2;

	unsigned int packet[64];
	unsigned int packet_size;
	const struct dsi_timing_subinfo *subinfo;
	const struct dsi_timing_info *timing_info = dsi_get_timing_info_for_vic(vic);
	volatile unsigned int *dsi_regs = DSI_REGS(bus);

	//pervasive_dsi_misc_unk(bus);

	dsi_regs[0x15] = 0;
	dsi_regs[0x146] = 1;

	/*if ((pervasive_read_misc(0x0000) & 0x1FF00) > 0xFF) {
		dsi_regs[0x240] = (dsi_regs[0x240] & 0xFFFFFFFC) | 2;
		dsi_regs[0x241] = (dsi_regs[0x241] & 0xFFFFFFFC) | 2;
		dsi_regs[0x242] = (dsi_regs[0x242] & 0xFFFFFFFC) | 2;
		if (lanes == 3)
			dsi_regs[0x243] = (dsi_regs[0x243] & 0xFFFFFFFC) | 2;
	}*/

	dsi_regs[0x250] = 0x200;
	dsi_regs[0x251] = 0x200;
	dsi_regs[0x252] = 0x200;

	if (lanes == 3)
		dsi_regs[0x253] = 0x200;

	if (pixel_size == 24)
		subinfo = timing_info->subinfo_24bpp;
	else
		subinfo = timing_info->subinfo_30bpp;

	if (subinfo) {
		dsi_regs[0x204] = subinfo->unk00;
		dsi_regs[0x205] = subinfo->unk04;
		dsi_regs[0x206] = subinfo->unk08;
		dsi_regs[0x207] = subinfo->unk0C;
		dsi_regs[0x208] = subinfo->subsubinfo.unk00;
		dsi_regs[0x209] = subinfo->subsubinfo.unk04;
		dsi_regs[0x20A] = subinfo->subsubinfo.unk08;
		dsi_regs[0x20B] = subinfo->subsubinfo.unk0C;
		dsi_regs[0x20C] = subinfo->subsubinfo.unk10;
		dsi_regs[0x20F] = subinfo->unk04 | (subinfo->unk04 << 16);
	}

	unsigned int flags = timing_info->flags;
	unsigned int HFP = timing_info->HFP;
	unsigned int HBP = timing_info->HBP;
	unsigned int HSW = timing_info->HSW;
	unsigned int VFP = timing_info->VFP;
	unsigned int VSW = timing_info->VSW;
	unsigned int VBP = timing_info->VBP;

	if (bus == 1) {
		dsi_regs[0x20D] = 0xF;
		dsi_regs[0x20E] = 0;
		dsi_regs[0x201] = 1;
		//dsi_regs[0x145] = 0xFFFFFFFF;

		if (unk07 == 1) {
			unsigned int v81 = 0xA30000A2;
			unsigned int v82 = timing_info->htotal - (timing_info->HBP + timing_info->HSW) - timing_info->HFP;

			if (((timing_info->flags << 0x1C) & 0x80000000) != 0)
				v82 -= 2;

			unsigned int v86 = timing_info->vtotal - 2 - VSW - VBP - VFP;
			unsigned int v89 = (768 * v82) & 0xFFFFFF;

			if (lanes == 3)
				v81 = 0xA30000A4;

			packet[1] = 0x40EFFFF;
			packet[2] = v81;
			packet[3] = 0x28000004;
			packet[4] = 0x10000001;
			packet[5] = 0x28000001;
			packet[6] = 0x10000021;
			packet[7] = (VBP + VSW - 3) | 0x4010000;
			packet[8] = 0x28000001;
			packet[9] = 0x10000021;
			packet[0xA] = 0x28000001;
			packet[0xB] = 0x10000021;
			packet[0xC] = 0x40005619;
			packet[0xD] = v89 | 0x4000003E;
			packet[0xE] = 0x40015019;
			packet[0xF] = (v86 | 0x4000000) | 0x30000;
			packet[0x10] = 0x10000021;
			packet[0x11] = 0x40005619;
			packet[0x12] = v89 | 0x4000003E;
			packet[0x13] = 0x40015019;
			packet[0x14] = 0x10000021;
			packet[0x15] = (VFP - 2) | 0x4010000;
			packet[0x16] = 0x28000001;
			packet[0x17] = 0x10000021;
			packet[0x18] = 0xC000000;

			packet_size = 0x18;
		} else {
			if (timing_info->mode) {
				unsigned int v61 = (unsigned int)(timing_info->vtotal + 1 - 2 * (VSW + timing_info->VFP + VBP)) >> 1;
				unsigned int v62 = timing_info->htotal - (HSW + HBP) - timing_info->HFP;
				unsigned int v63 = timing_info->flags & 2;
				unsigned int v64 = timing_info->flags & 4;
				unsigned int v65;
				unsigned int v70;
				unsigned int v98 = (v61 - 1) | 0x4050000;

				if (v63)
					v63 = pixel_size * HSW;
				if (timing_info->flags & 2)
					v63 = ((v63 >> 3) - 10) | 0x80000000;

				if (v64)
					v64 = pixel_size * HBP;

				if (timing_info->flags & 4)
					v64 = ((v64 >> 3) - 10) | 0x80000000;

				if (lanes == 3)
					v65 = 0xA30000A4;
				else
					v65 = 0xA30000A2;
				packet[1] = 0x415FFFF;
				packet[2] = v65;
				packet[3] = 0x28800005;
				packet[4] = 0x10000081;
				if (v63) {
					packet[5] = ((v63 << 8) & 0xFFFFFF) | 0x40000099;
					packet[6] = 0x100000B1;
					packet[7] = (VSW - 2) | 0x4030000;
					packet[8] = 0x28000001;
					packet[9] = 0x100000A1;
					packet[0xA] = ((v63 << 8) & 0xFFFFFF) | 0x40000099;
					packet[0xB] = 0x100000B1;
					packet[0xC] = 0x28000008;
					packet[0xD] = 0x10000091;
					packet[0xE] = ((v63 << 8) & 0xFFFFFF) | 0x40000099;
					packet[0xF] = 0x100000B1;
					packet[0x10] = (VBP - 3) | 0x4030000;
					packet[0x11] = 0x28000001;
					packet[0x12] = 0x100000A1;
					packet[0x13] = ((v63 << 8) & 0xFFFFFF) | 0x40000099;
					packet[0x14] = 0x100000B1;
					packet[0x15] = (v61 - 1) | 0x4050000;
					packet[0x16] = 0x28000001;
					packet[0x17] = 0x100000A1;
					packet[0x18] = ((v63 << 8) & 0xFFFFFF) | 0x40000099;
				} else {
					packet[5] = 0x28000002;
					packet[6] = 0x100000B1;
					packet[7] = (VSW - 2) | 0x4030000;
					packet[8] = 0x28000001;
					packet[9] = 0x100000A1;
					packet[0xA] = 0x28000002;
					packet[0xB] = 0x100000B1;
					packet[0xC] = 0x28000008;
					packet[0xD] = 0x10000091;
					packet[0xE] = 0x28000002;
					packet[0xF] = 0x100000B1;
					packet[0x10] = (VBP - 3) | 0x4030000;
					packet[0x11] = 0x28000001;
					packet[0x12] = 0x100000A1;
					packet[0x13] = 0x28000002;
					packet[0x14] = 0x100000B1;
					packet[0x15] = (v61 - 1) | 0x4050000;
					packet[0x16] = 0x28000001;
					packet[0x17] = 0x100000A1;
					packet[0x18] = 0x28000002;
				}

				packet[0x19] = 0x100000B1;

				unsigned int v66 = 0x28000010;
				if (v64)
					v66 = ((v64 << 8) & 0xFFFFFF) | 0x40000099;
				packet[0x1A] = v66;

				unsigned int v67;
				if (pixel_size == 24)
					v67 = ((768 * v62) & 0xFFFFFF) | 0x400000BE;
				else
					v67 = ((960 * v62) & 0xFFFF00) | 0x400000A9;

				packet[0x1B] = v67;
				packet[0x1C] = VFP | 0x4030000;
				packet[0x1D] = 0x28000001;
				packet[0x1E] = 0x100000A1;

				if (v63) {
					packet[0x1F] = ((v63 << 8) & 0xFFFFFF) | 0x40000099;
					packet[0x20] = 0x100000B1;
					packet[0x21] = 0x28000004;
					packet[0x22] = 0x100000C1;
					packet[0x23] = (VSW - 1) | 0x4030000;
					packet[0x24] = 0x28000001;
					packet[0x25] = 0x100000A1;
					packet[0x26] = ((v63 << 8) & 0xFFFFFF) | 0x40000099;
					packet[0x27] = 0x100000B1;
					packet[0x28] = 0x28000008;
					packet[0x29] = 0x100000D1;
					packet[0x2A] = (VBP - 2) | 0x4030000;
					packet[0x2B] = 0x28000001;
					packet[0x2C] = 0x100000A1;
					packet[0x2D] = ((v63 << 8) & 0xFFFFFF) | 0x40000099;
					packet[0x2E] = 0x100000B1;
					packet[0x2F] = v98;
					packet[0x30] = 0x28000001;
					packet[0x31] = 0x100000A1;
					packet[0x32] = ((v63 << 8) & 0xFFFFFF) | 0x40000099;
				} else {
					packet[0x1F] = 0x28000002;
					packet[0x20] = 0x100000B1;
					packet[0x21] = 0x28000004;
					packet[0x22] = 0x100000C1;
					packet[0x23] = (VSW - 1) | 0x4030000;
					packet[0x24] = 0x28000001;
					packet[0x25] = 0x100000A1;
					packet[0x26] = 0x28000002;
					packet[0x27] = 0x100000B1;
					packet[0x28] = 0x28000008;
					packet[0x29] = 0x100000D1;
					packet[0x2A] = (VBP - 2) | 0x4030000;
					packet[0x2B] = 0x28000001;
					packet[0x2C] = 0x100000A1;
					packet[0x2D] = 0x28000002;
					packet[0x2E] = 0x100000B1;
					packet[0x2F] = v98;
					packet[0x30] = 0x28000001;
					packet[0x31] = 0x100000A1;
					packet[0x32] = 0x28000002;
				}

				packet[0x33] = 0x100000B1;

				unsigned int v69 = 0x28000010;
				if (v64)
					v69 = ((v64 << 8) & 0xFFFFFF) | 0x40000099;

				packet[0x34] = v69;

				if (pixel_size == 24)
					v70 = ((768 * v62) & 0xFFFFFF) | 0x400000BE;
				else
					v70 = ((960 * v62) & 0xFFFF00) | 0x400000A9;

				packet[0x35] = v70;
				packet[0x36] = (VFP - 1) | 0x4030000;
				packet[0x37] = 0x28000001;
				packet[0x38] = 0x100000A1;

				unsigned int v72 = 0x28000002;
				if (v63)
					v72 = ((v63 << 8) & 0xFFFFFF) | 0x40000099;

				packet[0x39] = v72;
				packet[0x3A] = 0x100000B1;
				packet[0x3B] = 0xC000000;

				//packet_size = &packet[0x3C] - &packet[1];
				packet_size = 0xEC;
			} else {
				char *v57;
				unsigned int v38 = timing_info->HFP;
				unsigned int v40 = timing_info->vtotal - (VFP + timing_info->VBP);
				unsigned int v41 = VSW;
				unsigned int v42 = timing_info->HSW;
				unsigned int v44 = timing_info->htotal - (HBP + HFP) - HSW;
				unsigned int v45 = flags & 1;
				unsigned int v46 = 0x40DFFFF;

				packet[0] = v40 - VSW;

				if (flags & 1)
					v41 = pixel_size;
				if (flags & 1)
					v38 *= v41;
				if (flags & 1)
					v45 = ((v38 >> 3) - 0xC) | 0x80000000;

				unsigned int v47 = flags & 2;

				if (flags & 2)
					v38 = pixel_size;
				else
					v42 = 0;

				if (flags & 2)
					v42 *= v38;
				if (flags & 2)
					v42 = ((v42 >> 3) - 0xA) | 0x80000000;

				unsigned int v49 = flags & 4;
				unsigned int v48 = (v49 == 0);

				unsigned int v97;

				if (v49)
					v47 = pixel_size;
				else
					v97 = 0;

				unsigned int v50 = 0xA30000A2;
				if (lanes == 3)
					v50 = 0xA30000A4;

				if (!v48)
					v47 *= HBP;
				if (!v48)
					v97 = ((v47 >> 3) - 0xA) | 0x80000000;

				if (v45)
					v46 = 0x417FFFF;

				packet[1] = v46;
				packet[2] = v50;
				packet[3] = 0x28000004;
				packet[4] = 0x10000001;
				if (v42) {
					packet[5] = ((v42 << 8) & 0xFFFFFF) | 0x40000019;
					packet[6] = 0x10000031;
					packet[7] = (VSW - 2) | 0x4030000;
					packet[8] = 0x28000001;
					packet[9] = 0x10000021;
					packet[0xA] = ((v42 << 8) & 0xFFFFFF) | 0x40000019;
					packet[0xB] = 0x10000031;
					packet[0xC] = 0x28000008;
					packet[0xD] = 0x10000011;
					packet[0xE] = ((v42 << 8) & 0xFFFFFF) | 0x40000019;
					packet[0xF] = 0x10000031;
					packet[0x10] = (VBP - 2) | 0x4030000;
					packet[0x11] = 0x28000001;
					packet[0x12] = 0x10000021;
					packet[0x13] = ((v42 << 8) & 0xFFFFFF) | 0x40000019;
				} else {
					packet[5] = 0x28000002;
					packet[6] = 0x10000031;
					packet[7] = (VSW - 2) | 0x4030000;
					packet[8] = 0x28000001;
					packet[9] = 0x10000021;
					packet[0xA] = 0x28000002;
					packet[0xB] = 0x10000031;
					packet[0xC] = 0x28000008;
					packet[0xD] = 0x10000011;
					packet[0xE] = 0x28000002;
					packet[0xF] = 0x10000031;
					packet[0x10] = (VBP - 2) | 0x4030000;
					packet[0x11] = 0x28000001;
					packet[0x12] = 0x10000021;
					packet[0x13] = 0x28000002;
				}
				packet[0x14] = 0x10000031;
				if (v45) {
					packet[0x15] = 0x28000001;
					packet[0x16] = 0x10000021;

					unsigned int v75 = 0x28000002;
					if (v42)
						v75 = ((v42 << 8) & 0xFFFFFF) | 0x40000019;
					packet[0x17] = v75;

					packet[0x18] = 0x10000031;

					unsigned int v76 = 0x28000010;
					if (v97)
						v76 = ((v97 << 8) & 0xFFFFFF) | 0x40000019;
					packet[0x19] = v76;

					unsigned int v77;
					if (pixel_size == 24)
						v77 = ((768 * v44) & 0xFFFFFF) | 0x4000003E;
					else
						v77 = ((960 * v44) & 0xFFFF00) | 0x40000029;
					packet[0x1A] = v77;

					packet[0x1B] = ((v45 << 8) & 0xFFFFFF) | 0x40000019;
					packet[0x1C] = ((packet[0] & 0xFFFF) - 2) | 0x4050000;
					packet[0x1D] = 0x10000021;

					unsigned int  v78 = 0x28000002;
					if (v42)
						v78 = ((v42 << 8) & 0xFFFFFF) | 0x40000019;
					packet[0x1E] = v78;

					packet[0x1F] = 0x10000031;

					unsigned int v79;
					if (v97)
						v79 = ((v97 << 8) & 0xFFFFFF) | 0x40000019;
					else
						v79 = 0x28000010;
					packet[0x20] = v79;

					unsigned int v80;
					if (pixel_size == 24)
						v80 = ((768 * v44) & 0xFFFFFF) | 0x4000003E;
					else
						v80 = ((960 * v44) & 0xFFFF00) | 0x40000029;
					packet[0x21] = v80;

					packet[0x22] = ((v45 << 8) & 0xFFFFFF) | 0x40000019;
					packet[0x23] = 0x10000021;

					if (v42) {
						packet[0x24] = ((v42 << 8) & 0xFFFFFF) | 0x40000019;
						packet[0x25] = 0x10000031;
						packet[0x26] = (VFP - 2) | 0x4030000;
						packet[0x27] = 0x28000001;
						packet[0x28] = 0x10000021;
						packet[0x29] = ((v42 << 8) & 0xFFFFFF) | 0x40000019;
					} else {
						packet[0x24] = 0x28000002;
						packet[0x25] = 0x10000031;
						packet[0x26] = (VFP - 2) | 0x4030000;
						packet[0x27] = 0x28000001;
						packet[0x28] = 0x10000021;
						packet[0x29] = 0x28000002;
					}
					packet[0x2A] = 0x10000031;

					v57 = (char *)&packet[0x2B];
				} else {
					packet[0x15] = ((packet[0] & 0xFFFF) - 1) | 0x4050000;
					packet[0x16] = 0x28000001;
					packet[0x17] = 0x10000021;

					unsigned int v53 = 0x28000002;
					if (v42)
						v53 = ((v42 << 8) & 0xFFFFFF) | 0x40000019;
					packet[0x18] = v53;

					packet[0x19] = 0x10000031;

					unsigned int v54 = 0x28000010;
					if (v97)
						v54 = ((v97 << 8) & 0xFFFFFF) | 0x40000019;
					packet[0x1A] = v54;

					unsigned int v55;
					if (pixel_size == 24)
						v55 = ((768 * v44) & 0xFFFFFF) | 0x4000003E;
					else
						v55 = ((960 * v44) & 0xFFFF00) | 0x40000029;
					packet[0x1B] = v55;

					packet[0x1C] = (VFP - 1) | 0x4030000;
					packet[0x1D] = 0x28000001;
					packet[0x1E] = 0x10000021;

					unsigned int v56 = 0x28000002;
					if (v42)
						v56 = ((v42 << 8) & 0xFFFFFF) | 0x40000019;
					packet[0x1F] = v56;

					packet[0x20] = 0x10000031;

					v57 = (char *)&packet[0x21];
				}
				*(unsigned int *)v57 = 0xC000000;
				packet_size = (v57 + 4 - (char *)&packet[1]) >> 2;
			}
			if (packet_size <= 0)
				goto LABEL_18;
		}
	} else {
		unsigned int v32;

		unsigned int htotal = timing_info->htotal;
		unsigned int horizontal_porches = timing_info->HFP + timing_info->HSW;
		unsigned int vertical_porches = VSW + VBP;
		unsigned int vtotal = timing_info->vtotal;
		unsigned int horizontal_pixels = htotal - horizontal_porches;

		if (unk07)
			v32 = 0x400B7819;
		else
			v32 = 0x400B4019;

		dsi_regs[0x20D] = 7;
		dsi_regs[0x20E] = 0;
		dsi_regs[0x201] = 1;
		dsi_regs[0x145] = 0xFFFFFFFF;

		packet[1] = 0x407FFFF;
		packet[2] = 0xA3000082;
		packet[3] = 0x28000004;
		packet[4] = 0x10000001;
		packet[5] = v32;
		packet[6] = (vertical_porches - 2) | 0x4020000;
		packet[7] = 0x28000001;
		packet[8] = 0x10000021;
		packet[9] = v32;
		packet[0xA] = ((vtotal - 1) - VSW - VBP - VFP) | 0x4030000;
		packet[0xB] = 0x28000001;
		packet[0xC] = 0x10000021;
		packet[0xD] = ((768 * HBP - 0xA00) & 0xFFFFFF) | 0x40000019;
		packet[0xE] = ((768 * (horizontal_pixels - HBP)) & 0xFFFFFF) | 0x4000003E;
		packet[0xF] = (VFP - 1) | 0x4020000;
		packet[0x10] = 0x28000001;
		packet[0x11] = 0x10000021;
		packet[0x12] = v32;
		packet[0x13] = 0xC000000;

		packet_size = 0x13;
	}

	unsigned int *packer_ptr = packet;
	unsigned int i = 0;
	do {
		unsigned int data = packer_ptr[1];
		packer_ptr++;
		i++;

		while ( dsi_regs[0x12] << 0x1C )
			dmb();

		dsi_regs[0x140] = data;
		if (i == 32)
			break;
	} while (i < packet_size);

LABEL_18:
	dsi_regs[0x147] = 1;
}

void dsi_unk(int bus, unsigned int vic, int unk)
{
	static const unsigned int bus_index = 1;
	static const unsigned int lanes = 3;
	static const unsigned int pixel_size = 24;
	static const unsigned int intr_mask = 2;

	static const unsigned int lookup[] = {2, 2, 3, 4};

	const struct dsi_timing_info *timing_info = dsi_get_timing_info_for_vic(vic);
	volatile unsigned int *dsi_regs = DSI_REGS(bus);

	unsigned int flags = timing_info->flags;
	unsigned int htotal = timing_info->htotal;
	unsigned int vtotal = timing_info->vtotal;
	unsigned int HFP = timing_info->HFP;
	unsigned int HBP = timing_info->HBP;
	unsigned int HSW = timing_info->HSW;
	unsigned int VFP = timing_info->VFP;
	unsigned int VSW = timing_info->VSW;
	unsigned int VBP = timing_info->VBP;
	unsigned int mode = timing_info->mode;

	unsigned int hsync_end = HBP + HSW;
	unsigned int hact = htotal - (HBP + HSW) - HFP;
	if (flags & (1 << 3))
		hact -= 2;

	if (mode == 1)
		dsi_regs[1] = 3;
	else
		dsi_regs[1] = 0;

	unsigned int v15;
	if (lanes == 2)
		v15 = 6;
	else if (pixel_size == 24)
		v15 = 4;
	else
		v15 = 5;

	unsigned int HFP_start = hact + hsync_end;
	unsigned int HSW_clocks = HSW * v15;
	unsigned int htotal_clocks = htotal * v15 >> 1;
	dsi_regs[2] = htotal_clocks;

	unsigned int v20;
	unsigned int v21;

	if (mode == 1) { // Interlaced
		unsigned int v36 = HFP_start * v15;
		unsigned int v37 = htotal * v15 >> 2;

		v20 = vtotal + 1;
		v21 = (vtotal + 1) >> 1;

		dsi_regs[3] = ((vtotal << 16) & 0x1FFF0000) | (v21 & 0x1FFF);
		dsi_regs[4] = 0x10001;
		dsi_regs[5] = (((hsync_end * v15) >> 1) << 16) | (((v36 >> 1) + 1) & 0xFFFF);
		dsi_regs[7] = (((VBP + VSW - 1) << 16) & 0x1FFF0000) | ((v21 - VFP) & 0x1FFF);
		dsi_regs[8] = (((VBP + VSW + ((vtotal - 1) >> 1)) << 16) & 0x1FFF0000) | ((vtotal + 1 - VFP) & 0x1FFF);
		dsi_regs[9] = ((HSW_clocks >> 1) << 16) | 1;
		dsi_regs[0xB] = 0x10001;
		dsi_regs[0xC] = ((VSW << 16) & 0x1FFF0000) | (htotal_clocks & 0xFFFF);
		dsi_regs[0xD] = ((v21 << 16) & 0x1FFF0000) | (v37 + 1);
		dsi_regs[0xE] = (((VSW + v21) << 16) & 0x1FFF0000) | v37;
	} else { // Progressive
		v20 = vtotal + 1;
		v21 = (vtotal + 1) >> 1;

		dsi_regs[3] = vtotal;
		dsi_regs[4] = 0x10001;
		dsi_regs[5] = (((HBP + HSW) * v15) >> 1 << 16) | ((((HFP_start * v15) >> 1) + 1) & 0xFFFF);
		dsi_regs[7] = (((VBP + VSW) << 16) & 0x1FFF0000) | ((vtotal + 1 - VFP) & 0x1FFF);
		dsi_regs[9] = ((HSW_clocks >> 1) << 16) | 1;
		dsi_regs[0xB] = 0x10001;
		dsi_regs[0xC] = ((VSW << 16) & 0x1FFF0000) | htotal_clocks;
	}

	if (lanes == 3 && pixel_size == 30) {
		unsigned int v34;
		unsigned int v35;

		if (vtotal == 525) {
			dsi_regs[5] = 0x130083A;
			dsi_regs[6] = 0x131083A;
			dsi_regs[9] = 0x9A0001;
			dsi_regs[0xA] = 0x9B0861;
			v34 = 1088;
			v35 = 4;
		} else if (vtotal == 750) {
			dsi_regs[5] = 0x28A0F0B;
			dsi_regs[6] = 0x2890F0B;
			dsi_regs[9] = 0x640001;
			dsi_regs[0xA] = 0x63101D;
			v34 = 0;
			v35 = 1092;
		}

		dsi_regs[0x1D] = v34;
		dsi_regs[0x1E] = v35;
		dsi_regs[1] |= 0x100;
	}

	dsi_regs[0xF] = 1;
	unsigned int vsync_start = VBP + VSW;
	dsi_regs[0x10] = (((v21 + vsync_start - 8) << 16) & 0x1FFF0000) | ((vsync_start - 8) & 0x1FFF);
	if (mode == 1) {
		dsi_regs[0x17] = ((v21 << 16) & 0x1FFF0000) | 1;
		dsi_regs[0x18] = (((VBP + VSW + ((vtotal - 1) >> 1)) << 16) & 0x1FFF0000) | 1;
		dsi_regs[0x1B] = ((v20 << 0xE) & 0x1FFF0000) | 0x40000001;
		dsi_regs[0x19] = ((vtotal << 16) & 0x1FFF0000) | 1;
		dsi_regs[0x1A] = (((VBP + VSW - 1) << 16) & 0x1FFF0000) | 1;
		dsi_regs[0x1C] = (0xC000 * v20 & 0x1FFF0000) | 0x40000001;
	} else {
		dsi_regs[0x17] = 0x10001;
		dsi_regs[0x18] = (((VBP + VSW) << 16) & 0x1FFF0000) | 1;
		dsi_regs[0x1B] = (((VBP + VSW + (vtotal >> 1)) << 16) & 0x1FFF0000) | 0x40000001;
		dsi_regs[0x1C] = 0;
	}

	dsi_regs[0x14] = dsi_regs[0x14];
	dsi_regs[0x15] = intr_mask;
	if (bus_index)
		dsi_regs[0x20E] = 1;
	else
		dsi_regs[0x20E] = 0;

	unsigned int v30;
	if (unk == 1)
		v30 = (bus ^ 1) & 1;
	else
		v30 = 0;

	dsi_regs[0x142] = 0xFFFFFFFF;

	unsigned int v31 = (unk == 2) ? (bus & 1) : 0;
	if (v30 || v31 || (unk > 4))
		dsi_regs[0] = 1;
	else
		dsi_regs[0] = lookup[unk - 1];

	dmb();
}


#if 0
static int thread_new(SceSize args, void *argp)
{
	unsigned int orig_addr[4];

#define LOG_VAL(base, offset) \
	LOG(#base " +0x%04X: 0x%08X\n", offset, GET_VAL(base, offset))

#define SET_VAL(base, offset, val) \
	do { \
		*(u32 *)((char *)(base) + (offset)) = val; \
	} while (0)

#define GET_VAL(base, offset) (*(u32 *)((char *)(base) + (offset)))

	SceIftucRegs *iftu1creg = (SceIftucRegs *)iftu1creg_addr;
	SceIftuRegs *iftu1regA = (SceIftuRegs *)iftu1regA_addr;
	SceIftuRegs *iftu1regB = (SceIftuRegs *)iftu1regB_addr;

	orig_addr[0] = GET_VAL(iftu1regA_addr, 0x200);
	orig_addr[1] = GET_VAL(iftu1regA_addr, 0x300);
	orig_addr[2] = GET_VAL(iftu1regB_addr, 0x200);
	orig_addr[3] = GET_VAL(iftu1regB_addr, 0x300);

	LOG("0x%08X\n", *(u32 *)(iftu1regA_addr + 0x100));
	LOG("0x%08X\n", *(u32 *)(iftu1regB_addr + 0x100));
	LOG("CREG +0x00 0x%08X\n", *(u32 *)(iftu1creg_addr + 0x00));
	LOG("CREG +0x04 0x%08X\n", *(u32 *)(iftu1creg_addr + 0x04));

	fill_fb(fb_addr[0], RED);
	fill_fb(fb_addr[1], GREEN);
	fill_fb(fb_addr[2], BLUE);
	fill_fb(fb_addr[3], PURP);
	ksceKernelCpuDcacheWritebackRange(fb_addr[0], FB_SIZE);
	ksceKernelCpuDcacheWritebackRange(fb_addr[1], FB_SIZE);
	ksceKernelCpuDcacheWritebackRange(fb_addr[2], FB_SIZE);
	ksceKernelCpuDcacheWritebackRange(fb_addr[3], FB_SIZE);

	/*dsi_init();
	dsi_enable_bus(1, 0x8600);
	dsi_unk(1, 0x8600, 0);*/


	uart_print(0, "\e[1;1H\e[2J");
	int y = console_get_y();
	while (run) {
		//console_set_y(y);
		iftu_shit();
		iftu_shit2();

	}

	iftu1creg->control = 1;
	iftu1regA->plane_config[0].paddr = orig_addr[0];
	iftu1regA->plane_config[1].paddr = orig_addr[1];
	iftu1regB->plane_config[0].paddr = orig_addr[2];
	iftu1regB->plane_config[1].paddr = orig_addr[3];

	return 0;
}
#endif

static int thread_old(SceSize args, void *argp)
{
	unsigned int orig_addr[4];

#define LOG_VAL(base, offset) \
	LOG(#base " +0x%04X: 0x%08X\n", offset, GET_VAL(base, offset))

#define SET_VAL(base, offset, val) \
	do { \
		*(u32 *)((char *)(base) + (offset)) = val; \
	} while (0)

#define GET_VAL(base, offset) (*(u32 *)((char *)(base) + (offset)))

	SceIftucRegs *iftu1creg = (SceIftucRegs *)iftu1creg_addr;
	SceIftuRegs *iftu1regA = (SceIftuRegs *)iftu1regA_addr;
	SceIftuRegs *iftu1regB = (SceIftuRegs *)iftu1regB_addr;

	orig_addr[0] = GET_VAL(iftu1regA_addr, 0x200);
	orig_addr[1] = GET_VAL(iftu1regA_addr, 0x300);
	orig_addr[2] = GET_VAL(iftu1regB_addr, 0x200);
	orig_addr[3] = GET_VAL(iftu1regB_addr, 0x300);

	LOG("0x%08X\n", *(u32 *)(iftu1regA_addr + 0x100));
	LOG("0x%08X\n", *(u32 *)(iftu1regB_addr + 0x100));
	LOG("CREG +0x00 0x%08X\n", *(u32 *)(iftu1creg_addr + 0x00));
	LOG("CREG +0x04 0x%08X\n", *(u32 *)(iftu1creg_addr + 0x04));

	fill_fb(fb_addr[0], RED);
	fill_fb(fb_addr[1], GREEN);
	fill_fb(fb_addr[2], BLUE);
	fill_fb(fb_addr[3], PURP);
	ksceKernelCpuDcacheWritebackRange(fb_addr[0], FB_SIZE);
	ksceKernelCpuDcacheWritebackRange(fb_addr[1], FB_SIZE);
	ksceKernelCpuDcacheWritebackRange(fb_addr[2], FB_SIZE);
	ksceKernelCpuDcacheWritebackRange(fb_addr[3], FB_SIZE);

	uart_print(0, "\e[1;1H\e[2J");
	int y = console_get_y();
	while (run) {
		//console_set_y(y);
		static int i = 0;
		int j = i / 1000;
		int k = (j % 100) < 50;

		iftu1regA->plane_config[0].paddr = fb_paddr[0];
		iftu1regA->plane_config[1].paddr = fb_paddr[1];
		iftu1regB->plane_config[0].paddr = fb_paddr[2];
		iftu1regB->plane_config[1].paddr = fb_paddr[3];

		iftu1regA->plane_config[0].control = 0;
		iftu1regB->plane_config[0].control = 0;

		iftu1regA->plane_config[0].dst_x = 100+cosf(j/100.0f)*50;
		iftu1regA->plane_config[0].dst_y = 100+sinf(j/100.0f)*50;

		iftu1regB->plane_config[0].dst_x = 200+cosf(-j/100.0f)*70;
		iftu1regB->plane_config[0].dst_y = 200+sinf(-j/100.0f)*70;

		iftu1regA->plane_config[0].src_w = 0x20000;
		iftu1regA->plane_config[0].src_h = 0x20000;
		//iftu1regB->plane_config[0].src_fb_height = 0x100;

		i++;

		iftu1regA->crtc_mask = 0;
		iftu1regB->crtc_mask = 0;

		iftu1regA->interrupt_enable = 0;
		iftu1regA->interrupts = 0;
		iftu1regB->interrupt_enable = 0;
		iftu1regB->interrupts = 0;

		iftu1creg->subctrl[0].cfg_select = k;
		iftu1creg->subctrl[1].cfg_select = 0;
	}

	iftu1creg->control = 1;
	iftu1regA->plane_config[0].paddr = orig_addr[0];
	iftu1regA->plane_config[1].paddr = orig_addr[1];
	iftu1regB->plane_config[0].paddr = orig_addr[2];
	iftu1regB->plane_config[1].paddr = orig_addr[3];

	return 0;
}

#define HDMI_I2C_BUS	1
#define HDMI_I2C_ADDR	0x7A

/* Configurable addresses */
#define HDMI_I2C_CEC_ADDR	0x7C

static void hdmi_i2c_cmd_write(unsigned char addr, unsigned char reg, const unsigned char *data, int size)
{
	unsigned char buffer[8];
	int i = 0;

	while (size > 7) {
		buffer[0] = reg + i;
		memcpy(&buffer[1], &data[i], sizeof(buffer) - 1);
		ksceI2cTransferWrite(HDMI_I2C_BUS, addr, buffer, sizeof(buffer));
		i += 7;
		size -= 7;
	}

	if (size > 0) {
		buffer[0] = reg + i;
		memcpy(&buffer[1], &data[i], size);
		ksceI2cTransferWrite(HDMI_I2C_BUS, addr, buffer, size + 1);
	}
}

static void hdmi_i2c_cmd_write_read(unsigned char addr, unsigned char reg, unsigned char *buff, unsigned int size)
{
	unsigned char buffer;
	int i = 0;

	while (size > 8) {
		buffer = reg + i;
		ksceI2cTransferWriteRead(HDMI_I2C_BUS, addr, &buffer, sizeof(buffer), addr, &buff[i], size);
		i += 8;
		size -= 8;
	}

	if (size > 0) {
		buffer = reg + i;
		ksceI2cTransferWriteRead(HDMI_I2C_BUS, addr, &buffer, sizeof(buffer), addr, &buff[i], size);
	}
}

static inline void hdmi_i2c_cmd_write_1(unsigned char addr, unsigned char reg, unsigned char data)
{
	unsigned char buff = data;
	hdmi_i2c_cmd_write(addr, reg, &buff, sizeof(buff));
}

static inline void hdmi_i2c_cmd_write_read_1(unsigned char addr, unsigned char reg, unsigned char *data)
{
	hdmi_i2c_cmd_write_read(addr, reg, data, 1);
}

static int thread_hdmi_csc(SceSize args, void *argp)
{
	uart_print(0, "\e[1;1H\e[2J");

	/* CSC */
	unsigned char csc;
	hdmi_i2c_cmd_write_read_1(HDMI_I2C_ADDR, 0x1A, &csc);
	csc &= ~(1 << 5);
	csc |= 1 << 5;
	hdmi_i2c_cmd_write_1(HDMI_I2C_ADDR, 0x1A, csc);

	//for (int i = 0; i < 8*3; i++)
		//hdmi_i2c_cmd_write_1(HDMI_I2C_ADDR, 0x18 + i, 0xFF);

	hdmi_i2c_cmd_write_1(HDMI_I2C_ADDR, 0x1E, 0x0F);
	hdmi_i2c_cmd_write_1(HDMI_I2C_ADDR, 0x1F, 0xFF);

	hdmi_i2c_cmd_write_1(HDMI_I2C_ADDR, 0x26, 0x0F);
	hdmi_i2c_cmd_write_1(HDMI_I2C_ADDR, 0x27, 0xFF);

	hdmi_i2c_cmd_write_read_1(HDMI_I2C_ADDR, 0x1A, &csc);
	csc &= ~(1 << 5);
	csc |= 0 << 5;
	hdmi_i2c_cmd_write_1(HDMI_I2C_ADDR, 0x1A, csc);

	int y = console_get_y();
	while (run) {
		//console_set_y(y);
		static int i = 0;
		unsigned char data;

		#define LOG_REG(addr, reg) \
			do { \
				unsigned char __data; \
				hdmi_i2c_cmd_write_read_1(addr, reg, &__data); \
				LOG("0x%02X: 0x%02X\n", reg, __data); \
			} while (0)

		LOG_REG(HDMI_I2C_ADDR, 0x3E);
		LOG_REG(HDMI_I2C_ADDR, 0x96);
		LOG_REG(HDMI_I2C_ADDR, 0x1E);
		LOG("\n");
	}
	return 0;
}

static void init_reg_plane_config(volatile SceIftuRegsPlaneConfig *plane)
{
	plane->paddr = fb_paddr[0];
	plane->src_fb_pixelformat = 0x10;
	plane->src_fb_width = 0x500;
	plane->src_fb_height = 0x2D0;
	plane->leftover_stride = 0;
	plane->control = 0;
	plane->dst_fb_pixelformat = 0x2000;
	plane->dst_fb_width = 0x500;
	plane->dst_fb_height = 0x2D0;
	plane->src_w = 0x10000;
	plane->src_h = 0x10000;
	plane->dst_x = 0x1A;
	plane->dst_y = 0x0F;
	plane->dst_w = 0;
	plane->dst_h = 0;
}

static void init_reg(volatile SceIftuRegs *regs)
{
	regs->crtc_mask = 1;

	regs->unk008 = 0x01F00000;
	regs->unk010 = 0x001F8000;
	regs->unk030 = 0x00352500;
	regs->unk054 = 0x0000000F;
	regs->unk058 = 0x00000108;
	regs->unk070 = 0xE5031000;
	regs->unk074 = 0x00430204;
	regs->alpha = 0x000000FF;

	regs->alpha_control = 0;

	regs->csc_control = 1;
	regs->csc_unk104 = 0x40;
	regs->csc_unk108 = 0x202;
	regs->csc_rr2 = 0x00000254;
	regs->csc_rg2 = 0x00000000;
	regs->csc_rb2 = 0x00000395;
	regs->csc_gr2 = 0x00000254;
	regs->csc_gg2 = 0x00000F93;
	regs->csc_gb2 = 0x00000EF0;
	regs->csc_br2 = 0x00000254;
	regs->csc_bg2 = 0x00000439;
	regs->csc_bb2 = 0x00000000;
	regs->csc_unk130 = 0x00000000;
	regs->csc_unk134 = 0x00000000;
	regs->csc_rr = 0x000001B7;
	regs->csc_rg = 0x00000000;
	regs->csc_rb = 0x00000000;
	regs->csc_gr = 0x00000000;
	regs->csc_gg = 0x000001B7;
	regs->csc_gb = 0x00000000;
	regs->csc_br = 0x00000000;
	regs->csc_bg = 0x00000000;
	regs->csc_bb = 0x000001B7;
	regs->csc_unk15C = 0x000003AC;
	regs->csc_unk160 = 0x00000000;
	regs->csc_unk164 = 0x000003AC;
	regs->csc_unk168 = 0x00000000;

	regs->unk180 = 1;

	//regs->interrupt_enable = 3;

	init_reg_plane_config(&regs->plane_config[0]);
	init_reg_plane_config(&regs->plane_config[1]);
}

void iftu_shit()
{
	volatile SceIftuRegs *iftu1regA = (SceIftuRegs *)iftu1regA_addr;
	volatile SceIftuRegs *iftu1regB = (SceIftuRegs *)iftu1regB_addr;
	volatile SceIftucRegs *iftu1creg = (SceIftucRegs *)iftu1creg_addr;

	init_reg(iftu1regA);
	init_reg(iftu1regB);

	iftu1creg->alpha_control = 0;

	iftu1creg->subctrl[0].cfg_select = 0;
	iftu1creg->subctrl[0].unk04 = 0;
	iftu1creg->subctrl[1].cfg_select = 0;
	iftu1creg->subctrl[1].unk04 = 0;

	iftu1creg->control = (iftu1creg->control & (0xFFFFFFEF | 0xFFFFFFFB)) | 1;
	iftu1creg->enable = 1;
	dmb();
}

static int thread(SceSize args, void *argp)
{
	unsigned int orig_addr[4];

#define LOG_VAL(base, offset) \
	LOG(#base " +0x%04X: 0x%08X\n", offset, GET_VAL(base, offset))

#define SET_VAL(base, offset, val) \
	do { \
		*(u32 *)((char *)(base) + (offset)) = val; \
	} while (0)

#define GET_VAL(base, offset) (*(u32 *)((char *)(base) + (offset)))

	SceIftucRegs *iftu1creg = (SceIftucRegs *)iftu1creg_addr;
	SceIftuRegs *iftu1regA = (SceIftuRegs *)iftu1regA_addr;
	SceIftuRegs *iftu1regB = (SceIftuRegs *)iftu1regB_addr;

	orig_addr[0] = GET_VAL(iftu1regA_addr, 0x200);
	orig_addr[1] = GET_VAL(iftu1regA_addr, 0x300);
	orig_addr[2] = GET_VAL(iftu1regB_addr, 0x200);
	orig_addr[3] = GET_VAL(iftu1regB_addr, 0x300);

	LOG("0x%08X\n", *(u32 *)(iftu1regA_addr + 0x100));
	LOG("0x%08X\n", *(u32 *)(iftu1regB_addr + 0x100));
	LOG("CREG +0x00 0x%08X\n", *(u32 *)(iftu1creg_addr + 0x00));
	LOG("CREG +0x04 0x%08X\n", *(u32 *)(iftu1creg_addr + 0x04));

	fill_fb(fb_addr[0], RED);
	fill_fb(fb_addr[1], GREEN);
	fill_fb(fb_addr[2], BLUE);
	fill_fb(fb_addr[3], PURP);
	ksceKernelCpuDcacheWritebackRange(fb_addr[0], FB_SIZE);
	ksceKernelCpuDcacheWritebackRange(fb_addr[1], FB_SIZE);
	ksceKernelCpuDcacheWritebackRange(fb_addr[2], FB_SIZE);
	ksceKernelCpuDcacheWritebackRange(fb_addr[3], FB_SIZE);


	iftu_shit();

	uart_print(0, "\e[1;1H\e[2J");
	int y = console_get_y();
	while (run) {
		//console_set_y(y);
		static int i = 0;

	}

	iftu1creg->control = 1;
	iftu1regA->plane_config[0].paddr = orig_addr[0];
	iftu1regA->plane_config[1].paddr = orig_addr[1];
	iftu1regB->plane_config[0].paddr = orig_addr[2];
	iftu1regB->plane_config[1].paddr = orig_addr[3];

	return 0;
}


int module_start(SceSize argc, const void *args)
{
	ScePervasiveForDriver_EFD084D8(0); // Turn on clock
	ScePervasiveForDriver_A7CE7DCC(0); // Out of reset
	ksceUartInit(0);
	map_framebuffer();
	LOG("\n\ndisptest by xerpi\n");

	ioremap(0xE5060000, 0x1000, &dsi1_uid, (void **)&dsi1_addr);

	ioremap(0xE5030000, 0x1000, &iftu1regA_uid, (void **)&iftu1regA_addr);
	ioremap(0xE5031000, 0x1000, &iftu1regB_uid, (void **)&iftu1regB_addr);
	ioremap(0xE5032000, 0x1000, &iftu1creg_uid, (void **)&iftu1creg_addr);

	const unsigned int fb_size_align = ALIGN(FB_SIZE, 256 * 1024);
	for (int i = 0; i < 4; i++) {
		fb_uid[i] = ksceKernelAllocMemBlock("fb", 0x40404006 , fb_size_align, NULL);
		ksceKernelGetMemBlockBase(fb_uid[i], (void **)&fb_addr[i]);

		ksceKernelGetPaddr(fb_addr[i], &fb_paddr[i]);
	}


	LOG("Iftu1RegA addr: %p\n", iftu1regA_addr);
	LOG("Iftu1RegB addr: %p\n", iftu1regB_addr);
	LOG("Iftu1cReg addr: %p\n", iftu1creg_addr);

	int ratio;
	ksceRegMgrGetKeyInt("/CONFIG/DISPLAY", "hdmi_out_scaling_ratio", &ratio);
	LOG("raio: 0x%08X\n", ratio);

	/*LOG("fb0 vaddr: %p\n", (void *)fb0_addr);
	LOG("fb1 vaddr: %p\n", (void *)fb1_addr);
	LOG("fb0 paddr: %p\n", (void *)fb0_paddr);
	LOG("fb1 paddr: %p\n", (void *)fb1_paddr);*/
	LOG("\n");

	thid = ksceKernelCreateThread("disptest", thread, 0x00, 0x1000, 0, 1 << 0, 0);

	ksceKernelStartThread(thid, 0, NULL);

	return SCE_KERNEL_START_SUCCESS;
}

int module_stop(SceSize argc, const void *args)
{
	run = 0;
	ksceKernelWaitThreadEnd(thid, NULL, NULL);

	ksceKernelFreeMemBlock(dsi1_uid);

	ksceKernelFreeMemBlock(iftu1regA_uid);
	ksceKernelFreeMemBlock(iftu1regB_uid);
	ksceKernelFreeMemBlock(iftu1creg_uid);
	for (int i = 0; i < 4; i++) {
		ksceKernelFreeMemBlock(fb_uid[i]);
	}

	unmap_framebuffer();

	return SCE_KERNEL_STOP_SUCCESS;
}

int ioremap(uintptr_t paddr, unsigned int size, SceUID *uid, void **addr)
{
	int ret;
	SceUID mem_uid;
	unsigned int mem_size;
	void *mem_addr;

	mem_size = ALIGN(size, 4096);

	SceKernelAllocMemBlockKernelOpt opt;
	memset(&opt, 0, sizeof(opt));
	opt.size = sizeof(opt);
	opt.attr = 2;
	opt.paddr = paddr;
	mem_uid = ksceKernelAllocMemBlock("ioremap", 0x20100206, mem_size, &opt);
	if (mem_uid < 0)
		return mem_uid;

	ret = ksceKernelGetMemBlockBase(mem_uid, &mem_addr);
	if (ret < 0) {
		ksceKernelFreeMemBlock(mem_uid);
		return ret;
	}

	if (uid)
		*uid = mem_uid;
	if (addr)
		*addr = mem_addr;

	return 0;
}

int alloc_phycont(unsigned int size, SceUID *uid, void **addr)
{
	int ret;
	SceUID mem_uid;
	unsigned int mem_size;
	void *mem_addr;

	mem_size = ALIGN(size, 4096);

	SceKernelAllocMemBlockKernelOpt opt;
	memset(&opt, 0, sizeof(opt));
	opt.size = sizeof(opt);
	opt.attr = 0x200004;
	opt.alignment = 0x1000;
	mem_uid = ksceKernelAllocMemBlock("phycont", 0x30808006, mem_size, &opt);
	if (mem_uid < 0)
		return mem_uid;

	ret = ksceKernelGetMemBlockBase(mem_uid, &mem_addr);
	if (ret < 0) {
		ksceKernelFreeMemBlock(mem_uid);
		return ret;
	}

	if (uid)
		*uid = mem_uid;
	if (addr)
		*addr = mem_addr;

	return 0;
}
